from distutils.core import setup
setup(name='liblib',
      version='VERSION',
      description='Lithium-ion battery simulation library',
      author='Kenneth Higa',
      author_email='KHiga@lbl.gov',
      license='modified BSD',
      platforms='All',
      requires=['numpy','scipy','pygdh'],
      packages=['liblib'],
      package_dir={'liblib': 'liblib'},
      package_data = {'liblib': ['data/*pkl']})

