============
Introduction
============

LIBlib is a Python package for macrohomogenous lithium ion battery simulation. It is compatible with Python 2 and Python 3. It inherits the philosophy of the underlying PyGDH package, with the aims of making it relatively simple to specify detailed calculations and providing a great deal of flexibility.

LIBlib is a layer on top of PyGDH and the initial software release is largely a definition of an organizational scheme that we expect will assist users in writing flexible, reusable macrohomogenous battery model code. 

As LIBlib matures, we expect that its interfaces will evolve toward increased generality, as needs that we have not anticipated arise. We will attempt to implement these changes in a way that minimizes disruption for early adopters of the library. 
