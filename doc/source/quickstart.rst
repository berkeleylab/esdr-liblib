==========
Quickstart
==========

LIBlib and PyGDH are not presently available through ``pip`` because we are required to count downloads as a condition for releasing this software as open source.

In order to install LIBlib, one should first install PyGDH and its dependencies as detailed in the PyGDH instruction manual. 

One can then download the LIBlib package in one's format of choice, unpack the archive, and from a command-line interface, run::

  <python executable name> setup.py install

where ``<python executable name>`` is typically ``python`` or ``python3``.

To confirm that LIBlib has been successfully installed, enter your Python environment and type ``import liblib``. If no error messages are printed, LIBlib is now accessible.

The following listing shows a typical LIBlib program with explanatory comments. One can think of this listing as a template, as most users will only need to make small changes in order to run their models of interest. 

.. literalinclude:: sample.py
