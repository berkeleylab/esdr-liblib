#!/bin/sh
VERSION=`cat ../../version.txt`
YEARS=`cat ../../years.txt`
OWNER=`cat ../../owner.txt`
sed "s/VERSION/${VERSION}/;s/YEARS/${YEARS}/;s/OWNER/${OWNER}/;/^.*sphinx\.ext\.autodoc/a\\    'sphinx.etc.napoleon'," dist_conf.py > conf.py
