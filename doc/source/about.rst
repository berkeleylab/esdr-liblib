=====
About
=====

LIBlib was written by Kenneth Higa under supervision by Vincent Battaglia in the Energy Storage and Distributed Resources Division of the Energy Technologies Area at Lawrence Berkeley National Laboratory (LBNL). Earlier work that led to LIBlib was performed while Kenneth Higa was supervised at LBNL by Venkat Srinivasan. Kenneth Higa thanks former LBNL colleagues Shao-Ling Wu, who developed a model that informed the development of LIBlib, and Yiling Dai, who provided helpful comments.

LBNL is operated by the University of California on behalf of the U.S. Department of Energy. Funding for this work was provided through the Vehicle Technologies Office of the U.S. Department of Energy. This work was supported by the Assistant Secretary for Energy Efficiency and Renewable Energy, Office of Building Technology, State and Community Programs, of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

