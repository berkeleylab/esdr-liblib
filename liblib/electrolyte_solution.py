import liblib.common
import math
import scipy.interpolate

import sys
if sys.version_info.major == 2:
    import cPickle as pickle
else:
    import pickle

class BaseElectrolyteSolution:
    r'''
Minimal description of electrolyte solutions

Underscores and asterisks denote dimensional quantitites.

Parameters
----------
``T_`` : float
    Temperature [K]
    '''
    
    def __init__(self,T_=298.15):

        self.T_ = T_
        self.F__RT_ = liblib.common.F_ / (liblib.common.R_*self.T_) # C/J = 1/V
        self.RT__F_ = 1.0 / self.F__RT_ # V       
        
class BinarySaltElectrolyteSolution(BaseElectrolyteSolution):
    r'''
Minimal description of binary salt electrolyte solutions

Underscores and asterisks denote dimensional quantitites.

Parameters
----------
``c_initial_`` : float
    Initial salt concentration :math:`[\textrm{mol}/\textrm{m}^3]`

``T_`` : float
    Temperature [K]
    '''
    
    def __init__(self,c_initial_,T_=298.15):

        BaseElectrolyteSolution.__init__(self)
        
        self.c_initial_ = c_initial_
        self.inverse_c_initial_ = 1./self.c_initial_        

    def kappa_(self,c_):
        r'''
This must be overriden in derived classes. 

Parameters
----------
``c_`` : float
    Salt concentration :math:`[\textrm{mol}/\textrm{m}^3]`

Returns
-------
float
    Conductivity [S / m]
        '''
        raise NotImplementedError

    def cation_transference_number(self,c_):
        r'''
This must be overriden in derived classes. 

Parameters
----------
``c_`` : float
    Salt concentration :math:`[\textrm{mol}/\textrm{m}^3]`

Returns
-------
float
    Cation transference number
        '''
        raise NotImplementedError

    def D_(self,c_):
        r'''
This must be overriden in derived classes. 

Parameters
----------
``c_`` : float
    Salt concentration :math:`[\textrm{mol}/\textrm{m}^3]`

Returns
-------
float
    Salt diffusivity :math:`[\textrm{m}^2/\mathrm{s}]`
        '''
        raise NotImplementedError

    def thermodynamic_factor_(self,c_):
        r'''
This must be overriden in derived classes. 

Parameters
----------
``c_`` : float
    Salt concentration :math:`[\textrm{mol}/\textrm{m}^3]`

Returns
-------
float
    Thermodynamic factor :math:`\frac{\partial(\ln f^*)}{\partial(\ln c_2^*)}`
        '''
        raise NotImplementedError

    def thermodynamic_factor(self,c):
        r'''
Return thermodynamic factor as computed using parameters provided to __init__().

.. math::
   \frac{\partial(\ln f)}{\partial(\ln c)}

This is used in the dimensionless calculations of the parent class.

Parameters
----------
c : float
    Dimensionless salt concentration

Returns
-------
float
        '''
        return self.thermodynamic_factor_(c*self.c_initial_)
            
class TestBinarySaltElectrolyteSolution(BinarySaltElectrolyteSolution):

    def __init__(self):

        characteristic_potential_ = 1.0 # V

        T_ = characteristic_potential_*liblib.common.F_/liblib.common_R_
        BinarySaltElectrolyteSolution.__init__(self,1.0,T_=0.)

    def kappa_(self,c_):
        r'''
This must be overriden in derived classes. 

Parameters
----------
``c_`` : float
    Salt concentration :math:`[\textrm{mol}/\textrm{m}^3]`

Returns
-------
float
    Conductivity [S / m]
        '''
        return 1.

    def cation_transference_number(self,c_):
        r'''
This must be overriden in derived classes. 

Parameters
----------
``c_`` : float
    Salt concentration :math:`[\textrm{mol}/\textrm{m}^3]`

Returns
-------
float
    Cation transference number
        '''
        return 0.5

    def D_(self,c_):
        r'''
This must be overriden in derived classes. 

Parameters
----------
``c_`` : float
    Salt concentration :math:`[\textrm{mol}/\textrm{m}^3]`

Returns
-------
float
    Salt diffusivity :math:`[\textrm{m}^2/\mathrm{s}]`
        '''
        return 1.

    def thermodynamic_factor_(self,c_):
        r'''
Parameters
----------
``c_`` : float
    Salt concentration :math:`[\textrm{mol}/\textrm{m}^3]`

Returns
-------
float
    Thermodynamic factor :math:`\frac{\partial(\ln f^*)}{\partial(\ln c_2^*)}`
        '''
        return 0.
        
class LiPF6_EC_EMC_3_7_w(BinarySaltElectrolyteSolution):
    r'''
Defines objects describing solutions of :math:`\textrm{LiPF}_6` in a 3:7 mixture by mass of ethylene carbonate / ethyl methyl carbonate, as described by Nyman, A., Behm, M. & Lindbergh, G. Electrochemical characterisation and modelling of the mass transport phenomena in LiPF6–EC–EMC electrolyte. Electrochimica Acta 53, 6356–6365 (2008).

Underscores and asterisks denote dimensional quantitites.

Parameters
----------
``c_initial_`` : float
    Initial salt concentration in volume of solution :math:`[\textrm{mol}/\textrm{m}^3]`
    '''
    def __init__(self,c_initial_):

        BinarySaltElectrolyteSolution.__init__(self,c_initial_)

    def kappa_(self,c_):
        # c_ in mol / m^3
        A1 = 1.297e-1 * 1e-8 # S m^8 mol^3
        A2 = -25.1e-1 * 1e-1**3.5 # S m^3.5 / mol^1.5
        A3 = 33.29e-1 * 1e-2 # S m^2 / mol
        return (A1*c_**2 + A2*math.sqrt(c_) + A3)*c_ # S / m

    def cation_transference_number(self,c_):
        # c_ in mol / m^3
        c_mol__dm3 = 1e-3*c_
        return c_mol__dm3*(c_mol__dm3*(-0.1287*c_mol__dm3 + 0.4106) - 0.4717) + 0.4492

    def D_(self,c_):
        # c_ in mol / m^3
        c_mol__dm3 = 1e-3*c_        
        return c_mol__dm3*(c_mol__dm3*8.794e-11 - 3.972e-10) + 4.862e-10 # m^2 / s

    def thermodynamic_factor_(self,c_):
        # c_ in mol / m^3
        c_mol__dm3 = 1e-3*c_        
        # f+/-: salt activity
        # c: salt concentration
        one_plus_dlnfdlnc = (c_mol__dm3*(c_mol__dm3*0.28687 - 0.74678) + 0.44103)/(c_mol__dm3*(c_mol__dm3*(c_mol__dm3*0.1287 - 0.4106) + 0.4717) + 0.5508)
        return one_plus_dlnfdlnc - 1.0

class LiPF6_EC_DEC_1_1_w(BinarySaltElectrolyteSolution):
    r'''
Defines objects describing solutions of :math:`\textrm{LiPF}_6` in a 1:1 mixture by mass of ethylene carbonate / diethyl carbonate. Results adapted from S. G. Stewart and J. Newman, J. Electrochem. Soc., 155(1), F13 (2008).

Underscores and asterisks denote dimensional quantitites.

Parameters
----------
``c_initial_`` : float
    initial salt concentration in volume of solution :math:`[\textrm{mol}/\textrm{m}^3]`

D_pickle_filename : str
    Location of salt diffusivity data file
    
kappa_pickle_filename : str
    Location of conductivity data file
    '''

    def __init__(self,c_initial_,D_pickle_filename = None, kappa_pickle_filename = None):

        BinarySaltElectrolyteSolution.__init__(self,c_initial_)
        
        ## Find the package data directory
        partition =  __file__.rpartition('/')
        data_file_directory = partition[0] + '/data/'

        self.interpolated_D = (D_pickle_filename != None)
        if self.interpolated_D:
            with open(D_pickle_filename,'r') as pickle_file:
                # The two NumPy arrays give concentration [mol/m^3], D [m^2/s]
                concentration,D = pickle.load(pickle_file)
                self.D_interpolant = scipy.interpolate.interp1d(concentration,D)

        self.interpolated_kappa = (kappa_pickle_filename != None)
        if self.interpolated_kappa:
            with open(kappa_pickle_filename,'r') as pickle_file:
                # The two NumPy arrays give concentration [mol/m^3], kappa [S/m]                
                concentration,kappa = pickle.load(pickle_file)
                self.kappa_interpolant = scipy.interpolate.interp1d(concentration,kappa)
        
    def cation_transference_number(self,c_):
        # Fixed value
        return 0.4
        
    def kappa_(self,c_):
        if self.interpolated_kappa:
            return self.kappa_interpolant(c_)            
        else:
            milli_c_ = c_*0.001
            return 0.1*((1.5664*milli_c_-10.155)*milli_c_+16.5)*milli_c_ # from COMSOL model cathode variables_8a
    
    def thermodynamic_factor_(self,c_):
        # Presumably, this was originally written for concentration in kmol/m^3
        sqrt_factor = math.sqrt(c_*0.001)
        # f+/-: salt activity
        # c: salt concentration
        dlnfdlnc = c_*0.001*(-1.0178*0.5/(1.0+0.9831*sqrt_factor)*(1.0/sqrt_factor-0.9831/(1.0+0.9831*sqrt_factor))+1.5842)
        return dlnfdlnc

    def D_(self,c_):
        if self.interpolated_D:
            return self.D_interpolant(c_)
        else:
            return 2.582E-9*math.exp(-2.856*c_*0.001) # from COMSOL model variables_8a for EC/DEC
