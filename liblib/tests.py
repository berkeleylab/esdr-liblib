import pygdh
import liblib
import sys

import math

import unittest

import numpy
import scipy.optimize

import liblib

class InsulatedSingleParticle(liblib.electrode.SingleParticle):

    def residual(self,sqrt_lambda):

        return math.tan(sqrt_lambda) - sqrt_lambda
    
    def __init__(self,particle_unit_count):

        self.sqrt_eigenvalue = scipy.optimize.brentq(self.residual,4.4,4.6)

        self.initial_condition = numpy.empty(particle_unit_count)
        
        liblib.electrode.SingleParticle.__init__(self,None)

    # We usually don't overload this, but in this case I want to supply an eigenfunction
    def set_initial_conditions(self):
        
        self.grid[0].field[0][0,0] = self.sqrt_eigenvalue
        self.initial_condition[0] = self.sqrt_eigenvalue 
        for vol_number in range(1,self.grid[0].unit_count):
            r = float(vol_number) / float(self.grid[0].unit_count-1)
            self.initial_condition[vol_number] = math.sin(self.sqrt_eigenvalue*r)/r
            self.grid[0].field[0][0,vol_number] = self.initial_condition[vol_number]
        
    def D1(self,c1):

        return 1.0

    def i_n(self):

        return 0.0

class UninsulatedSingleParticle(liblib.electrode.SingleParticle):

    def residual(self,sqrt_lambda):

        return math.tan(sqrt_lambda) - sqrt_lambda
    
    def __init__(self,particle_unit_count):

        self.sqrt_eigenvalue = scipy.optimize.brentq(self.residual,4.4,4.6)

        self.initial_condition = numpy.empty(particle_unit_count)
        
        liblib.electrode.SingleParticle.__init__(self,None)

    # We usually don't overload this, but in this case I want to supply an eigenfunction
    def set_initial_conditions(self):
        
        self.grid[0].field[0][0,0] = self.sqrt_eigenvalue
        self.initial_condition[0] = self.sqrt_eigenvalue 
        for vol_number in range(1,self.grid[0].unit_count):
            r = float(vol_number) / float(self.grid[0].unit_count-1)
            self.initial_condition[vol_number] = math.sin(self.sqrt_eigenvalue*r)/r
            self.grid[0].field[0][0,vol_number] = self.initial_condition[vol_number]
        
    def D1(self,c1):

        return 1.0

    def i_n(self):

        return 1.0
    
class TestSingleParticleModel_(unittest.TestCase):

    def test_insulated(self):

        particle_unit_count = 10    
        
        model = InsulatedSingleParticle(particle_unit_count)

        filename_root = 'insulated_particle'

        output_types = []

        timestep_size = 0.01

        simulation = pygdh.Simulation()

        unused_domain = liblib.electrode.SingleParticleDomain('electrode',particle_unit_count)
        
        electrode_domain = liblib.electrode.SingleParticleDomain('electrode',particle_unit_count)

        surface_domain = liblib.electrode.SingleParticleSurfaceDomain('surface')
  
        simulation.initialize_simulation([unused_domain,electrode_domain,surface_domain],[model],stored_solution_count=2)
        
        simulation.solve_time_dependent_system(filename_root,output_types,timestep_count=1, timestep_size=timestep_size, handle_exception_in_solver=False)

        tol = 1e-1
        relaxation_factor = math.exp(-model.D1(None)*model.sqrt_eigenvalue**2*timestep_size)
        for i in range(model.grid[0].unit_count):
            r = float(i)/float(model.grid[0].unit_count-1)
            assert abs(model.grid[0].field[0][0,i] - model.initial_condition[i]*relaxation_factor) < tol

    def test_uninsulated(self):

        particle_unit_count = 50
        
        model = UninsulatedSingleParticle(particle_unit_count)

        filename_root = 'uninsulated_particle'

        output_types = ['GNUPLOT']

        timestep_size = 0.01

        simulation = pygdh.Simulation()

        unused_domain = liblib.electrode.SingleParticleDomain('electrode',particle_unit_count)
        
        electrode_domain = liblib.electrode.SingleParticleDomain('electrode',particle_unit_count)

        surface_domain = liblib.electrode.SingleParticleSurfaceDomain('surface')
  
        simulation.initialize_simulation([unused_domain,electrode_domain,surface_domain],[model],stored_solution_count=2)
        
        simulation.solve_time_dependent_system(filename_root,output_types,timestep_count=1, timestep_size=timestep_size, handle_exception_in_solver=False)
 
        tol = 3e-2
        integrand = numpy.empty(model.grid[0].unit_count)
        relaxation_factor = math.exp(-model.D1(None)*model.sqrt_eigenvalue**2*timestep_size)
        for i in range(model.grid[0].unit_count):
            r = float(i)/float(model.grid[0].unit_count-1)
            integrand[i] = r**2*model.initial_condition[i]*model.grid[0].field[0][0,i]
        numerical_inner_product = numpy.trapz(integrand,dx=1./float(model.grid[0].unit_count))
        eigenfunction_norm = 0.5 - math.sin(2.*model.sqrt_eigenvalue)/(4.*model.sqrt_eigenvalue) 
        analytical_inner_product = relaxation_factor*eigenfunction_norm - math.sin(model.sqrt_eigenvalue)*model.i_n()/(model.D1(None)*model.sqrt_eigenvalue**2)*(1.-relaxation_factor)
        assert abs(numerical_inner_product - analytical_inner_product) < tol
        
class HeatEquation(liblib.separator.ConcentratedBinarySaltSolutionFilledSeparator,liblib.common.CellRegion_):

    def __init__(self):

        liblib.separator.ConcentratedBinarySaltSolutionFilledSeparator.__init__(self,1.0,1.0)

        liblib.common.CellRegion_.__init__(self,1.0,1.0,1.0)

        self.set_neighbors(self,self)

    def left_neighboring_c2(self,relative_time_index):

        return None

    def right_neighboring_c2(self,relative_time_index):

        return None
    
    def pore_volume_(self,unit_number):

        return 0.0
       
    def boundary_term_(self,unit_number):

        return 0.0

    def declare_unknowns(self):

        # These aliases help with generality, but make the code more verbose. These assignments can't be made in __init__ because this routine is called in initialize_model, but the grids are recognized through that routine as well. This is messy, I should probably define an additional hook.

        separator_grid_number = self.grid_number_with_name['solution']

        ### It would be cleaner to make these assignments elsewhere
        self.separator_grid = self.grid[separator_grid_number]
        
        self.concentration_field_number = self.separator_grid.field_number_with_name['c']
        self.potential_field_number = self.separator_grid.field_number_with_name['phi']

        for i in range(0,self.separator_grid.unit_count):
            self.unknown[separator_grid_number][self.potential_field_number,i] = False
       
        for i in range(self.separator_grid.unit_count):        
            self.unknown[separator_grid_number][self.concentration_field_number,0] = True
        
    def set_initial_conditions(self):

        concentration_field_number = self.grid[0].field_number_with_name['c']
        potential_field_number = self.grid[0].field_number_with_name['phi']
        c2 = self.grid[0].field[0][concentration_field_number]
        phi2 = self.grid[0].field[0][potential_field_number]

        for i in range(self.grid[0].unit_count):
            # Concentrations are scaled by initial concentration
            c2[i] = 2. + math.cos(math.pi*float(i)/float(self.grid[0].unit_count-1))
            phi2[i] = 0.0        
        
    def D_eff(self,c2):
        
        return 1.0

    def cation_transference_number(self,c2):

        return 0.5

    def i(self):

        return 0.0

    def kappa_eff(self,c2):

        return 1.0

    def thermodynamic_factor(self,c2):

        return 0.0

    def phi2_(self,unit_number):

        return 0.0

    def c2_(self,relative_time_index,unit_number):

        return 0.0

    def left_neighboring_phi2(self):

        return 0.0

    def right_neighboring_phi2(self):

        return 0.0
    
class CurrentDensity(liblib.separator.ConcentratedBinarySaltSolutionFilledSeparator,liblib.common.CellRegion_):

    def __init__(self):

        liblib.separator.ConcentratedBinarySaltSolutionFilledSeparator.__init__(self,0.0,1.0)

        liblib.common.CellRegion_.__init__(self,1.0,1.0,1.0)

        self.set_neighbors(self,self)
 
    def pore_volume_(self,unit_number):

        return 0.0
       
    def boundary_term_(self,unit_number):

        return 1.0
        
    def declare_unknowns(self):

        # These aliases help with generality, but make the code more verbose. These assignments can't be made in __init__ because this routine is called in initialize_model, but the grids are recognized through that routine as well. This is messy, I should probably define an additional hook.

        separator_grid_number = self.grid_number_with_name['solution']

        ### It would be cleaner to make these assignments elsewhere
        self.separator_grid = self.grid[separator_grid_number]
        
        self.concentration_field_number = self.separator_grid.field_number_with_name['c']
        self.potential_field_number = self.separator_grid.field_number_with_name['phi']

        for i in range(0,self.separator_grid.unit_count):
            self.unknown[separator_grid_number][self.potential_field_number,i] = False
       
        for i in range(self.separator_grid.unit_count):        
            self.unknown[separator_grid_number][self.concentration_field_number,0] = True

    def set_initial_conditions(self):

        concentration_field_number = self.grid[0].field_number_with_name['c']
        potential_field_number = self.grid[0].field_number_with_name['phi']
        c2 = self.grid[0].field[0][concentration_field_number]
        phi2 = self.grid[0].field[0][potential_field_number]

        for i in range(self.grid[0].unit_count):
            # Concentrations are scaled by initial concentration
            c2[i] = 1.0
            phi2[i] = 0.0        
        
    def D_eff(self,c2):
        
        return 1.0

    def cation_transference_number(self,c2):

        return 0.5

    def i(self):

        return 1.0

    def kappa_eff(self,c2):

        return 1.0

    def thermodynamic_factor(self,c2):

        return 0.0

    def phi2_(self,unit_number):

        return 0.0

    def c2_(self,relative_time_index,unit_number):

        return 0.0
    
class TestConcentratedBinarySaltSolutionFilledSeparator(unittest.TestCase):

    def test_heat_equation(self):

        separator_unit_count = 10
        unused_domain1 = liblib.separator.SeparatorDomain('solution',separator_unit_count)
        unused_domain2 = liblib.separator.SeparatorDomain('solution',separator_unit_count)                
        separator_domain = liblib.separator.SeparatorDomain('solution',separator_unit_count)
                
        model = HeatEquation()

        filename_root = 'heat_equation'

        output_types = ['']

        timestep_size = 0.01
 
        simulation = pygdh.Simulation()       
        
        simulation.initialize_simulation([unused_domain1,unused_domain2,separator_domain],[model],stored_solution_count=2)
        
        simulation.solve_time_dependent_system(filename_root,output_types,timestep_count=1, timestep_size=timestep_size, handle_exception_in_solver=False)
 
        tol = 1e-2
        k = model.D_eff(None)/model.porosity
        for i in range(separator_unit_count):
            x = float(i)/float(separator_unit_count-1)
            assert abs(separator_domain.field[0][0,i] - 2. - math.cos(math.pi*x)*math.exp(-math.pi**2*k*timestep_size)) < tol

        tol = 5e-2
        # Since phi2 is only unique to within an additive constant, we need to define a location at which phi2 = 0, which is currently the right edge of the domain. We will probably want to generalize this.
        baseline = 2.*model.T*(1.-model.cation_transference_number(None))*(1.+model.thermodynamic_factor(None))*math.log(math.exp(-math.pi**2*timestep_size)+2.)

        for i in range(separator_unit_count):
            x = float(i)/float(separator_unit_count-1)
            phi2 = separator_domain.field[0][1,i]

            assert abs(phi2-2.*model.T*(1.-model.cation_transference_number(None))*(1.+model.thermodynamic_factor(None))*math.log(math.cos(math.pi*x)*math.exp(-math.pi**2*timestep_size)+2.) + baseline) < tol
                    
    def test_current_density(self):

        separator_unit_count = 10
        unused_domain1 = liblib.separator.SeparatorDomain('solution',separator_unit_count)
        unused_domain2 = liblib.separator.SeparatorDomain('solution',separator_unit_count)                
        
        separator_domain = liblib.separator.SeparatorDomain('solution',separator_unit_count)
                
        model = CurrentDensity()

        filename_root = 'current_density'

        output_types = ['']

        timestep_size = 0.1

        simulation = pygdh.Simulation()
        
        simulation.initialize_simulation([unused_domain1,unused_domain2,separator_domain],[model],stored_solution_count=2)
        
        simulation.solve_time_dependent_system(filename_root,output_types,timestep_count=1, timestep_size=timestep_size, handle_exception_in_solver=False)

        tol = 1e-8
        c = separator_domain.field[0][0]
        phi2 = separator_domain.field[0][1]
        slope = (1.-model.cation_transference_number(None))/(model.D_eff(None))*model.inverse_z_plus_nu_plus*model.i()
        for vol in separator_domain.unit_with_number:
            dx_center = vol.default_interpolant(0.0,1,1)
            assert (dx_center(c) - slope) < tol

        # We will not test phi2 in this case, since the numerical solution for c2 contains an arbitrary additive constant
                            
if __name__ == '__main__':
    unittest.main()
