import scipy.optimize
import pygdh
import liblib.common

class Circuit(liblib.common.CellRegion_,pygdh.Model):

    def __init__(self,positive_terminal_potential_lower_bound=4.0,positive_terminal_potential_upper_bound=5.):

        self.positive_terminal_potential_lower_bound = positive_terminal_potential_lower_bound # V
        self.positive_terminal_potential_upper_bound = positive_terminal_potential_upper_bound # V
        
        self.i_ = 0.0
        self.left_neighbor = None
        self.right_neighbor = None

    def set_initial_conditions(self):
        pass
            
    def set_equation_scalar_counts(self):
        pass
    
    def declare_unknowns(self):
        pass
        
    def get_left_i_(self):

        return self.i_

    def get_right_i_(self):

        return self.i_

    def set_i_(self,i_):

        self.i_ = i_

    def residual(self,phi__positive_,V_):

        # Assume that the neighbor to the left (treating the cell layout as a circular list) is the positive electrode, and that the neighbor to the right is the negative electrode
        return self.left_neighbor.i_(phi__positive_) - self.right_neighbor.i_(phi__positive_-V_)
        
    def set_V_(self,V_):

        # V_ is the cell potential
        
        # Determine the potential at the positive terminal        
        #### This algorithm seems to evaluate the residual at the given bounds, which could conceivably lead to overflows in some situations. Might also want to consider working with an alternative form of the residual using logarithms.
        phi1_ = scipy.optimize.brentq(self.residual,self.positive_terminal_potential_lower_bound,self.positive_terminal_potential_upper_bound,args=(V_,),disp=False)

        # Assume that the neighbor to the left (treating the cell layout as a circular list) is the positive electrode, and that the neighbor to the right is the negative electrode        
        self.left_neighbor.set_phi1_(phi1_)

        self.right_neighbor.set_phi1_(phi1_ - V_)

        self.set_i_(self.left_neighbor.i_(phi1_))
        
    # def process_solution(self):

    #     V__negative = self.negative_electrode_model.get_phi_()
    #     V__positive = self.positive_electrode_model.get_phi_()

    #     V__field_number = self.grid[0].field_number_with_name['V_']

    #     self.grid[0].field[0][V__field_number,0] = V__positive - V__negative

    # def process_solution(self):

    #     #cell_domain = self.grid_with_name['cell']
    #     V__field_number = self.cell_domain.field_number_with_name['V_']
    #     V_ = self.cell_domain.field[0][V__field_number,0]

    #     # Determine the potential at the positive terminal        
    #     #### This algorithm seems to evaluate the residual at the given bounds, which could conceivably lead to overflows in some situations. Might also want to consider working with an alternative form of the residual using logarithms.
    #     phi1_ = scipy.optimize.brentq(self.residual,self.positive_terminal_potential_lower_bound,self.positive_terminal_potential_upper_bound,args=(V_,),disp=False)
        
    #     self.positive_electrode_model.set_phi1_(phi1_)

    #     self.negative_electrode_model.set_phi1_(phi1_ - V_)

    #     i__field_number = self.cell_domain.field_number_with_name['i_']

    #     self.cell_domain.field[0][i__field_number,0] = self.positive_electrode_model.i_(phi1_)
        
