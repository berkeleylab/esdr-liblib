import pygdh

F_ = 96487. # C/mol, Faraday's constant
inverse_F_ = 1./F_ # mole/C, Faraday's constant        
R_ = 8.314 # J/mol*K

# Objects of this class are the units from which the spatial domain will be
# constructed.
class OneDimensionalVolume(pygdh.Volume):

    def define_variables(self):

        self.left = self.relative_position_with_local_position(-1.0)
        self.right = self.relative_position_with_local_position(1.0)
    
    # Defining this method gives users an opportunity to define their own
    # mathematical operators, for later use in describing governing equations.
    def define_interpolants(self):

        # Operator for estimating first derivative at left boundary of volume
        self.dx_left = self.default_interpolant(-1.0, 1, 1) 
        
        # Operator for estimating function value at the left boundary of volume
        self.interpolate_left = self.default_interpolant(-1.0, 0, 1)

        # Operator for estimating function value at the volume center
        self.interpolate = self.default_interpolant(0.0, 0, 2)

        # Operator for estimating first derivative at the volume center
        self.dx = self.default_interpolant(0.0, 1, 2) 
        
        # Operator for estimating function value at the right boundary of volume
        self.interpolate_right = self.default_interpolant(1.0, 0, 1)
        
        # Operator for estimating first derivative at right boundary of volume
        self.dx_right = self.default_interpolant(1.0, 1, 1)

class CellRegion:

    def set_neighbors(self,left_neighbor,right_neighbor):
        
        self.left_neighbor = left_neighbor
        self.right_neighbor = right_neighbor
    
class CellRegion_(CellRegion):

    def __init__(self,thickness_,potential_scale):

        self.thickness_ = thickness_       

        self.inverse_thickness_ = 1./self.thickness_
        self.potential_scale = potential_scale
        self.inverse_potential_scale = 1./self.potential_scale        
            
    def get_left_i_(self):

        raise NotImplementedError

    def get_right_i_(self):

        raise NotImplementedError

class CellPorousRegionBinarySalt(CellRegion):

    def __init__(self,porosity):

        self.porosity = porosity
    
    def D_eff(self,c):
        r'''
Return

.. math::
        
   D_{\mathrm{eff}} \left( 1 - \frac{\partial \ln c_0}{\partial \ln c} \right)

Note that this is not merely the dimensionless effective salt diffusivity, although the derivative in the second factor is often assumed to be negligibly small.

This must be overridden in derived classes.

Parameters
----------
c : float
    Dimensionless salt concentration

Returns
-------
float
        '''
        raise NotImplementedError

    def kappa_eff(self,c):
        r'''
Return dimensionless effective conductivity.

This must be overriden in derived classes. 

Parameters
----------
c : float
    Dimensionless salt concentration

Returns
-------
float
        '''
        raise NotImplementedError        

class CellPorousRegionBinarySalt_(CellRegion_,CellPorousRegionBinarySalt):
    '''
Parameters
----------
porosity : float
    Separator pore volume divided by total volume

tortuosity : float
    Tortuosity :math:`\tau` of pores in separator with porosity :math:`\phi`, defined so that effective conductivity is given by :math:`\kappa_{\mathrm{eff}}^* = (\phi / \tau) \kappa^*`, where :math:`\kappa^*` is the dimensional intrinsic electrolyte solution conductivity
    '''
    def __init__(self,thickness_,electrolyte_solution,porosity,tortuosity,characteristic_potential_=1.0):

        self.electrolyte_solution = electrolyte_solution

        CellRegion_.__init__(self,thickness_,characteristic_potential_)
        CellPorousRegionBinarySalt.__init__(self,porosity)

        self.porosity__tortuosity = porosity / tortuosity

        self.D_eff__at_c_initial_ = self.D_eff_(self.electrolyte_solution.c_initial_)        
        self.inverse_D_eff__at_c_initial_ = 1./self.D_eff__at_c_initial_

        self.characteristic_time_ = self.thickness_**2 * self.inverse_D_eff__at_c_initial_
        
        self.inverse_characteristic_time_ = 1./self.characteristic_time_
        
        self.flux_scale = self.electrolyte_solution.c_initial_*self.D_eff__at_c_initial_/self.thickness_
        
        self.inverse_flux_scale = 1./self.flux_scale
        self.concentration_scale = self.electrolyte_solution.c_initial_
        self.inverse_concentration_scale = 1./self.concentration_scale        

        self.characteristic_potential_ = characteristic_potential_
                
    def pore_volume_(self,unit_number):

        raise NotImplementedError

    def boundary_term_(self,unit_number):

        raise NotImplementedError

    def c2_(self,relative_time_index,unit_number):

        raise NotImplementedError

    def phi2_(self,unit_number):

        raise NotImplementedError

    def get_left_phi2_(self):    

        raise NotImplementedError
    
    def get_right_phi2_(self):    

        raise NotImplementedError

    def left_neighboring_phi2(self):

        return self.left_neighbor.phi2_(-1) * self.inverse_potential_scale

    def right_neighboring_phi2(self):

        return self.right_neighbor.phi2_(0) * self.inverse_potential_scale
    
    def left_neighboring_pore_volume(self):

        return self.left_neighbor.pore_volume_(-1) * self.inverse_thickness_

    def left_neighboring_boundary_term(self):

        return self.left_neighbor.boundary_term_(-1) * self.inverse_flux_scale

    def right_neighboring_pore_volume(self):

        return self.right_neighbor.pore_volume_(0) * self.inverse_thickness_

    def right_neighboring_boundary_term(self):

        return self.right_neighbor.boundary_term_(0) * self.inverse_flux_scale

    def D_eff_(self,c_):
        r'''
Return

.. math::
        
   D_{\mathrm{eff}}^* \left( 1 - \frac{\partial \ln c_0^*}{\partial \ln c^*} \right)

as computed using parameters provided to __init__().

Note that this is not merely the dimensional effective diffusivity, although the derivative in the second factor is often assumed to be negligibly small.

Parameters
----------
c_ : float
    Dimensional salt concentration

Returns
-------
float
        '''      
        return self.porosity__tortuosity*self.electrolyte_solution.D_(c_)

    def D_eff(self,c):
        r'''
Return

.. math::
        
   D_{\mathrm{eff}} \left( 1 - \frac{\partial \ln c_0}{\partial \ln c} \right)

as computed using the dimensional effective diffusivity defined in ``D_eff_()``.

Note that this is not merely the dimensionless effective diffusivity, although the derivative in the second factor is often assumed to be negligibly small.

Parameters
----------
c : float
    Dimensionless salt concentration

Returns
-------
float
        '''
        return self.D_eff_(c*self.electrolyte_solution.c_initial_)*self.inverse_D_eff__at_c_initial_

    def kappa_eff(self,c):
        r'''
Return dimensionless effective conductivity as computed using parameters provided to __init__().

This is used in the dimensionless calculations of the parent class.

Parameters
----------
c : float
    Dimensionless salt concentration

Returns
-------
float
        '''
        return self.characteristic_potential_*self.inverse_D_eff__at_c_initial_*self.electrolyte_solution.inverse_c_initial_*inverse_F_*self.porosity__tortuosity*self.electrolyte_solution.kappa_(c*self.electrolyte_solution.c_initial_)
        
    def get_left_c2_(self):    

        raise NotImplementedError
    
    def get_right_c2_(self):    

        raise NotImplementedError
    
    def left_neighboring_c2(self,relative_time_index):

        left_neighbor_c2_ = self.left_neighbor.c2_(relative_time_index,-1)
        if left_neighbor_c2_ == None:
            return None
        else:
            return left_neighbor_c2_ * self.inverse_concentration_scale

    def right_neighboring_c2(self,relative_time_index):

        right_neighbor_c2_ = self.right_neighbor.c2_(relative_time_index,0)
        if right_neighbor_c2_ == None:
            return None
        else:
            return right_neighbor_c2_ * self.inverse_concentration_scale

    def cation_transference_number(self,c):
        r'''
Return cation transference number as computed using parameters provided to __init__().

This is used in the dimensionless calculations of the parent class.

Parameters
----------
c : float
    Dimensionless salt concentration

Returns
-------
float
        '''
        return self.electrolyte_solution.cation_transference_number(c*self.electrolyte_solution.c_initial_)
        
### specify Grids, Models, then customized Simulation can be used to notify all Models of neighbors
### for Models to not depend on a known Grid layout, they all have to be subsimulations. Or we can pass a grid number, but that seems messy. Could autodetect type of grid, but what if we're using more than one grid of a given type?
### specify grid and model, fixing grid numbers, and also pass left and right Models. Or simulations? We currently link Models after the fact, this is just another way to do it that also fixes the grid identification issue. We might be able to put the Grids in the Simulations

class CellRegionSimulation_:

    def set_neighbors(self,left_neighbor,right_neighbor):

        raise NotImplementedError
    
    def get_left_i_(self):

        raise NotImplementedError

    def get_right_i_(self):

        raise NotImplementedError
            
class CellPorousRegionSimulation_(CellRegionSimulation_):

    def pore_volume_(self,unit_number):

        raise NotImplementedError

    def boundary_term_(self,unit_number):

        raise NotImplementedError

    def phi2_(self,unit_number):

        raise NotImplementedError

class CellPorousRegionBinarySaltSimulation_(CellRegionSimulation_):

    def c2_(self,relative_time_index,unit_number):

        raise NotImplementedError
    
    def get_left_c2_(self):    

        raise NotImplementedError
    
    def get_right_c2_(self):    

        raise NotImplementedError
    
    def left_neighboring_c2(self,relative_time_index):

        raise NotImplementedError

    def right_neighboring_c2(self,relative_time_index):

        raise NotImplementedError
    
        
