import pygdh
import math
import numpy
import liblib.common

class BaseElectrode(liblib.common.CellRegion_):

    def i_(self,phi1_):    
        r'''
This is required to use cell potential for control, must be overloaded in derived classes.

Parameters
----------
``phi1_`` : float
    Dimensional terminal potential [V]

Returns
-------
float
    Dimensional current density :math:`[\textrm{A}/\textrm{m}^2]`
        '''
        raise NotImplementedError

    ## Assume that no current is consumed within the electrode
    
    def get_left_i_(self):    

        return self.right_neighbor.get_left_i_()
    
    def get_right_i_(self):    

        return self.left_neighbor.get_right_i_()

class BasePorousElectrodeBinarySalt_(liblib.common.CellPorousRegionBinarySalt_):

    def __init__(self,thickness_,electrolyte_solution,porosity,tortuosity,characteristic_potential_=1.0):

        liblib.common.CellPorousRegionBinarySalt_.__init__(self,thickness_,electrolyte_solution,porosity,tortuosity,characteristic_potential_=characteristic_potential_)
        
    def i_(self,phi1_):    
        r'''
This is required to use cell potential for control, must be overloaded in derived classes.

Parameters
----------
``phi1_`` : float
    Dimensional terminal potential [V]

Returns
-------
float
    Dimensional current density :math:`[\textrm{A}/\textrm{m}^2]`
        '''
        raise NotImplementedError

    ## Assume that no current is consumed within the electrode
    
    def get_left_i_(self):    

        return self.right_neighbor.get_left_i_()
    
    def get_right_i_(self):    

        return self.left_neighbor.get_right_i_()
    
class SingleParticleDomain(pygdh.Grid):
    r'''
Describes objects used to store calculations for ``SingleParticle``

Parameters
----------
name : str
    Used to generate output file names and also provides a convenient way of referencing these objects

unit_count : int
    Number of units into which domain is discretized
    '''

    def __init__(self, name, unit_count):

        self.initialize_grid(name,
                             coordinate_values = {
                                 'r': numpy.linspace(0., 1., unit_count)},
                             field_names=['c'],
                             unit_classes=[ liblib.common.OneDimensionalVolume
                                            for i in range(unit_count)],
                             stored_solution_count=2)

    def define_field_variables(self):

        self.r_ = numpy.zeros(self.unit_count)
        self.c_ = numpy.zeros(self.unit_count)

    def set_output_fields(self):

        self.output_fields = {
            'r_ [m]': self.r_,
            'c_ [mol/m^3]': self.c_}

class SingleParticleSurfaceDomain(pygdh.Grid):

    def __init__(self,name):

        self.initialize_grid(name, unit_classes=[ pygdh.Volume ])

    def define_field_variables(self):

        self.phi_ = numpy.zeros(1)        

    ### this output might not appear if there are not regular fields defined
    def set_output_fields(self):

        self.output_fields = {
            'phi_ [V]': self.phi_}
        
class SingleParticle(pygdh.Model):
    r'''
Asterisks mark dimensionless quantities. Within the source code, variables representing quantities and having names without a trailing underscore are dimensionlesss. The following definitions transform the dimensional model described by ``SingleParticle_``:

.. math::

   c_1 = c_1^*/c_{1,\mathrm{max}}^*

.. math::

   r = r^*/r_n^*

.. math::

   t = \frac{t^* D_1^*(c_{1,\mathrm{max}}^*)}{{r_n^*}^2}

.. math::

   N_1 = \frac{r_n^* N_1^*}{D_1^*(c_{1,\mathrm{max}}^*) c_{1,\mathrm{max}}^*}

.. math::

   D_1(c_1) = D_1^*(c_1^*) / D_1^*(c_{1,\mathrm{max}}^*).

This class describes objects that approximately solve

.. math:: 
    
   \frac{\partial c_1}{\partial t} = -\frac{1}{{r}^2} \frac{\partial}{\partial r} \left[{r}^2 N_1\right]

using the FVM representation

.. math::
    
   0 = \frac{\overline{c_1}(t,r_i) - \overline{c_1}(t-\Delta t,r_i)}{\Delta t} \frac{{r_{i+1}}^3 - {r_i}^3}{3} + \left[{r}^2 N_1(t)\right]_{r_i}^{r_{i+1}} + \mathcal{O}(\Delta t),

where

.. math::

   \overline{c_1}(t,r_i) = \frac{3}{{r_{i+1}}^3 - {r_i}^3} \int_{r_i}^{r_{i+1}} {r}^2 c_1(t,r) \ dr

.. math::

   N_1 = -D_1(c_1) \frac{\partial c_1}{\partial r}

.. math::

   N_1 = 0 \textrm{ at center } r = 0

.. math::

   N_1 = i_n \textrm{ at surface } r = 1.

Parameters
----------
c_initial : float
    Initial dimensionless uniform lithium concentration within the particle

interior_domain : electrode.SingleParticleDomain
    Storage object for calculations within the particle

surface_domain : electrode.SingleParticleSurfaceDomain
    Storage object for calculations involving the particle surface
    '''
    
    def __init__(self,grid_assignment,c_initial):

        assert type(grid_assignment) == dict
        
        self.grid_assignment = grid_assignment

        self.c_initial = c_initial

        self.one_third = 1./3.

    def declare_unknowns(self):

        self.electrode_grid_number = self.grid_number_with_name[self.grid_assignment['interior']]
        for grid_number in range(len(self.grid)):
            self.unknown[grid_number][:,:] = (grid_number == self.electrode_grid_number)

        self.electrode_grid = self.grid[self.electrode_grid_number]

        self.surface_grid_number = self.grid_number_with_name[self.grid_assignment['surface']]

        self.surface_grid = self.grid[self.surface_grid_number]        
        
        # self.positive_electrode_grid = self.grid[electrode_grid_number]
        # for vol_number in range(self.grid[electrode_grid_number].unit_count):
        #     self.unknown[electrode_grid_number][0,vol_number] = True
            
    def set_equation_scalar_counts(self):
        self.equation_scalar_counts = {
            self.particle_inner_equation:1,
            self.particle_interior_equation:1,
            self.particle_outer_equation:1 } 

    def assign_equations(self):

        electrode_equations = self.equations[self.electrode_grid_number]
        electrode_equations[0] = [self.particle_inner_equation]
        for i in range(1,self.grid[self.electrode_grid_number].unit_count-1):
            electrode_equations[i] = [self.particle_interior_equation]
        electrode_equations[-1] = [self.particle_outer_equation]        

    def set_initial_conditions(self):

        for vol in self.grid[self.electrode_grid_number].unit_with_number:
            self.grid[self.electrode_grid_number].field[0][0,vol.number] = self.c_initial

    def particle_inner_equation(self, vol, residual):
        # This method isn't actually necessary; particle_interior_equation should give the same result at this location, just slightly less efficiently.

        # Defined for clarity
        c_1 = self.electrode_grid.field[-1][0]        
        c = self.electrode_grid.field[0][0]

        #c_left = vol.interpolate_left(c)
        c_right = vol.interpolate_right(c)        

        r_left = vol.coordinate + vol.left
        r_right = vol.coordinate + vol.right
        
        residual[0] = (vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size*r_right**3*self.one_third - self.D1(c_right)*r_right**2*vol.dx_right(c)

    def particle_interior_equation(self, vol, residual):

        # Defined for clarity
        c_1 = self.electrode_grid.field[-1][0]        
        c = self.electrode_grid.field[0][0]

        c_left = vol.interpolate_left(c)
        c_right = vol.interpolate_right(c)        

        r_left = vol.coordinate + vol.left
        r_right = vol.coordinate + vol.right

        residual[0] = (vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size*(r_right**3 - r_left**3)*self.one_third - (self.D1(c_right)*r_right**2*vol.dx_right(c) - self.D1(c_left)*r_left**2*vol.dx_left(c))

    def particle_outer_equation(self, vol, residual):

        # Defined for clarity
        c_1 = self.electrode_grid.field[-1][0]        
        c = self.electrode_grid.field[0][0]

        c_left = vol.interpolate_left(c)
        #c_right = vol.interpolate_right(c)        

        r_left = vol.coordinate + vol.left
        r_right = vol.coordinate + vol.right

        residual[0] = (vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size*(r_right**3 - r_left**3)*self.one_third + r_right**2*self.i_n() + self.D1(c_left)*r_left**2*vol.dx_left(c)

    def D1(self,c):
        r'''
This must be overridden in derived classes. 

Parameters
----------
    c : Dimensionless lithium concentration

Returns
-------
    float
        Dimensionless lithium diffusivity
        '''
        raise NotImplementedError        

    def i_n(self):
        r'''
This must be overridden in derived classes. 

Returns
-------
    float
        Instantaneous dimensionless conventional current density, equal to the dimensionless lithium flux, in the positive r direction
        '''
        raise NotImplementedError
            
    def process_solution(self):
        pass
    
class SingleParticle_(BasePorousElectrodeBinarySalt_,SingleParticle):
    r'''
    All dimensional quantities are marked with asterisks. The choice of units for dimensional quantities must be consistent, so that all units cancel in the dimensionless definitions given in the ``SingleParticle`` docstring. Within the source code, variables representing quantities and having names with a trailing underscore are dimensional.

Uses ``SingleParticle`` to approximately solve

.. math:: 
    
   \frac{\partial c_1^*}{\partial t^*} = -\frac{1}{{r^*}^2} \frac{\partial}{\partial r^*} \left[{r^*}^2 N_1^*\right]

.. math::

   N_1^* = -D_1^* \frac{\partial c_1^*}{\partial r^*},

where 

.. math::

   N_1^* = 0 \textrm{ at center } r^* = 0

.. math::

   N_1^* = i_n^* / F^* \textrm{ at surface } r^* = r_n^*

Parameters
----------
interior_domain : SingleParticleDomain
    Storage object for calculations within the particle

surface_domain : SingleParticleSurfaceDomain
    Storage object for information at the particle surface

active_material : active_material.BaseMaterial
    Object describing electrode active material propoerties

``particle_radius_`` : float
    Dimensional particle radius

``active_material_loading_`` : float
    Active material mass per unit electrode area :math:`[\textrm{kg}/\textrm{m}^2]`

electrochemical_reaction : electrochemical_reaction.BaseElectrochemicalReaction
    Object describing main electrochemical reaction for electrode
    '''
    
    def __init__(self,grid_assignment,active_material,particle_radius_,active_material_loading_,electrochemical_reaction):        
        
        # These are only used for input and output and therefore do not need to
        # be "registered" with the nonlinear solver system

        # All electrolyte solution fields will keep their initial inactive
        # status, information will be copied from the neighboring electrolyte
        # solution regions.

        self.active_material = active_material

        SingleParticle.__init__(self,grid_assignment,self.active_material.c_initial_*self.active_material.inverse_c_max_)

        # This is not needed as this does not really have to be handled as a porous region, it just has to look like one to the neighboring regions when responding to queries
        #BasePorousElectrodeBinarySalt.__init__(self,self.thickness_,self.electrolyte_solution,self.electrolyte_solution.c_initial_,self.electrolyte_solution.c_initial_*self.D_eff__at_c_initial_/self.thickness_)
        
        self.particle_radius_ = particle_radius_

        self.active_material_volume_per_area_ = active_material_loading_*self.active_material.inverse_density_ # m

        self.capacity_per_area_ = liblib.common.F_*(self.active_material.c_max_ - self.active_material.c_min_)*self.active_material_volume_per_area_ # C / m^2, assumes one-electron reaction   
                
        self.active_material_area_density_ = self.active_material_volume_per_area_ / (4/3.*math.pi*particle_radius_**3) # m^{-2}, particle number area density
        
        self.inverse_active_material_area_density_ = 1./self.active_material_area_density_

        self.electrochemical_reaction = electrochemical_reaction

        self.surface_area_per_particle_ = 4. * math.pi * self.particle_radius_**2

        self.inverse_surface_area_per_particle_ = 1./self.surface_area_per_particle_

        self.characteristic_time_ = self.particle_radius_**2 * self.active_material.inverse_D_at_c_initial_
        
        self.inverse_characteristic_time_ = 1./self.characteristic_time_

    def set_grid(self,grid,stored_solution_count=1):

        SingleParticle.set_grid(self,grid,stored_solution_count=stored_solution_count)

        # These dimensional coordinates never change
        for index in range(self.electrode_grid.unit_count):
            self.electrode_grid.r_[index] = self.particle_radius_*self.electrode_grid.coordinates_list[0][index]

    def set_initial_conditions(self):

        # don't override the initial condition on c_
        SingleParticle.set_initial_conditions(self)
        
    def pore_volume_(self,number):
        # This is not a porous region
        return 0.0

    def boundary_term_(self,number):
        # All current at the interface is carried by cations
        return liblib.common.inverse_F_*self.left_neighbor.get_right_i_()

    def c2_(self,relative_time_index,unit_number):

        return None
    
    def phi2_(self,unit_number):

        return 0.0
    
    def set_phi1_(self,phi_):

        # concentratedbinarysaltsolutionfilledseparator sets the value of phi_ an extra field variable, which is just supposed to be a scaled value. I don't see why we should be setting it here, that's probably a mistake.
        # I think this is an extra field variable and we're just reporting out, but I think we should be reporting something else out
        self.grid[self.surface_grid_number].phi_[0] = phi_
        
    def get_phi_(self):

        return self.grid[self.surface_grid_number].phi_[0]

    def time_dependent_system_step(self,timestep_size,output,handle_exception_in_solver=True,adaptive_timestepping=False,minimum_timestep_size=1e-6,use_predictor=True,quiet=False,interrupted=False):

        # Call base class method with dimensionless timestep size
        return SingleParticle.time_dependent_system_step(self,timestep_size*self.inverse_characteristic_time_,output,handle_exception_in_solver=handle_exception_in_solver,adaptive_timestepping=adaptive_timestepping,minimum_timestep_size=minimum_timestep_size,use_predictor=use_predictor,quiet=quiet,interrupted=interrupted)
        
    def D1(self,c):
        r'''
This is used in the dimensionless calculations of the parent class.

Parameters
----------
c : float
    Dimensionless lithium concentration

Returns
-------
float
    Dimensionless lithium diffusivity
        '''
        return self.active_material.D_(c*self.active_material.c_max_)*self.active_material.inverse_D_at_c_initial_

    def i_n(self):
        r'''
This is used in the dimensionless calculations of the parent class.

Returns
-------
    float
        Instantaneous dimensionless conventional microscopic current density (at the particle surface), equal to the dimensionless lithium flux, in the positive r direction
        '''
        i_ = self.left_neighbor.get_right_i_()

        return self.particle_radius_*self.active_material.inverse_D_at_c_initial_*self.active_material.inverse_c_max_*self.inverse_surface_area_per_particle_*self.inverse_active_material_area_density_*i_*liblib.common.inverse_F_

    def i_n_(self):
        r'''
Returns
-------
    float
        Instantaneous dimensional conventional microscopic current density (at the particle surface), equal to the dimensionless lithium flux, in the positive r direction
        '''
        i_ = self.left_neighbor.get_right_i_()
        
        return self.inverse_surface_area_per_particle_*self.inverse_active_material_area_density_*i_

    def i_(self,phi1_):
        r'''
Calculates dimensional current density by the Butler-Volmer equation, as required to use cell potential for control

Parameters
----------
``phi1_`` : float
    Dimensional terminal potential [V]

Returns
-------
float
    Dimensional current density :math:`[\textrm{A}/\textrm{m}^2]`
    '''

        c_field_number = self.electrode_grid.field_number_with_name['c']
        c_ = self.electrode_grid.field[0][c_field_number,-1]*self.active_material.c_max_

        if isinstance(self.left_neighbor,liblib.common.CellPorousRegionBinarySalt_) or isinstance(self.left_neighbor,liblib.common.CellPorousRegionBinarySaltSimulation_):
            c2_ = self.left_neighbor.c2_(0,-1)
            phi2_ = self.left_neighbor.phi2_(-1)
        elif isinstance(self.right_neighbor,liblib.common.CellPorousRegionBinarySalt_) or isinstance(self.right_neighbor,liblib.common.CellPorousRegionBinarySaltSimulation_):
            c2_ = self.right_neighbor.c2_(0,0)
            phi2_ = self.right_neighbor.phi2_(0)
        else:
            print('ERROR: SingleParticle_ does not have a neighboring porous region.')
            raise AssertionError

        eta_s_ = phi1_ - phi2_ - self.electrochemical_reaction.U_(c_)

        return self.surface_area_per_particle_*self.active_material_area_density_*self.electrochemical_reaction.i_n_(eta_s_,c_,c2_)
    
    def process_solution(self):

        c_field_number = self.electrode_grid.field_number_with_name['c']
        for index in range(self.electrode_grid.unit_count):
            self.electrode_grid.c_[index] = self.active_material.c_max_*self.electrode_grid.field[0][c_field_number,index]

        # For this thin electrode, the macroscopic salt concentration and potential in solution are taken to be identical to an adjacent solution-filled region. In all foreseeable uses, this electrode will have an adjacent solution-filled region on only one side 

        if isinstance(self.left_neighbor,liblib.common.CellPorousRegionBinarySalt_) or isinstance(self.left_neighbor,liblib.common.CellPorousRegionBinarySaltSimulation_):
            c2_ = self.left_neighbor.c2_(0,-1)
            phi2_ = self.left_neighbor.phi2_(-1)
        elif isinstance(self.right_neighbor,liblib.common.CellPorousRegionBinarySalt_) or isinstance(self.right_neighbor,liblib.common.CellPorousRegionBinarySaltSimulation_):
            c2_ = self.right_neighbor.c2_(0,0)
            phi2_ = self.right_neighbor.phi2_(0)
        else:
            raise NotImplementedError

        self.grid[self.surface_grid_number].phi_[0] = self.electrochemical_reaction.V_(self.i_n_(),self.electrode_grid.c_[-1],c2_) + phi2_

class SingleParticleSimulation_(pygdh.Simulation,liblib.common.CellPorousRegionBinarySaltSimulation_):
    
    def __init__(self,name,output_types,particle_unit_count,active_material,particle_radius_,active_material_loading_,electrochemical_reaction,handle_exception_in_solver=True):

        pygdh.Simulation.__init__(self,name,output_types,handle_exception_in_solver=handle_exception_in_solver)
        
        self.particle_unit_count = particle_unit_count
        particle_domain = SingleParticleDomain('interior',self.particle_unit_count)
        surface_domain = liblib.electrode.SingleParticleSurfaceDomain('surface')
        
        self.electrode_model = SingleParticle_({'interior':'interior','surface':'surface'},active_material,particle_radius_,active_material_loading_,electrochemical_reaction)
        self.initialize_simulation([particle_domain,surface_domain],[self.electrode_model],stored_solution_count=2)
        self.capacity_per_area_ = self.electrode_model.capacity_per_area_

    def set_neighbors(self,left,right):
        self.electrode_model.set_neighbors(left,right)
        self.right_neighbor = self.electrode_model.right_neighbor
        self.left_neighbor = self.electrode_model.left_neighbor                

    # It would be nice to have a more elegant way of making these methods available. However, a Simulation inheriting a Model doesn't make sense within the PyGDH organizational scheme. Inheriting from something like a common Component class also wouldn't work because the Simulation creates Models as members. Allowing neighbors to reach into the encapsulated Models to execute these methods would break encapsulation. Creating a Component class that is inherited by the Models (or a ComponentModel class) and which requires these methods would be helpful for organization, and a ComponentSimulation class could define all of these automatically, based on the "primary" encapsulated ComponentModel.
        
    def i_(self,phi1_):
        return self.electrode_model.i_(phi1_)

    def set_phi1_(self,phi_):
        return self.electrode_model.set_phi1_(phi_)

    def phi2_(self,unit_number):
        return self.electrode_model.phi2_(unit_number)

    def c2_(self,relative_time_index,unit_number):

        return self.electrode_model.c2_(relative_time_index,unit_number)

    def pore_volume_(self,number):

        return self.electrode_model.pore_volume_(number)
    
    def boundary_term_(self,number):

        return self.electrode_model.boundary_term_(number)

    def get_phi_(self):

        return self.electrode_model.get_phi_()

    def get_left_i_(self):

        return self.electrode_model.get_left_i_()

    def get_right_i_(self):

        return self.electrode_model.get_right_i_()    
    
#### Pseudo-2D electrode model ####

class EmbeddedParticle_(BaseElectrode,SingleParticle):
    r'''
    All dimensional quantities are marked with asterisks. The choice of units for dimensional quantities must be consistent, so that all units cancel in the dimensionless definitions given in the ``SingleParticle`` docstring. Within the source code, variables representing quantities and having names with a trailing underscore are dimensional.

    Thisi class is similar to SingleParticle_ but has a more complicated relationship to the surrounding electrolyte region

Uses ``SingleParticle`` to approximately solve

.. math:: 
    
   \frac{\partial c_1^*}{\partial t^*} = -\frac{1}{{r^*}^2} \frac{\partial}{\partial r^*} \left[{r^*}^2 N_1^*\right]

.. math::

   N_1^* = -D_1^* \frac{\partial c_1^*}{\partial r^*},

where 

.. math::

   N_1^* = 0 \textrm{ at center } r^* = 0

.. math::

   N_1^* = i_n^* / F^* \textrm{ at surface } r^* = r_n^*

Parameters
----------
interior_domain : SingleParticleDomain
    Storage object for calculations within the particle

surface_domain : SingleParticleSurfaceDomain
    Storage object for information at the particle surface

active_material : active_material.BaseMaterial
    Object describing electrode active material propoerties

``particle_radius_`` : float
    Dimensional particle radius

``active_material_loading_`` : float
    Active material mass per unit electrode area :math:`[\textrm{kg}/\textrm{m}^2]`

electrochemical_reaction : electrochemical_reaction.BaseElectrochemicalReaction
    Object describing main electrochemical reaction for electrode
    '''
    
    def __init__(self,grid_assignment,medium,position,active_material,particle_radius_,active_material_loading_,electrochemical_reaction):        
        
        # These are only used for input and output and therefore do not need to
        # be "registered" with the nonlinear solver system

        # All electrolyte solution fields will keep their initial inactive
        # status, information will be copied from the neighboring electrolyte
        # solution regions.

        self.medium = medium
        self.position = position
        
        self.active_material = active_material

        SingleParticle.__init__(self,grid_assignment,self.active_material.c_initial_*self.active_material.inverse_c_max_)

        self.particle_radius_ = particle_radius_

        self.active_material_volume_per_area_ = active_material_loading_*self.active_material.inverse_density_ # m

        self.capacity_per_area_ = liblib.common.F_*(self.active_material.c_max_ - self.active_material.c_min_)*self.active_material_volume_per_area_ # C / m^2, assumes one-electron reaction   
                
        self.active_material_area_density_ = self.active_material_volume_per_area_ / (4/3.*math.pi*particle_radius_**3) # m^{-2}, particle number area density
        
        self.inverse_active_material_area_density_ = 1./self.active_material_area_density_

        self.electrochemical_reaction = electrochemical_reaction

        self.surface_area_per_particle_ = 4. * math.pi * self.particle_radius_**2

        self.inverse_surface_area_per_particle_ = 1./self.surface_area_per_particle_

        self.characteristic_time_ = self.particle_radius_**2 * self.active_material.inverse_D_at_c_initial_
        
        self.inverse_characteristic_time_ = 1./self.characteristic_time_

    def set_initial_conditions(self):
        #### not the right place for this
        self.interior_grid = self.grid_with_name[self.grid_assignment['interior']]
        self.surface_grid = self.grid_with_name[self.grid_assignment['surface']]
        
        # These dimensional coordinates n`<ever change
        for index in range(self.interior_grid.unit_count):
            self.interior_grid.r_[index] = self.particle_radius_*self.interior_grid.coordinates_list[0][index]

        self.surface_grid.phi_[0] = 4.0
        # don't override the initial condition on c_
        SingleParticle.set_initial_conditions(self)
        
    def pore_volume_(self,number):
        # This is not a porous region
        return 0.0
    #### not used internally
    def boundary_term_(self,number):
        # All current at the interface is carried by cations
        return liblib.common.inverse_F_*1e-8#self.cell_domain.field[0][self.cell_domain.field_number_with_name['i_'],0]

    def c2_(self,relative_time_index,unit_number):

        return None
    
    def phi2_(self,unit_number):

        return 0.0
    
    def set_phi1_(self,phi_):
        self.surface_grid.phi_[0] = phi_
        
    def get_phi_(self):
        return self.surface_grid.phi_[0]

    def time_dependent_system_step(self,timestep_size,output,handle_exception_in_solver=True,adaptive_timestepping=False,minimum_timestep_size=1e-6,use_predictor=True,quiet=False,interrupted=False):

        # Call base class method with dimensionless timestep size
        return SingleParticle.time_dependent_system_step(self,timestep_size*self.inverse_characteristic_time_,output,handle_exception_in_solver=handle_exception_in_solver,adaptive_timestepping=adaptive_timestepping,minimum_timestep_size=minimum_timestep_size,use_predictor=use_predictor,quiet=quiet,interrupted=interrupted)
        
    def i_n(self):
        r'''
This is used in the dimensionless calculations of the parent class.

Returns
-------
    float
        Instantaneous dimensionless conventional microscopic current density (at the particle surface), equal to the dimensionless lithium flux, in the positive r direction
        '''
        # Unlike with SingleParticle_, the macroscopic current density is not uniformly distributed to all particles and must be computed        
        return self.particle_radius_*self.active_material.inverse_D_at_c_initial_*self.active_material.inverse_c_max_*self.inverse_surface_area_per_particle_*self.inverse_active_material_area_density_*liblib.common.inverse_F_*self.i_n_()
    
    def i_n_(self):
        r'''
Returns
-------
    float
        Instantaneous dimensional conventional microscopic current density (at the particle surface), equal to the dimensionless lithium flux, in the positive r direction
        '''
        ## Unlike with SingleParticle_, the macroscopic current density is not uniformly distributed to all particles and must be computed
        
        c_field_number = self.interior_grid.field_number_with_name['c']
        c_ = self.interior_grid.field[0][c_field_number,-1]*self.active_material.c_max_

        phi1_ = self.surface_grid.phi_[0]

        ## One could alternatively use direct access to data structures, which would be faster but breaks encapsulation
        
        c2_ = self.medium.c2_(self.position)
        phi2_ = self.medium.phi2_(self.position)

        eta_s_ = phi1_ - phi2_ - self.electrochemical_reaction.U_(c_)

        return self.electrochemical_reaction.i_n_(eta_s_,c_,c2_)

    def i_(self,phi1_):
        r'''
Calculates dimensional current density by the Butler-Volmer equation, as required to use cell potential for control

Parameters
----------
``phi1_`` : float
    Dimensional terminal potential [V]

Returns
-------
float
    Dimensional current density :math:`[\textrm{A}/\textrm{m}^2]`
    '''

        c_field_number = self.interior_grid.field_number_with_name['c']
        c_ = self.interior_grid.field[0][c_field_number,-1]*self.active_material.c_max_
        #### this now has to come out of the information about the medium. This should now be used for all control, not just cell potential control, since the particles might have different current densities due to different internal concentrations, different potentials, different medium information. This will still be used for cell potential control, but when current control is used, we might require an root-finding step to get the correct potential and then use this. Or in the original code, the boundary condition for the matrix potential was tied to the applied current density, can that be done here? Not if the entire matrix is at the same potential

        #### Could alternatively use direct access to data structures
        c2_ = self.medium.c2_(self.position)
        phi2_ = self.medium.phi2_(self.position)

        eta_s_ = phi1_ - phi2_ - self.electrochemical_reaction.U_(c_)
        #### this is macroscopic. why don't we use the i_n_ here in place of the one defined above?
        return self.surface_area_per_particle_*self.active_material_area_density_*self.electrochemical_reaction.i_n_(eta_s_,c_,c2_)
    
    def process_solution(self):

        c_field_number = self.interior_grid.field_number_with_name['c']
        for index in range(self.interior_grid.unit_count):
            self.interior_grid.c_[index] = self.active_material.c_max_*self.interior_grid.field[0][c_field_number,index]

        #### Could alternatively use direct access to data structures
        c2_ = self.medium.c2_(self.position)
        phi2_ = self.medium.phi2_(self.position)

        self.surface_grid.phi_[0] = self.electrochemical_reaction.V_(self.i_n_(),self.interior_grid.c_[-1],c2_) + phi2_

#### maybe simulations should be responsible for printing structure contents (allowing for overrides), and Models are responsible for the math (and might share structures), and simulations can coordinate Models and other cooridinators. It would be nice to be able to have simulations do staggered or simultaneous solves, although the equations would always contain the staggered forms, although that's okay as that should be compatible with the simultaneous forms. But this would mean separating the solving structure from the Models and moving it to the simulation.

# Objects of this class describe the spatial domains on which models will be
# solved.
class PorousElectrodeDomain(pygdh.Grid):
    '''This represents the porous, electrically-conductive matrix surrounding the active material particles in a porous electrode.
'''
    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_grid' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self, name, volume_count):

        # # NMC333
        # self.phi1_int = 4.3 # V initial potential of positive electrode, from COMSOL model
        # #self.phi1_int = 4.290516691195901 # V initial potential of positive electrode, modified
        # self.phi2_int = 0.0 # V initial potential of solution, from COMSOL model
        # Graphite
        # self.phi1_int = 1.3 # V initial potential of positive electrode, from COMSOL model
        # self.phi2_int = 0.0 # V initial potential of solution, from COMSOL model        

        # self.c2_int = 500. # mol/m^3 initial salt concentration, from COMSOL model

        self.initialize_grid(name,
                             field_names=['phi1','phi2','c2'],
                             # The dependent variable will be named 'x',
                             # and 'phi1' will be computed at the specified number
                             # of equally-spaced values of 'x'
                             coordinate_values={
                                 'x': numpy.linspace(0.0, 1.0, volume_count)},
                             # Each unit in 'Domain' will be described by a
                             # 'Volume' object
                             unit_classes=[
                                 liblib.common.OneDimensionalVolume for i in range(volume_count) ])

    # This method is required. It informs GDB of the unknown quantities for
    # for which it must solve.
    def declare_unknowns(self):

        for vol_number in range(0, self.unit_count):
            vol = self.unit_with_number[vol_number]
            vol.unknown_field[0] = True
            vol.unknown_field[1] = True            
            vol.unknown_field[2] = True

    def set_initial_conditions(self):

        phi1 = self.field[0][0]        
        phi2 = self.field[0][1]        
        c2 = self.field[0][2]
        
        for i in range(self.unit_count):
            phi1[i] = self.phi1_int
            phi2[i] = self.phi2_int
            c2[i] = self.c2_int

    def define_field_variables(self):

        self.surface_overpotential = numpy.empty(self.unit_count)        
        self.i_n_value = numpy.empty(self.unit_count)
        
        # self.left_Li_flux = numpy.empty(self.unit_count)
        # self.right_Li_flux = numpy.empty(self.unit_count)        
        # self.moles_Li_change = numpy.empty(self.unit_count)
        # self.flux_difference = numpy.empty(self.unit_count)        

    def set_output_fields(self):

        self.output_fields = {
            'surface_overpotential' : self.surface_overpotential,
            'i_n' : self.i_n_value}#,
            # 'left_Li_flux' : self.left_Li_flux,
            # 'right_Li_flux' : self.right_Li_flux,            
            # 'moles_Li_change' : self.moles_Li_change,
            # 'flux_difference' : self.flux_difference}

class ConcentratedBinarySaltSolutionFilledPorousConductiveMatrix_(pygdh.Model,BasePorousElectrodeBinarySalt_):
    r'''
Asterisks here mark dimensional quantities, those without asterisks are dimensionless. Within the source code, variables representing quantities and having names without a trailing underscore are dimensionlesss. The following definitions transform the dimensional model described by ``ConcentratedBinarySaltSolutionFilledSeparator_``:

.. math::

   c = c^* / \overline{c}^*

.. math::

   D_{\mathrm{eff}} = D_{\mathrm{eff}}^*(c^*) / D_{\mathrm{eff}}^*(\overline{c}^*)

.. math::

   t = \frac{t^* D_{\mathrm{eff}^*}(\overline{c^*})}{{L^*}^2}

.. math::

   x = \frac{x^* - x_0^*}{L^*}

.. math::

   N_+ = \frac{L^* N_+^*}{\overline{c}^* D_{\mathrm{eff}^*}(\overline{c}^*)}

.. math:

   T = \frac{R^* T^*}{F^* \phi_c^*}

where

.. math::

   L^* = x_n^* - x_0^*

and :math:`\phi_c^*` is a user-defined characteristic potential, typically specified in derived classes as 1 V.

This class describes objects that approximately solve

.. math:: 

   \frac{\partial (\epsilon c)}{\partial t} = -\frac{\partial}{\partial x} \left[\frac{N_+}{\nu_+}\right]

using the FVM representation

.. math::
    
   0 = \epsilon \frac{\overline{c}(t,x_i) - \overline{c}(t-\Delta t,x_i)}{\Delta t} (x_{i+1} - x_i) + \left[\frac{N_+}{\nu_+} \right]_{x_i}^{x_{i+1}} + \mathcal{O}(\Delta t)

where

.. math::

   \overline{c}(t,x_i) = \frac{1}{x_{i+1} - x_i} \int_{x_i}^{x_{i+1}} c(t,x) \ dx

.. math::

   \frac{N_+}{\nu_+} = -D_{\mathrm{eff}} \left( 1 - \frac{\partial \ln c_0}{\partial \ln c} \right) \frac{\partial c}{\partial x} + \frac{t_+^0}{z_+ \nu_+} i + c v_0.

It is assumed that the last term is negligible

.. math::

    c v_0 = 0.

At boundaries at which all current is carried by the cations,

.. math::

   \frac{N_+}{\nu_+} = \frac{i}{z_+ \nu_+}.

Parameters
----------
separator_domain : SeparatorDomain
    Storage object for calculations within the particle

porosity : float
    Separator pore volume divided by total volume

T : float
    Dimensionless temperature :math:`T`
    '''

    def __init__(self, grid_assignment, porosity, tortuosity, T, thickness_, electrolyte_solution):

        BasePorousElectrodeBinarySalt_.__init__(self,thickness_,electrolyte_solution,porosity,tortuosity)
        
        assert type(grid_assignment) == dict
        
        self.grid_assignment = grid_assignment
        
        self.porosity = porosity

        self.T = T

        self.inverse_z_plus_nu_plus = 1.0

    def set_initial_conditions(self):

        self.porous_grid = self.grid[self.grid_number_with_name[self.grid_assignment['region']]]
        
        c2 = self.porous_grid.field[0][self.salt_concentration_field_number]
        phi1 = self.porous_grid.field[0][self.matrix_potential_field_number]
        phi2 = self.porous_grid.field[0][self.solution_potential_field_number]

        for i in range(self.porous_grid.unit_count):
            # Concentrations are scaled by initial concentration
            c2[i] = 1.0
            phi1[i] = 0.0
            phi2[i] = 0.0

    def set_equation_scalar_counts(self):
        
        self.equation_scalar_counts = {
            self.governing_equations_at_left_interface:2,
            self.governing_equations_in_bulk: 2,
            self.governing_equations_at_right_interface:2}
        
    def assign_equations(self):

        solution_equations = self.equations[self.porous_grid_number]

        solution_equations[0] = [self.governing_equations_at_left_interface]

        for vol_number in range(1,self.porous_grid.unit_count-1):
            solution_equations[vol_number] = [self.governing_equations_in_bulk]

        # "Right" domain boundary
        solution_equations[-1] = [self.governing_equations_at_right_interface]

    def i(self):
        r'''
Return instantaneous dimensionless conventional current density in the positive x direction

This must be overriden in derived classes. 

Returns
-------
    float
        '''
        raise NotImplementedError

    def left_flux(self,vol):

        c = self.porous_grid.field[0][self.concentration_field_number]
        
        c_left = vol.interpolate_left(c)

        D_eff_left = self.D_eff(c_left)

        return -D_eff_left*vol.dx_left(c) + self.cation_transference_number(c_left)*self.inverse_z_plus_nu_plus*self.i()
    
    def right_flux(self,vol):

        c = self.porous_grid.field[0][self.concentration_field_number]
        
        c_right = vol.interpolate_right(c)

        D_eff_right = self.D_eff(c_right)

        return -D_eff_right*vol.dx_right(c) + self.cation_transference_number(c_right)*self.inverse_z_plus_nu_plus*self.i()
    
    def governing_equations_at_left_interface(self, vol, residual):

        #c_1 = self.left_neighboring_c2(-1)
        #if c_1 == None:
        # doesn't seem like the interpolation makes a difference
        #c_1 = vol.interpolate(self.porous_grid.field[-1][self.concentration_field_number])
        c_1 = self.porous_grid.field[-1][self.concentration_field_number,0]
        
        c = self.porous_grid.field[0][self.concentration_field_number]
        left_neighboring_c2 = self.left_neighboring_c2(0)
        if left_neighboring_c2 == None:
            residual[0] = ((self.left_neighboring_pore_volume() + self.porosity*vol.volume)*(vol.interpolate(c) - c_1)*self.inverse_timestep_size + self.right_flux(vol) - self.left_neighboring_boundary_term())
        else:
            residual[0] = vol.interpolate(c) - self.left_neighboring_c2(0)

    def governing_equations_in_bulk(self, vol, residual):

        c_1 = self.porous_grid.field[-1][self.concentration_field_number]
        c = self.porous_grid.field[0][self.concentration_field_number]

        residual[0] = (self.porosity*vol.volume*(vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size + self.right_flux(vol) - self.left_flux(vol))

    def governing_equations_at_right_interface(self, vol, residual):
        
        c_1 = self.right_neighboring_c2(-1)
        if c_1 == None:
        #c_1 = vol.interpolate(self.porous_grid.field[-1][self.concentration_field_number])
            c_1 = self.porous_grid.field[-1][self.concentration_field_number,-1]
            
        c = self.porous_grid.field[0][self.concentration_field_number]
        
        residual[0] = ((self.porosity*vol.volume + self.right_neighboring_pore_volume())*(vol.interpolate(c) - c_1)*self.inverse_timestep_size + self.right_neighboring_boundary_term() - self.left_flux(vol))
    
    def declare_unknowns(self):

        self.porous_grid_number = self.grid_number_with_name[self.grid_assignment['region']]
        for grid_number in range(len(self.grid)):
            self.unknown[grid_number][:,:] = (grid_number == self.porous_grid_number)

        self.porous_grid = self.grid[self.porous_grid_number]

        self.salt_concentration_field_number = self.porous_grid.field_number_with_name['c2']
        self.matrix_potential_field_number = self.porous_grid.field_number_with_name['phi1']
        self.solution_potential_field_number = self.porous_grid.field_number_with_name['phi2']
        

        for i in range(self.porous_grid.unit_count):
            self.unknown[self.porous_grid_number][self.solution_potential_field_number,i] = False
       
        for i in range(self.porous_grid.unit_count):        
            self.unknown[self.porous_grid_number][self.salt_concentration_field_number,0] = True

    def process_solution(self):

        ## The integration performed here is based on volume "centers," not the boundaries of the control volumes
        
        c_field = self.porous_grid.field[0][self.salt_concentration_field_number]
        phi2_field_number = self.porous_grid.field_number_with_name['phi2']

        self.porous_grid.field[0][phi2_field_number,0] = 0.0
        
        vol = self.porous_grid.unit_with_number[0]
            
        c = vol.interpolate(c_field)

        integrand_left = 2.*self.T*(1.-self.cation_transference_number(c))*(1.+self.electrolyte_solution.thermodynamic_factor(c))/c*vol.dx(c_field) - self.i() / self.kappa_eff(c)
        
        for vol_number in range(1,self.porous_grid.unit_count):

            last_vol = vol
            vol = self.porous_grid.unit_with_number[vol_number]
            
            c = vol.interpolate(c_field)

            integrand_right = 2.*self.T*(1.-self.cation_transference_number(c))*(1.+self.electrolyte_solution.thermodynamic_factor(c))/c*vol.dx(c_field) - self.i() / self.kappa_eff(c)

            # Integration by trapezoidal rule
            self.porous_grid.field[0][phi2_field_number,vol_number] = self.porous_grid.field[0][phi2_field_number,vol_number-1] + 0.5*(vol.coordinate - last_vol.coordinate)*(integrand_left + integrand_right)

            integrand_left = integrand_right

        ## Information will flow from left to right by default

        if isinstance(self.left_neighbor,liblib.common.CellPorousRegion_) or isinstance(self.left_neighbor,liblib.common.CellPorousRegionSimulation_):
            self.porous_grid.field[0][phi2_field_number,:] += self.left_neighboring_phi2()
        elif isinstance(self.right_neighbor,liblib.common.CellPorousRegion_) or isinstance(self.right_neighbor,liblib.common.CellPorousRegionSimulation_):

            self.porous_grid.field[0][phi2_field_number,:] += self.right_neighboring_phi2() - self.porous_grid.field[0][phi2_field_number,-1]
        else:
            print('ERROR: ConcentratedBinarySaltSolutionFilledPorous has no neighboring porous regions.')
            raise AssertionError

    def calculate_global_values(self):

        particle_grid = self.grid[2]
        
        phi1 = self.porous_grid.field[0][0]
        phi2 = self.porous_grid.field[0][1]
        c2 = self.porous_grid.field[0][2]
        
        for electrode_vol in self.porous_grid.unit_with_number:
            particle_vol = particle_grid.unit_with_index[electrode_vol.number,-1]
            c1s = particle_grid.field[0][0,particle_vol.number]        
            self.porous_grid.surface_overpotential[electrode_vol.number] = (electrode_vol.interpolate(phi1) - electrode_vol.interpolate(phi2) - self.Ueq(c1s))
            self.porous_grid.i_n_value[electrode_vol.number] = self.i_n(c1s,electrode_vol.interpolate(c2),self.porous_grid.surface_overpotential[electrode_vol.number])
        
    def electrode_governing_equations_at_positive_current_collector(self, vol, residual):

        electrode_grid = self.grid[0]
        
        phi1 = electrode_grid.field[0][0]
        phi2 = electrode_grid.field[0][1]
        c2_1 = electrode_grid.field[-1][2]
        c2 = electrode_grid.field[0][2]

        # approximate integral of rate source
        integral_of_rate_source = self.Sa*electrode_grid.i_n_value[vol.number]*vol.volume # on discharge, the concentration drops in the solution phase of the electrode, so this has to be negative

        # Zero flux boundary condition at positive current collector
        self.governing_equations_at_left_interface(vol, residual)
        residual[0] -= (1.0-self.tplus)*integral_of_rate_source*self.Lpos**2/self.F
        
        ## Solid phase
        # During cell discharge, self.i_app < 0 and so electrons flow into positive electrode from
        # positive current collector.
        # Electrons flow toward increasing potential, so the potential derivative at the current
        # collector should be positive, so the self.i_app term has the correct sign.
        residual[1] = (self.sigma_eff*vol.dx_right(phi1) + self.i_app*self.Lpos
                       - self.Lpos**2*integral_of_rate_source)

        c2_right = vol.interpolate_right(c2)

        kappa_eff_right = self.kappa(c2_right)*self.eff_factor_pos
        kappa_effd_right = kappa_eff_right*self.effd_factor(c2_right)

        ## Solution phase
        # Zero flux boundary condition at positive current collector
        residual[2] = (-kappa_eff_right*vol.dx_right(phi2) + kappa_effd_right*vol.dx_right(c2)/c2_right - self.Lpos**2*integral_of_rate_source)

    def electrode_governing_equations(self, vol, residual):

        electrode_grid = self.grid[0]

        phi1 = electrode_grid.field[0][0]
        phi2 = electrode_grid.field[0][1]
        c2_1 = electrode_grid.field[-1][2]
        c2 = electrode_grid.field[0][2]        

        # approximate integral of rate source
        integral_of_rate_source = self.Sa*electrode_grid.i_n_value[vol.number]*vol.volume
        
        # Salt concentration
        # dc2/dt will have the same sign as integral_of_rate_source. On discharge, Li+ ions are being
        # absorbed from the solution, so integral_of_rate_source < 0.
        
        # This fills residual[0] with the residual for a separator
        self.governing_equations_in_bulk(vol,residual)

        # To the residual for a separator, one must add a source term describing the outflow of cations from the co-located active material
        residual[0] -= (1.0-self.tplus)*integral_of_rate_source*self.Lpos**2/self.F
        
        ## Solid phase
        residual[1] = (self.sigma_eff*(vol.dx_right(phi1) - vol.dx_left(phi1))
                       - self.Lpos**2*integral_of_rate_source) # checks

        ## Solution phase
        c2_right = vol.interpolate_right(c2)
        c2_left = vol.interpolate_left(c2)

        kappa_eff_left = self.kappa(c2_left)*self.eff_factor_pos
        kappa_eff_right = self.kappa(c2_right)*self.eff_factor_pos
        kappa_effd_left = kappa_eff_left*self.effd_factor(c2_left)
        kappa_effd_right = kappa_eff_right*self.effd_factor(c2_right)
        
        residual[2] = (-kappa_eff_right*vol.dx_right(phi2) + kappa_effd_right*vol.dx_right(c2)/c2_right + kappa_eff_left*vol.dx_left(phi2) - kappa_effd_left*vol.dx_left(c2)/c2_left - self.Lpos**2*integral_of_rate_source) # checks
                
    def electrode_governing_equations_at_separator(self, vol, residual):

        electrode_grid = self.grid[0]

        phi1 = electrode_grid.field[0][0]
        phi2 = electrode_grid.field[0][1]
        c2_1 = electrode_grid.field[-1][2]
        c2 = electrode_grid.field[0][2]        

        # approximate integral of rate source
        integral_of_rate_source = self.Sa*electrode_grid.i_n_value[vol.number]*vol.volume

        # Salt concentration
        # dc2/dt will have the same sign as integral_of_rate_source. On discharge, Li+ ions are being
        # absorbed from the solution, so integral_of_rate_source < 0.

        self.governing_equations_at_right_interface(vol, residual)
        residual[0] -= (1.0-self.tplus)*integral_of_rate_source*self.Lpos**2/self.F        
        
        ## Solid phase
        residual[1] = (self.sigma_eff*vol.dx_left(phi1)
                       + self.Lpos**2*integral_of_rate_source)

        # Boundary conditions for phi2, c2 are not explicitly given in COMSOL
        # The two "half-volumes" here are treated as a "whole-volume" controlled by a single value for each field. The governing equation averages that on the two sides.
        
        c2_left = vol.interpolate_left(c2)
        c2_right = vol.interpolate_right(c2)
                        
        kappa_eff_left = self.kappa(c2_left)*self.eff_factor_pos
        kappa_eff_right = self.kappa(c2_right)*self.eff_factor_pos
        kappa_effd_left = kappa_eff_left*self.effd_factor(c2_left)
        kappa_effd_right = kappa_eff_right*self.effd_factor(c2_right)

        corresponding_solution_vol = self.grid[1].unit_with_number[0]
        corresponding_phi2 = self.grid[1].field[0][0]
        corresponding_c2 = self.grid[1].field[0][1]
        corresponding_c2_left = corresponding_solution_vol.interpolate_left(corresponding_c2)                
        corresponding_kappa_eff_left = self.kappa(corresponding_c2_left)*self.eff_factor_sep
        corresponding_kappa_effd_left = corresponding_kappa_eff_left*self.effd_factor(corresponding_c2_left)
        corresponding_D2_eff_left = self.D2(corresponding_c2_left)*self.eff_factor_sep
        corresponding_c2_right = corresponding_solution_vol.interpolate_right(corresponding_c2)                        
        corresponding_D2_eff_right = self.D2(corresponding_c2_right)*self.eff_factor_sep                  
        corresponding_kappa_eff_right = self.kappa(corresponding_c2_right)*self.eff_factor_sep
        corresponding_kappa_effd_right = corresponding_kappa_eff_right*self.effd_factor(corresponding_c2_right)
        # This is the equation governing the interior of the electrode
        residual[2] = (-(self.Lpos/self.Lsep*(corresponding_kappa_eff_right*corresponding_solution_vol.dx_right(corresponding_phi2) - corresponding_kappa_effd_right*corresponding_solution_vol.dx_right(corresponding_c2)/corresponding_c2_right)) + kappa_eff_left*vol.dx_left(phi2) - kappa_effd_left*vol.dx_left(c2)/c2_left - self.Lpos**2*integral_of_rate_source)
        
    def governing_equations_at_left_interface(self, vol, residual):

        #c_1 = self.left_neighboring_c2(-1)
        #if c_1 == None:
        # doesn't seem like the interpolation makes a difference
        #c_1 = vol.interpolate(self.separator_grid.field[-1][self.salt_concentration_field_number])
        c_1 = self.porous_grid.field[-1][self.salt_concentration_field_number,0]
        
        c = self.porous_grid.field[0][self.salt_concentration_field_number]
        left_neighboring_c2 = self.left_neighboring_c2(0)
        if left_neighboring_c2 == None:
            residual[0] = ((self.left_neighboring_pore_volume() + self.porosity*vol.volume)*(vol.interpolate(c) - c_1)*self.inverse_timestep_size + self.right_boundary_term(vol) - self.left_neighboring_boundary_term())
        else:
            residual[0] = vol.interpolate(c) - self.left_neighboring_c2(0)

    # This method is used to define boundary conditions in which the solution
    # value is specified. This must be consistent with the contents of the
    # 'declare_unknowns' methods defined in the corresponding 'Grid' objects.
    def set_boundary_values(self):

        #### message of consistency check is too cryptic.
        #### this needs to be checked
        #### the neighbors have not yet been established, but they should be
        
        phi2 = self.porous_grid.field[0][0]
        c2 = self.porous_grid.field[0][1]

        # Solution electrochemical potential is continuous at the interface
        # between the electrode and separator
        phi2[0] = self.left_neighbor.get_right_phi2_()

        # The condition for c2 at the interface between electrode and
        # separator is not explicitly given to COMSOL
        c2[0] = self.left_neighbor.get_right_c2_()

        # Electrochemical potential throughout the cell will be measured
        # relative to that at the negative electrode surface.
        phi2[-1] = 0.0        

    def source(self,coordinate):
        return 0
        ### it is flexible, but inefficient, to reference this by coordinate position and interpolate, if we have as many particles as electrode positions. However, this is necessary if we allow these number to be different, although it seems inefficient when there are an excess of particles
        ### Assuming that macroscopic current density is given, it has to be distributed among particles at all positions. The distribution of particles is assumed to be uniform, so one could write microscopic current density as a function of position
        ### the way tthat I think this works is that the overall current density is set at the cell boundaries, and everywhere in the porous electrode, there's a term that describes the current flowing into the particles. The potential in solution is simultaneously solved and ensures zero ionic flux at the current collector. The potential in the matrix is also simultaneously solved and all current is carried electronically at the current collector. So this forces the potentil
        
    def governing_equations_at_right_interface(self, vol, residual):
        
        c_1 = self.right_neighboring_c2(-1)
        if c_1 == None:
        #c_1 = vol.interpolate(self.porous_grid.field[-1][self.salt_concentration_field_number])
            c_1 = self.porous_grid.field[-1][self.salt_concentration_field_number,-1]
            
        c = self.porous_grid.field[0][self.salt_concentration_field_number]
        
        residual[0] = ((self.porosity*vol.volume + self.right_neighboring_pore_volume())*(vol.interpolate(c) - c_1)*self.inverse_timestep_size + self.right_neighboring_boundary_term() - self.left_boundary_term(vol))
    
### maybe we should be separating mathematical model descriptions from the solvers, but sometimes the discretization is closely tied to the submodel ordering, like when information is expected to flow from region to region in a particular order
### seems like output should be handled at the lowest level, but maybe we want multiple models to write to the same grid?

    def i_n(self,c1s,c2,surface_overpotential):
        i0 = self.k0*self.F*((self.c1max-c1s)*c2)**self.alpha_a*c1s**self.alpha_c # discrepancy with COMSOL model parameter, but consistent with COMSOL variable definition
        return i0*(math.exp(surface_overpotential*self.alpha_a*self.F__RT) - math.exp(-surface_overpotential*self.alpha_c*self.F__RT)) # consistent with COMSOL model cathode variable definition, if surface_overpotential = phi1 - phi2 - Ueq

class PorousElectrodeSimulation_(pygdh.Simulation,liblib.common.CellPorousRegionBinarySalt_):

    def __init__(self,name,output_types,representative_particle_count,interior_unit_count,porosity,tortuosity,T,active_material,particle_radius_,active_material_loading_,electrochemical_reaction,electrolyte_solution):

        pygdh.Simulation.__init__(self,name,output_types)

        #### placeholder
        self.capacity_per_area_ = 1.0 
        
        # Representative particles will be evenly spaced

        ## Allocate Grids for particles and matrix

        self.particle_count = representative_particle_count

        #### might want a different name
        self.electrode_domain = PorousElectrodeDomain('region',self.particle_count)
        
        self.interior_domains = [ SingleParticleDomain('interior_' + str(i),interior_unit_count) for i in range(self.particle_count) ]
        
        self.surface_domains = [ SingleParticleSurfaceDomain('surface_' + str(i)) for i in range(self.particle_count) ]        

        grid_sequence = [ self.electrode_domain ]
        grid_sequence.extend(self.interior_domains)
        grid_sequence.extend(self.surface_domains)

        #### placeholder
        thickness = 1.0
        self.electrode_model = ConcentratedBinarySaltSolutionFilledPorousConductiveMatrix_({'region':'region'},porosity,tortuosity,T,thickness,electrolyte_solution)

        self.electrode_model.set_grid(grid_sequence,stored_solution_count=2)

        #### check these arguments and make sure that the arguments to this method are appropriate
        #### could alternatively pass grid indices, but could also reconstruct names from position
        self.particle_models = [ EmbeddedParticle_({'interior':'interior_' + str(i),'surface':'surface_' + str(i)},self.electrode_model,i,active_material,particle_radius_,active_material_loading_,electrochemical_reaction) for i in range(self.particle_count) ]

        submodels = [ self.electrode_model ]
        submodels.extend(self.particle_models)

        self.initialize_simulation(grid_sequence,submodels,stored_solution_count=2)

    def set_neighbors(self,left,right):

        self.electrode_model.set_neighbors(left,right)
        self.right_neighbor = self.electrode_model.right_neighbor
        self.left_neighbor = self.electrode_model.left_neighbor

    #### placeholders
    def pore_volume_(self,unit_number):

        return 1.0

    def boundary_term_(self,unit_number):

        return 1.0

    def c2_(self,relative_time_index,unit_number):

        return 1.0

    def phi2_(self,unit_number):

        return 1.0
        
class LithiumSurfaceDomain(pygdh.Grid):

    def __init__(self, name):

        self.initialize_grid(name, unit_classes=[ pygdh.Volume ])
    
    def define_field_variables(self):

        self.phi_ = numpy.zeros(1)        

    def set_output_fields(self):

        self.output_fields = {
            'phi_ [V]': self.phi_}

## We might have to treat this as a porous region so that it "contains" solution to reference in calculations        
class Lithium(BaseElectrode,pygdh.Model):
    r'''
Parameters
----------
surface_domain : LithiumSurfaceDomain
    Storage object for information at the particle surface

electrochemical_reaction : electrochemical_reaction.BaseElectrochemicalReaction
    Object describing main electrochemical reaction for electrode
    '''

    def __init__(self,surface_domain,electrochemical_reaction):

        self.surface_domain = surface_domain
        #self.solution_domain = solution_domain        
        self.electrochemical_reaction = electrochemical_reaction

    def pore_volume_(self,number):
        # This is not a porous region
        return 0.0

    def boundary_term_(self,number):
        # All current at the interface is carried by cations
        return liblib.common.inverse_F_*self.left_neighbor.get_right_i_()

    def c2_(self,relative_time_index,unit_number):

        return None
    
    def set_initial_conditions(self):
        pass
 
    def set_phi1_(self,phi_):
        self.surface_domain.phi_[0] = phi_
        
    def get_phi_(self):
        return self.surface_domain.phi_[0]

    def i_(self,phi_):
        r'''
This is required to use cell potential for control

Parameters
----------
``phi1_`` : float
    Dimensional terminal potential [V]

Returns
-------
float
    Dimensional current density :math:`[\textrm{A}/\textrm{m}^2]`
        '''

        if isinstance(self.left_neighbor,liblib.common.CellPorousRegionBinarySalt_) or isinstance(self.left_neighbor,liblib.common.CellPorousRegionBinarySaltSimulation_):
            c2_ = self.left_neighbor.c2_(0,-1)
            phi2_ = self.left_neighbor.phi2_(-1)
        elif isinstance(self.right_neighbor,liblib.common.CellPorousRegionBinarySalt_) or isinstance(self.right_neighbor,liblib.common.CellPorousRegionBinarySaltSimulation_):
            c2_ = self.right_neighbor.c2_(0,0)
            phi2_ = self.right_neighbor.phi2_(0)
        else:
            print('ERROR: Lithium has no neighboring porous regions.')
            raise AssertionError
        ### The value of the potential in solution right outside of the electrode should not be this high.
        #print(phi_,phi2_)
        eta_s_ = phi_ - phi2_

        return self.electrochemical_reaction.i_n_(eta_s_,None,c2_)

    def process_solution(self):

        if isinstance(self.left_neighbor,liblib.common.CellPorousRegionBinarySalt_) or isinstance(self.left_neighbor,liblib.common.CellPorousRegionBinarySaltSimulation_):
            c2_ = self.left_neighbor.c2_(0,-1)
            phi2_ = self.left_neighbor.phi2_(-1)
        elif isinstance(self.right_neighbor,liblib.common.CellPorousRegionBinarySalt_) or isinstance(self.right_neighbor,liblib.common.CellPorousRegionBinarySaltSimulation_):
            c2_ = self.right_neighbor.c2_(0,0)
            phi2_ = self.right_neighbor.phi2_(0)
        else:
            print('ERROR: Lithium has no neighboring porous regions.')
            raise AssertionError

        # Either the left or right neighbor will give the same information
        self.surface_domain.phi_[0] = self.electrochemical_reaction.V_(self.left_neighbor.get_right_i_(),None,c2_) + phi2_

class LithiumSimulation(pygdh.Simulation,liblib.common.CellRegionSimulation_):

    def __init__(self,name,output_types,electrolyte_solution,handle_exception_in_solver=True):

        pygdh.Simulation.__init__(self,name,output_types,handle_exception_in_solver=handle_exception_in_solver)

        electrode_reaction = liblib.electrochemical_reaction.Lithium(electrolyte_solution.c_initial_)

        electrode_surface_domain = liblib.electrode.LithiumSurfaceDomain('lithium_surface')

        self.electrode_model = liblib.electrode.Lithium(electrode_surface_domain,electrode_reaction)

        self.initialize_simulation([electrode_surface_domain],[self.electrode_model],stored_solution_count=2)

    def set_neighbors(self,left,right):
        self.electrode_model.set_neighbors(left,right)
        self.right_neighbor = self.electrode_model.right_neighbor
        self.left_neighbor = self.electrode_model.left_neighbor                
        
    def i_(self,phi1_):
        return self.electrode_model.i_(phi1_)

    #### Some of these might not be necessary
    
    def set_phi1_(self,phi_):
        return self.electrode_model.set_phi1_(phi_)

    def phi2_(self,unit_number):
        return self.electrode_model.phi2_(unit_number)

    def c2_(self,relative_time_index,unit_number):

        return self.electrode_model.c2_(relative_time_index,unit_number)

    def pore_volume_(self,number):

        return self.electrode_model.pore_volume_(number)
    
    def boundary_term_(self,number):

        return self.electrode_model.boundary_term_(number)

    def get_phi_(self):
        
        return self.electrode_model.get_phi_()
    
    def get_left_i_(self):

        return self.electrode_model.get_left_i_()

    def get_right_i_(self):

        return self.electrode_model.get_right_i_()    

