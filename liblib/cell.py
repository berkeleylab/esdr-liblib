import pygdh

class CellSimulation(pygdh.Simulation):

    def __init__(self,name,output_types,layout,submodels,domains=[],handle_exception_in_solver=False,write_initial_fields=True):

        pygdh.Simulation.__init__(self,name,output_types,handle_exception_in_solver=handle_exception_in_solver,write_initial_fields=write_initial_fields)

        self.initialize_simulation(domains,submodels,stored_solution_count=2)

        self.set_layout(layout)

    def set_layout(self,layout):
        '''
Parameters
----------
layout: list or tuple
        Specified models describe cell regions in order from right to left, corresponding to first to last elements of sequence

Returns
-------
'''
        len_layout = len(layout)
        if len_layout == 1:
            layout[0].set_neighbors(layout[0],layout[0])
        elif len_layout >= 2:
            layout[0].set_neighbors(layout[-1],layout[1])
            for model_number in range(1,len_layout-1):
                layout[model_number].set_neighbors(layout[model_number-1],layout[model_number+1])
            layout[len_layout-1].set_neighbors(layout[len_layout-2],layout[0])
