import liblib.common
import math
import numpy
import scipy.optimize

class BaseElectrochemicalReaction:
    def __init__(self,characteristic_potential_=1.0):

        self.characteristic_potential_ = characteristic_potential_
        self.inverse_characteristic_potential_ = 1./self.characteristic_potential_
        
        T_ = 298.15 # K
        self.F__RT_ = liblib.common.F_ / (liblib.common.R_*T_) # C/J = 1/V
        self.RT__F_ = 1.0 / self.F__RT_ # V

    def set_initial_conditions(self):
        pass

    def set_equation_scalar_counts(self):
        pass    
            
class BaseButlerVolmer(BaseElectrochemicalReaction):

    def __init__(self, n, beta, k0_, characteristic_potential_=1.0):

        BaseElectrochemicalReaction.__init__(self,characteristic_potential_)
        
        self.n = n # number of electrons transferred in reaction
        
        self.beta = beta # symmetry factor

        self.alpha_a = (1. - self.beta)*self.n # apparent anodic transfer coefficient
        self.alpha_c = self.beta*self.n # apparent cathodic transfer coefficient

        self.k0_ = k0_

    def V_(self,i_n_,c1_,c2_,lower_bound=0,upper_bound=5.0):
        r'''
This must be overridden in derived classes. 

Parameters
----------
``i_n_`` : float
    Dimensional current density on active material :math:``[\textrm{A}/\textrm{m}^2]``

``c1_`` : float
    Dimensional lithium concentration in active material :math:``[\textrm{mol}/\textrm{m^3}]``

``c2_`` : float
    Dimensional salt concentration in electrolyte solution :math:``[\textrm{mol}/\textrm{m^3}]``

lower_bound : float
    Lower bound on solution search interval

upper_bound : float
    Upper bound on solution search interval

Returns
-------
    float
        Dimensional equilibrium potential [V]
        '''

        raise NotImplementedError

    def i_n_(self,eta_s_,c1_,c2_):
        r'''
This must be overridden in derived classes. 

Parameters
----------
``V_`` : float
    Dimensional surface potential [V]

``c1_`` : float
    Dimensional lithium concentration in active material :math:``[\textrm{mol}/\textrm{m^3}]``

``c2_`` : float
    Dimensional salt concentration in electrolyte solution :math:``[\textrm{mol}/\textrm{m^3}]``

Returns
-------
    float
        Dimensional current density on active material :math:``[\textrm{A}/\textrm{m}^2]``
        '''

        raise NotImplementedError
    
class Lithium(BaseButlerVolmer):

    def __init__(self,c2_initial_,characteristic_potential_=1.0):

        n = 1

        # This is in part already built in as an assumption in the equations of this class
        beta = 0.5

        k0_ = 3e-6 # mol^{1/2} / (m^{1/2} s)
        
        BaseButlerVolmer.__init__(self, n, beta, k0_, characteristic_potential_)

    def U_(self,c1_):
        r'''
Parameters
----------
    c1_ : Dimensional lithium concentration in active material

Returns
-------
    float
        Dimensional equilibrium potential [V]
        '''
        return 0.
        
    def V_(self,i_n_,c1_,c2_): # V

        eta_s_ = -2*self.RT__F_*math.asinh(0.5*i_n_*self.k0_*liblib.common.F_*math.sqrt(c2_))

        return eta_s_ # U_ = 0

    def i_n_(self,eta_s_,c1_,c2_):

        # c1_ is not applicable here
        i0_ = 2.*self.k0_*liblib.common.F_*c2_**self.alpha_a

        # Here, a positive current density corresponds to an anodic reaction, negative to cathodic
        return i0_*math.sinh(-eta_s_*self.alpha_a*self.F__RT_)
    
class ButlerVolmer_(BaseButlerVolmer):

    def __init__(self, n, beta, k0_, active_material, particle_radius_, c2_initial_, characteristic_potential_=1.0):

        BaseButlerVolmer.__init__(self,n,beta,k0_,characteristic_potential_)
        
        self.active_material = active_material
        
        self.c2_initial_ = c2_initial_

        self.particle_radius_ = particle_radius_
        self.inverse_particle_radius_ = 1./self.particle_radius_

    def U_(self,c1_):
        r'''
This must be overridden in derived classes. 

Parameters
----------
    c1_ : Dimensional lithium concentration in active material

Returns
-------
    float
        Dimensional equilibrium potential
        '''

        raise NotImplementedError

    def i_n_(self,eta_s_,c1_,c2_):

        i0_ = self.k0_*liblib.common.F_*((self.active_material.c_max_ - c1_)*c2_)**self.alpha_a * c1_**self.alpha_c

        # Here, a positive current density corresponds to an anodic reaction, negative to cathodic

        #### Might want to consider constructing an alternative residual form using logarithms since some of the values can get quite large.
        return i0_*(math.exp(eta_s_*self.alpha_a*self.F__RT_) - math.exp(-eta_s_*self.alpha_c*self.F__RT_))

    def residual(self,eta_s_,c1_,c2_,i_n_):

        return self.i_n_(eta_s_,c1_,c2_) - i_n_
    
    def eta_s_(self,c1_,c2_,i_n_,lower_bound=-5.0,upper_bound=5.0): 

        return scipy.optimize.brentq(self.residual,lower_bound,upper_bound,args=(c1_,c2_,i_n_),disp=True)
    
    def V_(self,i_n_,c1_,c2_,lower_bound=-5.0,upper_bound=5.0): # V
        r'''
Required for ``electrode`` postprocessing calculations

Parameters
----------
i_n_ : float
    dimensional current density :math:``[\textrm{A}/\textrm{m}^2]``

c1_ : float
    dimensional lithium concentration in active material :math:``[\textrm{mol}/\textrm{m}^3]``

c2_ : float
    dimensional salt concentration in electrolyte solution :math:``[\textrm{mol}/\textrm{m}^3]``

Returns
-------
float
    Surface overpotential [V]
        '''

        return self.eta_s_(c1_,c2_,i_n_,lower_bound=lower_bound,upper_bound=upper_bound) + self.U_(c1_)

class NMC333Reaction(ButlerVolmer_):

    def __init__(self,n,beta,active_material,particle_radius_,c2_initial_,characteristic_potential_=1.0):

        k0_ = 1.15e-10 # 1/(s mol^{1/2})
        
        ButlerVolmer_.__init__(self,n,beta,k0_,active_material,particle_radius_,c2_initial_,characteristic_potential_=characteristic_potential_)

    def U_(self,c1_):
        theta = c1_/self.active_material.c_max_
        return 0.60826E1+(-0.69922E1+(0.71062E1-0.25947E1*theta)*theta)*theta-0.54549E-4*math.exp(0.12423E3*theta-0.1142593002E3) # V
