import math
import pygdh
    
class BaseActiveMaterial:
    r'''
Minimal description for active material.

Parameters
----------
``c_min_`` : float
    Minimium dimensional lithium concentration :math:`[\textrm{mol}/\textrm{m}^3]`

``c_initial_`` : float
    Initial dimensional lithium concentration :math:`[\textrm{mol}/\textrm{m}^3]`

``c_max_`` : float
    Maximum dimensional lithium concentration :math:`[\textrm{mol}/\textrm{m}^3]`
    '''

    def __init__(self,density_,c_min_,c_initial_,c_max_):

        self.density_ = density_
        self.inverse_density_ = 1./self.density_
        self.c_min_ = c_min_
        self.inverse_c_min_ = 1./self.c_min_
        self.c_initial_ = c_initial_
        self.inverse_c_initial_ = 1./self.c_initial_        
        self.c_max_ = c_max_
        self.inverse_c_max_ = 1./self.c_max_
        self.D_at_c_initial_ = self.D_(self.c_initial_)
        self.inverse_D_at_c_initial_ = 1.0/self.D_at_c_initial_
        
    def D_(self,c_):
        r'''
This must be overriden in derived classes. 

Parameters
----------
``c_`` : float
    Dimensional lithium concentration :math:`[\textrm{mol}/\textrm{m}^3]`

Returns
-------
float
    Dimensional lithium diffusivity :math:`[\textrm{m}^2/\textrm{s}]`
        '''
        raise NotImplementedError

class NMC333Material(BaseActiveMaterial):
    r'''
Description for NMC333 active material, from S.-L. Wu, W. Zhang, X. Song, A. K. Shukla, G. Liu, V. Battaglia, and V. Srinivasan, J. Electrochem. Soc., 159(4), A438 (2012).

Parameters
----------
``c_initial_`` : float
    Initial dimensional lithium concentration :math:`[\textrm{mol}/\textrm{m}^3]`
    '''
    def __init__(self,c_initial_=18078.97852876613):

        density_ = 4.68e3 # kg / m^3        
        c_min_ = 18078.97852876613 # mol / m^3
        c_max_ = 46715.70679267733 # mol / m^3
        BaseActiveMaterial.__init__(self,density_,c_min_,c_initial_,c_max_)
    
    def D_(self,c_):
        SODs2 = (c_/self.c_max_-0.387)/(1-0.387)
        return (1.475593E-14*SODs2-3.516466E-14)*SODs2+2.058506E-14 # m^2 / s
