import liblib
import sys

### Describe the overall simulation structure

class CustomCellSimulation(liblib.cell.CellSimulation):

    def __init__(self,capacity_limiting_electrode,circuit,layout,submodels,**keywords):

        self.circuit = circuit
        
        C_rate = 1.0 # 1/h
        C_rate_ = -C_rate / 3600. # 1 / s
        # This is only used when simulations are controlled through current density
        self.current_density_ = capacity_limiting_electrode.capacity_per_area_*C_rate_ # A / m^2        
        print('Current density:', self.current_density_)

        # The simulation name can be set manually, but here it is taken automatically from the name of the present file`
        simulation_name = sys.argv[0].rsplit('.')[0]
        output_types = ['GNUPLOT']

        liblib.cell.CellSimulation.__init__(self,simulation_name,output_types,layout,submodels,**keywords)

    def pre_step_hook(self):

        # The dimensional time is available here as self.time[0]
        t_ = self.time[0]

        # Current density specified over this timestep
        
        self.circuit.set_i_(self.current_density_)
        
        # Cell potential specified over this timestep

        #self.circuit.set_V_(4.2)
    
### Describe simulation components
        
# Define working space for cell control and monitoring

cell_domain = liblib.interface.CellDomain('cell')        

# Choose an electrolyte solution

c2_initial_ = 500.0 # mol / m^3
electrolyte_solution = liblib.electrolyte_solution.LiPF6_EC_EMC_3_7_w(c2_initial_)
#electrolyte_solution = liblib.electrolyte_solution.LiPF6_EC_DEC_1_1_w(c2_initial_)
#electrolyte_solution = liblib.electrolyte_solution.TestBinarySaltElectrolyteSolution()

## Separator submodel

separator_unit_count = 9

# Choose a model to describe the internal operation of the separator

separator_thickness_ = 50e-6 # m
porosity = 0.41
tortuosity = 3.809116
separator_simulation = liblib.separator.ConcentratedBinarySaltSolutionFilledSeparatorSimulation_('separator',['GNUPLOT'],separator_unit_count,porosity,tortuosity,separator_thickness_,electrolyte_solution,handle_exception_in_solver=False)

## Positive electrode submodel

representive_particle_count = 25

# Choose a positive electrode active material

positive_electrode_active_material = liblib.active_material.NMC333Material()

# Choose the dominant electrochemical reaction for the positive electrode

particle_radius_ = 1.5e-6 # m
positive_electrode_reaction = liblib.electrochemical_reaction.NMC333Reaction(1,0.5,positive_electrode_active_material,particle_radius_,electrolyte_solution.c_initial_)

# Choose a model to describe the internal operation of the positive electrode

#active_material_loading_ = 0.49*1e-6*100**2 # kg/m^2, a thin electrode
active_material_loading_ = 13.5*1e-6*100**2 # kg/m^2, a thick electrode

representative_particle_count = 5
positive_electrode_simulation = liblib.electrode.PorousElectrodeSimulation_('positive_electrode',['GNUPLOT'],representative_particle_count,representative_particle_count,0.5,4,273,positive_electrode_active_material,particle_radius_,active_material_loading_,positive_electrode_reaction,electrolyte_solution)

## Negative electrode submodel

# Choose the dominant electrochemical reaction for the negative electrode
negative_electrode_simulation = liblib.electrode.LithiumSimulation('negative_electrode',['GNUPLOT'],electrolyte_solution)

## Create object for simulation control

circuit = liblib.circuit.Circuit()

### Connect the simulation components

# Describe physical layout of cell
layout = [circuit,negative_electrode_simulation,separator_simulation,positive_electrode_simulation]

# The physical layout might not be the order in which we want to solve.
submodels = [separator_simulation,negative_electrode_simulation,positive_electrode_simulation]

# Create an object to coordinate all cell component behavior
simulation = CustomCellSimulation(positive_electrode_simulation,circuit,layout,submodels)

### Perform the simulation, cycling through the various submodels

timestep_size = 1e1 # s
simulation.solve_time_dependent_system(timestep_count=10, timestep_size=timestep_size)
