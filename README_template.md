# README #

### What is this repository for? ###

* LIBlib is a Python 2 / Python 3 library for macrohomogenous simulation of lithium ion batteries, designed with a focus on flexibility and extensibility.
* Please note that this software is at a very early stage. Documentation and updates will be added to the repository as they are developed.
* Official releases and documentation will be available on the Downloads page.

### How do I get set up? ###

* The documentation includes installation details.
* Python 2 or Python 3, NumPy, SciPy, and PyGDH version PYGDH_VERSION are required.
* Cython, GNUPLOT, and h5py / HDF5 are recommended.
* To use the repository, create the root directory of the intended repository as well as a `doc` subdirectory and run `sphinx-quickstart` within the `doc` subdirectory if you intend to build the documentation yourself. Clone the repository into the root directory as usual.
* See liblib/examples/sample.py for a simple example of how to use LIBlib to simulate a cell discharge.

### Contribution guidelines ###

* Solvers for governing equations, at a minimum, should be covered by unit tests.
* `tests.py` currently contains fundamental unit tests.
* Detailed docstrings should be provided for all modules and methods.

### Whom do I talk to? ###

* Kenneth Higa <khiga@lbl.gov>
