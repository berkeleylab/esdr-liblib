import math
import pygdh
    
class BaseActiveMaterial:
    r'''
Minimal description for active material.

Parameters
----------
``c_min_`` : float
    Minimium dimensional lithium concentration :math:`[\textrm{mol}/\textrm{m}^3]`

``c_initial_`` : float
    Initial dimensional lithium concentration :math:`[\textrm{mol}/\textrm{m}^3]`

``c_max_`` : float
    Maximum dimensional lithium concentration :math:`[\textrm{mol}/\textrm{m}^3]`
    '''

    def __init__(self,density_,c_min_,c_initial_,c_max_):

        self.density_ = density_
        self.inverse_density_ = 1./self.density_
        self.c_min_ = c_min_
        self.inverse_c_min_ = 1./self.c_min_
        self.c_initial_ = c_initial_
        self.inverse_c_initial_ = 1./self.c_initial_        
        self.c_max_ = c_max_
        self.inverse_c_max_ = 1./self.c_max_
        self.D1_at_c_initial_ = self.D1_(self.c_initial_)
        self.inverse_D1_at_c_initial_ = 1.0/self.D1_at_c_initial_
        
    def D1_(self,c_):
        r'''
This must be overriden in derived classes. 

Parameters
----------
``c_`` : float
    Dimensional lithium concentration :math:`[\textrm{mol}/\textrm{m}^3]`

Returns
-------
float
    Dimensional lithium diffusivity :math:`[\textrm{m}^2/\textrm{s}]`
        '''
        raise NotImplementedError

class NMC333Material(BaseActiveMaterial):
    r'''
Description for NMC333 active material, from S.-L. Wu, W. Zhang, X. Song, A. K. Shukla, G. Liu, V. Battaglia, and V. Srinivasan, J. Electrochem. Soc., 159(4), A438 (2012).

Parameters
----------
``c_initial_`` : float
    Initial dimensional lithium concentration :math:`[\textrm{mol}/\textrm{m}^3]`
    '''
    def __init__(self,c_initial_=18078.97852876613):

        density_ = 4.68e3 # kg / m^3        
        c_min_ = 18078.97852876613 # mol / m^3
        c_max_ = 46715.70679267733 # mol / m^3
        BaseActiveMaterial.__init__(self,density_,c_min_,c_initial_,c_max_)
    
    def D1_(self,c_):
        SODs2 = (c_/self.c_max_-0.387)/(1-0.387)
        return (1.475593E-14*SODs2-3.516466E-14)*SODs2+2.058506E-14 # m^2 / s
    
# ### edit from this point    
# class ParticleSurfaceDomain(pygdh.Grid):

#     # This method is automatically called immediately after an object of this
#     # class is created. Users are free to modify this method definition as
#     # desired, as long as the 'self.initialize_grid' method is called with
#     # appropriate parameter values at some point in this method.
#     def __init__(self, name):

#         self.initialize_grid(name,
#                              field_names=['Gamma'],
#                              coordinate_values={},
#                              # Each unit in 'Domain' will be described by a
#                              # 'Volume' object
#                              unit_classes=[ pygdh.Volume ])
    
# class NMC:

#     def Ueq(self,cs):
#         x_s = cs/self.cmax
#         return 0.6379 + 0.5416*math.exp(-305.5309*x_s) + 0.044*math.tanh((0.1958-x_s)/0.1088) - 0.1978*math.tanh((x_s - 1.0571)/0.0854) - 0.6875*math.tanh((x_s+0.0117)/0.0529) - 0.0175*math.tanh((x_s - 0.5692)/0.0875)

#     def D_Li(self,c):
#         return 2e-14 # m^2/s
        
# class Graphite:

#     def __init__(self):
#         # Graphite, from M. Safari and C. Delacourt, JES, 2011, 158, 5, A562-A571

#         # particle mass per area
#         self.particle_mass_loading = 0.1 # kg / m^
#         particle_density = 100.0 # kg / m^3
#         particle_radius = 1e-6 # m
#         particle_height = 1e-6 # m
#         particle_volume = math.pi*particle_radius**2*particle_height
#         particle_number_density = particle_mass_loading/(particle_volume*particle_density)

#         rho = 2.26e3 # kg/m^3

#         self.rp = 5e-6 # m, from Gao
        
#         self.Sa = 3.0*eps_active/self.rp # m^2/m^3 specific surface area of electrode, from COMSOL model

#         particle_inactive_surface_grid = ParticleSurfaceDomain('inactive')
#         particle_active_surface_grid = ParticleSurfaceDomain('active')
#         particle_interior_grid = ParticleInteriorDomain('interior',particle_unit_count)

#         # Graphite, from M. Safari and C. Delacourt, JES, 2011, 158, 5, A562-A571
#         self.cmax = 31370.0 # mol / m^3
#         c_int = 500.0 # mol / m^3

#         particle_inactive_surface_grid = self.grid[0]
#         particle_active_surface_grid = self.grid[1]
#         particle_interior_grid = self.grid[2]
        
# #         Gamma = particle_inactive_surface_grid.field[0][0]
# #         Gamma[0] = self.Gamma_inactive_init

# #         Gamma = particle_active_surface_grid.field[0][0]        
# #         Gamma[0] = self.Gamma_active_init        

#     def set_equation_scalar_counts(self):
#         self.equation_scalar_counts = {
#             self.particle_equations:1,
#             self.particle_equations_zero_flux_left_boundary:1,
#             self.particle_equations_zero_flux_down_boundary:1,
#             self.particle_equations_zero_flux_right_boundary:1,
#             self.particle_equations_up_left_boundary:1,
#             self.particle_equations_up_right_boundary:1,
#             self.particle_equations_down_left_boundary:1,
#             self.particle_equations_down_right_boundary:1,
#             self.particle_equations_influx_boundary:1}        

#         particle_grid = self.grid[2]
        
#     def particle_inactive_surface_equation(self, vol, residual):

#         Gamma = self.grid[0].field[0][0,0]
#         Gamma_last = self.grid[0].field[-1][0,0]
#         residual[0] = (Gamma - Gamma_last)*self.inverse_timestep_size = 

#     def particle_active_surface_equation(self, vol, residual):

#         pass

#     def particle_interior_inner_equation(self, vol, residual):

#         # Defined for clarity
#         c_1 = self.grid[2].field[-1][0]        
#         c = self.grid[2].field[0][0]

#         c_left = vol.interpolate_left(c)
#         c_right = vol.interpolate_right(c)        
#         c_up = vol.interpolate_up(c)        
#         c_down = vol.interpolate_down(c)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]
        
#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size + (-x2**2*self.D_Li(c_right)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_right(c) + x1**2*self.D_Li(c_left)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_left(c))*vol.length[1] + (-y2**2*self.D_Li(c_up)/self.rp*vol.dy_up(c))*vol.length[0]

#         # Defined for clarity
#         c_1 = self.grid[2].field[-1][0]        
#         c = self.grid[2].field[0][0]

#         c_left = vol.interpolate_left(c)
#         c_right = vol.interpolate_right(c)        
#         c_up = vol.interpolate_up(c)        
#         c_down = vol.interpolate_down(c)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]
        
#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size + (-x2**2*self.D_Li(c_right)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_right(c))*vol.length[1] + (-y2**2*self.D_Li(c_up)/self.rp*vol.dy_up(c) + y1**2*self.D_Li(c_down)/self.rp*vol.dy_down(c))*vol.length[0]

#         # Defined for clarity
#         c_1 = self.grid[2].field[-1][0]        
#         c = self.grid[2].field[0][0]

#         c_left = vol.interpolate_left(c)
#         c_right = vol.interpolate_right(c)        
#         c_up = vol.interpolate_up(c)        
#         c_down = vol.interpolate_down(c)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]
        
#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size + (x1**2*self.D_Li(c_left)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_left(c))*vol.length[1] + (-y2**2*self.D_Li(c_up)/self.rp*vol.dy_up(c) + y1**2*self.D_Li(c_down)/self.rp*vol.dy_down(c))*vol.length[0]

#         # Defined for clarity
#         c_1 = self.grid[2].field[-1][0]        
#         c = self.grid[2].field[0][0]

#         c_left = vol.interpolate_left(c)
#         c_right = vol.interpolate_right(c)        
#         c_up = vol.interpolate_up(c)        
#         c_down = vol.interpolate_down(c)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]
        
#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size + (-x2**2*self.D_Li(c_right)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_right(c))*vol.length[1] + (-y2**2*self.D_Li(c_up)/self.rp*vol.dy_up(c))*vol.length[0]

#         # Defined for clarity
#         c_1 = self.grid[2].field[-1][0]        
#         c = self.grid[2].field[0][0]

#         c_left = vol.interpolate_left(c)
#         c_right = vol.interpolate_right(c)        
#         c_up = vol.interpolate_up(c)        
#         c_down = vol.interpolate_down(c)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]

#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size + (x1**2*self.D_Li(c_left)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_left(c))*vol.length[1] + (-y2**2*self.D_Li(c_up)/self.rp*vol.dy_up(c))*vol.length[0]
        
#     def particle_interior_bulk_equation(self, vol, residual):

#         # Defined for clarity
#         c_1 = self.grid[2].field[-1][0]        
#         c = self.grid[2].field[0][0]

#         c_left = vol.interpolate_left(c)
#         c_right = vol.interpolate_right(c)        
#         c_up = vol.interpolate_up(c)        
#         c_down = vol.interpolate_down(c)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]
        
#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size + (-x2**2*self.D_Li(c_right)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_right(c) + x1**2*self.D_Li(c_left)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_left(c))*vol.length[1] + (-y2**2*self.D_Li(c_up)/self.rp*vol.dy_up(c) + y1**2*self.D_Li(c_down)/self.rp*vol.dy_down(c))*vol.length[0]
    
#     def particle_interior_outer_equation(self, vol, residual):

#         electrode_grid = self.grid[0]

#         # Defined for clarity
#         c_1 = self.grid[2].field[-1][0]        
#         c = self.grid[2].field[0][0]

#         c_left = vol.interpolate_left(c)
#         c_right = vol.interpolate_right(c)        
#         c_up = vol.interpolate_up(c)        
#         c_down = vol.interpolate_down(c)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]

#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size + (-x2**2*self.D_Li(c_right)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_right(c) + x1**2*self.D_Li(c_left)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_left(c))*vol.length[1] + (electrode_grid.i_n_value[vol.index[0]]/self.F + y1**2*self.D_Li(c_down)/self.rp*vol.dy_down(c))*vol.length[0]

#         electrode_grid = self.grid[0]

#         # Defined for clarity
#         c_1 = self.grid[2].field[-1][0]        
#         c = self.grid[2].field[0][0]

#         c_left = vol.interpolate_left(c)
#         c_right = vol.interpolate_right(c)        
#         c_up = vol.interpolate_up(c)        
#         c_down = vol.interpolate_down(c)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]

#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size + (-x2**2*self.D_Li(c_right)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_right(c))*vol.length[1] + (electrode_grid.i_n_value[vol.index[0]]/self.F + y1**2*self.D_Li(c_down)/self.rp*vol.dy_down(c))*vol.length[0]

#         electrode_grid = self.grid[0]

#         # Defined for clarity
#         c_1 = self.grid[2].field[-1][0]        
#         c = self.grid[2].field[0][0]

#         c_left = vol.interpolate_left(c)
#         c_right = vol.interpolate_right(c)        
#         c_up = vol.interpolate_up(c)        
#         c_down = vol.interpolate_down(c)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]
        
#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size + (x1**2*self.D_Li(c_left)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_left(c))*vol.length[1] + (electrode_grid.i_n_value[vol.index[0]]/self.F + y1**2*self.D_Li(c_down)/self.rp*vol.dy_down(c))*vol.length[0]
    
#             particle_vol = particle_grid.unit_with_index[electrode_vol.number,-1]
#             cs = particle_grid.field[0][0,particle_vol.number]        

# particle_unit_count,
