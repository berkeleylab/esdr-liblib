import pygdh

class CellSimulation(pygdh.Simulation):

    def set_layout(self,layout):
        '''
Parameters
----------
layout: list or tuple
        Specified models describe cell regions in order from right to left, corresponding to first to last elements of sequence

Returns
-------
'''
        len_layout = len(layout)
        assert len_layout >= 2
        layout[0].set_neighbors(None,layout[1])
        for model_number in range(1,len_layout-1):
            layout[model_number].set_neighbors(layout[model_number-1],layout[model_number+1])
        layout[len_layout-1].set_neighbors(layout[len_layout-2],None)
