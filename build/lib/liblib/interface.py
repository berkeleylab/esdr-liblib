import pygdh
import numpy
import scipy

class InterfaceDomain(pygdh.Grid):
    r'''
Describes objects used to transfer information between ``Model`` objects

Parameters
----------
name : str
    This identifier will be used to name output files and can also be used to reference the object in a user-friendly way
    '''

    def __init__(self, name):

        self.initialize_grid(name,
                             field_names=['phi1_','phi2_','c1_','c2_'],
                             unit_classes=[ pygdh.Volume ],
                             stored_solution_count=2)
        
        self.active_field = [ False for i in range(self.field_count) ]
        
    def define_field_variables(self):

        self.V_ = numpy.empty(1)        
        self.eta_s_ = numpy.empty(1)
        
    def set_output_fields(self):

        self.output_fields = {
            'V_ [V]' : self.V_,            
            'eta_s_ [V]' : self.eta_s_}
                    
class CellDomain(pygdh.Grid):
    r'''
Describes objects used to store calculations for ``Cell``

Parameters
----------
name : str
    This identifier will be used to name output files and can also be used to reference the object in a user-friendly way
    '''

    def __init__(self, name):

        self.initialize_grid(name,
                             field_names=['i_', 'V_'],
                             unit_classes=[ pygdh.Volume ],
                             stored_solution_count=2)

class Cell(pygdh.Model):
    r'''
Minimal descriptions of objects needed to monitor and control electrochemical cell operation

Parameters
----------
cell_domain : CellDomain
    Storage object for cell-level observables and settings

negative_electrode_model : pygdh.Model
    Object describing mathematical model that represents negative electrode

positive_electrode_model : pygdh.Model
    Object describing mathematical model that represents positive electrode
    '''

    def __init__(self,negative_electrode_model,positive_electrode_model):

        self.negative_electrode_model = negative_electrode_model
        self.positive_electrode_model = positive_electrode_model        
                
    def set_initial_conditions(self):
        pass
            
    def set_equation_scalar_counts(self):
        pass
    
    def declare_unknowns(self):
        pass
    
class CellSetCurrentDensity_(Cell):
    r'''
Descriptions of objects needed to monitor electrochemical cell operation and directly control current density

Parameters
----------
cell_domain : CellDomain
    Storage object for cell-level observables and settings

negative_electrode_model : pygdh.Model
    Object describing mathematical model that represents negative electrode

positive_electrode_model : pygdh.Model
    Object describing mathematical model that represents positive electrode
    '''

    def __init__(self,negative_electrode_model,positive_electrode_model):

        Cell.__init__(self,negative_electrode_model,positive_electrode_model)

    def process_solution(self):

        V__negative = self.negative_electrode_model.get_phi_()
        V__positive = self.positive_electrode_model.get_phi_()

        V__field_number = self.grid[0].field_number_with_name['V_']

        self.grid[0].field[0][V__field_number,0] = V__positive - V__negative
        
class CellSetPotential_(Cell):
    r'''
Descriptions of objects needed to monitor electrochemical cell operation and directly control cell potential

Parameters
----------
cell_domain : CellDomain
    Storage object for cell-level observables and settings

negative_electrode_model : pygdh.Model
    Object describing mathematical model that represents negative electrode

positive_electrode_model : pygdh.Model
    Object describing mathematical model that represents positive electrode
    '''

    def __init__(self,negative_electrode_model,positive_electrode_model,positive_terminal_potential_lower_bound=4.0,positive_terminal_potential_upper_bound=5.):

        self.positive_terminal_potential_lower_bound = positive_terminal_potential_lower_bound # V
        self.positive_terminal_potential_upper_bound = positive_terminal_potential_upper_bound # V

        Cell.__init__(self,negative_electrode_model,positive_electrode_model)

    def declare_unknowns(self):
        pass

    def set_initial_conditions(self):
        pass
            
    def set_boundary_values(self):
        pass
       
    def set_equation_scalar_counts(self):
        # While one could use the general solver to obtain i_, this is a
        # one-dimensional model and it is more efficient to use a specialized
        # solver in postprocessing
        pass

    def residual(self,phi__positive_,V_):

        return self.positive_electrode_model.i_(phi__positive_) - self.negative_electrode_model.i_(phi__positive_-V_)

    def process_solution(self):

        cell_domain = self.grid_with_name['cell']
        V__field_number = cell_domain.field_number_with_name['V_']
        V_ = cell_domain.field[0][V__field_number,0]

        #### This algorithm seems to evaluate the residual at the given bounds, which could conceivably lead to overflows in some situations. Might also want to consider working with an alternative form of the residual using logarithms.
        phi1_ = scipy.optimize.brentq(self.residual,self.positive_terminal_potential_lower_bound,self.positive_terminal_potential_upper_bound,args=(V_,),disp=False)
        
        self.positive_electrode_model.set_phi1_(phi1_)

        self.negative_electrode_model.set_phi1_(phi1_ - V_)

        i__field_number = cell_domain.field_number_with_name['i_']

        cell_domain.field[0][i__field_number,0] = self.positive_electrode_model.i_(phi1_)
