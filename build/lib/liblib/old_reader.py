# examine LNTMFO113018-012419Brate[12].tif, LNTMFO113018-012419B.tif, LNTMFO113018-012419CV.tif, LNTMFO-113018-120618A-hppc.tif for OCV, LNTMFO113018-012419Bp.009.xlsx, LNTMFO-113018-012419CV.xlsx, LNTMFO-113018-120618A-hppc.xlsx for OCV
# Can take current (density?) information from the files and calculate concentration distributions, then try to match concentration information and cell potential with reaction models.
# loading with LibreOffice, saving as UTF-8 CSV with , as field delimiter, " as text delimiter, cell content saved as shown, all text cells quoted.

import csv
import numpy
import sys
if sys.version_info.major == 2:
    import cPickle as pickle
else:
    import pickle
from functools import reduce

def csv_to_pickle(csv_filename,header_line_count,starting_column,keys,types,pickle_filename_root,delimiter=','):
    assert len(keys) == len(types)
    range_len_keys = range(len(keys))
    first_None = [ None for key_index in range_len_keys ]
    with open(csv_filename,'r') as csv_file:
        # skip header
        for index in range(header_line_count):
            csv_file.readline()
        csv_reader = csv.reader(csv_file,delimiter=delimiter)
        cv_data = []
        record_index = 0
        for record in csv_reader:
            converted = []
            for key_index in range_len_keys:
                candidate = record[starting_column + key_index]
                if keys[key_index] != None and candidate != '':
                    comma_split = candidate.split(',')
                    if len(comma_split) == 1:
                        converted.append(types[key_index](candidate))
                    else:
                        converted.append(types[key_index](reduce(lambda x,y: x+y,comma_split)))
                else:
                    if first_None[key_index] == None:
                        first_None[key_index] = record_index
                    converted.append(None)
            record_index += 1
            cv_data.append(converted)

        data = {}
        for key_index in range_len_keys:
            key = keys[key_index]
            first_None_index = first_None[key_index]
            if first_None_index == None:
                data[key] = numpy.empty(record_index,dtype=types[key_index])
                for index in range(record_index):
                    data[key][index] = cv_data[index][key_index]
            else:
                data[key] = numpy.empty(first_None_index,dtype=types[key_index])
                for index in range(first_None_index):
                    data[key][index] = cv_data[index][key_index]

        with open(pickle_filename_root + '.pkl','wb') as pickle_file:
            pickle.dump(data,pickle_file)

class InterpolatedSequence:

    def __init__(self,dict_or_pickle_filename_root,independent_key=None):

        if type(dict_or_pickle_filename_root) == dict:
            self.data = dict_or_pickle_filename_root
        else:
            with open(dict_or_pickle_filename_root + '.pkl','rb') as pickle_file:
                self.data = pickle.load(pickle_file)

        self.restart()
            
        self.independent_key = independent_key
        if self.independent_key != None:
            self.independent_variable = self.data[self.independent_key]
        
    def element_advance(self,step):
        '''Advance internal sequence position by 'step' elements'''
        self.float_index = self.index + self.remainder + step
        self.index = int(self.float_index)
        self.remainder = self.float_index - self.index
        # Since the data was collected in real time, delta_t is in real time (s)

    def restart(self):
        self.index = 0
        self.remainder = 0.0
        self.float_index = 0.0
        
    def value(self,key):
        values = self.data[key]
        return values[self.index] + self.remainder*(values[self.index+1] - values[self.index])
        
    def independent_advance(self,step):
        '''Advance internal sequence position by 'step' in independent variable, with independent variable values recorded in the optional 'independent_key' argument to __init__.'''
        
        assert step >= 0.0
        assert self.independent_key != None
        
        current_independent_value = self.independent_variable[self.index] + self.remainder*(self.independent_variable[self.index+1] - self.independent_variable[self.index])
        new_independent_value = current_independent_value + step
        
        while self.independent_variable[self.index+1] <= new_independent_value:
            self.index += 1
            
        self.remainder = (new_independent_value - self.independent_variable[self.index]) / (self.independent_variable[self.index+1] - self.independent_variable[self.index])
        
        self.float_index = self.index + self.remainder
