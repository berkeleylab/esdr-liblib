from __future__ import print_function
import math
import numpy
import scipy.optimize

def linear_interpolation(x_data,y_data,x):
    # assume that x_data decreases as the index increases
    if x < x_data[-1]:
        return y_data[-1]
    elif x > x_data[0]:
        return y_data[0]
    else:
        length = len(x_data)
        assert length == len(y_data)
        for i in range(length-1):
            if x > x_data[i+1]:
                break
        return y_data[i+1] + (x - x_data[i+1])/(x_data[i] - x_data[i+1])*(y_data[i] - y_data[i+1])
    
def calculate_fractional_occupancy(equilibrium_potential):
    standard_potential = [0.089, 0.120, 0.165, 0.210, 0.400, 1.500] # V
    fraction_of_sites = [0.500, 0.250, 0.170, 0.030, 0.049, 0.001]
    xi = [10., 10., 1., 10., 0.2, 0.2]
    F = 96487. # C/mol Faraday's constant
    R = 8.314 # J/mol*K
    T = 298.15 # K
    F__RT = F / (R*T) # C/J = 1/V
    fractional_occupancy = 0.0
    for j in range(6):
        fractional_occupancy += fraction_of_sites[j] / (1. + math.exp(xi[j]*F__RT*(equilibrium_potential - standard_potential[j])))
    # Approaches zero as equilibrium_potential approaches infinity
    return fractional_occupancy

point_count = 200
fractional_occupancies = numpy.empty(point_count)
equilibrium_potentials = numpy.empty(point_count)
for i in range(point_count):
    equilibrium_potentials[i] = i/float(point_count-1)*1.5 # V
    fractional_occupancies[i] = calculate_fractional_occupancy(equilibrium_potentials[i])

    print(fractional_occupancies[i], equilibrium_potentials[i])    
    
print()
print()

#check_fractional_occupancies = numpy.linspace(fractional_occupancies[0],fractional_occupancies[point_count-1],point_count)
check_fractional_occupancies = numpy.linspace(0.0,1.0,point_count)

for i in range(point_count-1,-1,-1):
    print(check_fractional_occupancies[i], linear_interpolation(fractional_occupancies, equilibrium_potentials, check_fractional_occupancies[i]))
