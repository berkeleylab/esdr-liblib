import math
import numpy
import scipy.optimize

class BaseElectrochemicalReaction:
    def __init__(self,characteristic_potential_=1.0):

        self.characteristic_potential_ = characteristic_potential_
        self.inverse_characteristic_potential_ = 1./self.characteristic_potential_
        
        self.F_ = 96487. # C/mol Faraday's constant
        R_ = 8.314 # J/mol*K
        T_ = 298.15 # K
        self.F__RT_ = self.F_ / (R_*T_) # C/J = 1/V
        self.RT__F_ = 1.0 / self.F__RT_ # V

    def set_initial_conditions(self):
        pass

    def set_equation_scalar_counts(self):
        pass    
            
class BaseButlerVolmer(BaseElectrochemicalReaction):

    def __init__(self, n, beta, k0_, characteristic_potential_=1.0):

        BaseElectrochemicalReaction.__init__(self,characteristic_potential_)
        
        self.n = n # number of electrons transferred in reaction
        
        self.beta = beta # symmetry factor

        self.alpha_a = (1. - self.beta)*self.n # apparent anodic transfer coefficient
        self.alpha_c = self.beta*self.n # apparent cathodic transfer coefficient

        self.k0_ = k0_

    def V_(self,i_n_,c1_,c2_,lower_bound=0,upper_bound=5.0):
        r'''
This must be overridden in derived classes. 

Parameters
----------
``i_n_`` : float
    Dimensional current density on active material :math:``[\textrm{A}/\textrm{m}^2]``

``c1_`` : float
    Dimensional lithium concentration in active material :math:``[\textrm{mol}/\textrm{m^3}]``

``c2_`` : float
    Dimensional salt concentration in electrolyte solution :math:``[\textrm{mol}/\textrm{m^3}]``

lower_bound : float
    Lower bound on solution search interval

upper_bound : float
    Upper bound on solution search interval

Returns
-------
    float
        Dimensional equilibrium potential [V]
        '''

        raise NotImplementedError

    def i_n_(self,eta_s_,c1_,c2_):
        r'''
This must be overridden in derived classes. 

Parameters
----------
``V_`` : float
    Dimensional surface potential [V]

``c1_`` : float
    Dimensional lithium concentration in active material :math:``[\textrm{mol}/\textrm{m^3}]``

``c2_`` : float
    Dimensional salt concentration in electrolyte solution :math:``[\textrm{mol}/\textrm{m^3}]``

Returns
-------
    float
        Dimensional current density on active material :math:``[\textrm{A}/\textrm{m}^2]``
        '''

        raise NotImplementedError
    
class Lithium(BaseButlerVolmer):

    def __init__(self,c2_initial_,characteristic_potential_=1.0):

        n = 1

        # This is in part already built in as an assumption in the equations of this class
        beta = 0.5

        ### This is presently disabled
        self.area_ratio = 1.0

        k0_ = 3e-6 # mol^{1/2} / (m^{1/2} s)
        
        BaseButlerVolmer.__init__(self, n, beta, k0_, characteristic_potential_)

    def U_(self,c1_):
        r'''
Parameters
----------
    c1_ : Dimensional lithium concentration in active material

Returns
-------
    float
        Dimensional equilibrium potential [V]
        '''
        return 0.
        
    def V_(self,i_n_,c1_,c2_): # V

        eta_s_ = -2*self.RT__F_*math.asinh(0.5*i_n_*self.k0_*self.F_*math.sqrt(c2_))

        return eta_s_ # U_ = 0

    def i_n_(self,eta_s_,c1_,c2_):

        # c1_ is not applicable here
        i0_ = 2.*self.k0_*self.F_*c2_**self.alpha_a

        # Here, a positive current density corresponds to an anodic reaction, negative to cathodic
        return i0_*math.sinh(-eta_s_*self.alpha_a*self.F__RT_)
    
    # # Replace generic exchange current density method
    # def exchange_current_density(self,concentrations):
    #     # 'concentrations' is a sequence with a single element, the concentration of lithium ions just outside of the electrode
    #     # This is in the generic form
    #     #return self.n*self.F*self.k_a**self.beta*(self.k_c*concentrations[0])**(1.-self.beta)
    #     # This a hybrid with the form that Shao-Ling used
    #     #return self.n*self.F*self.area_ratio*self.k_neg*concentrations[0]**(1.-self.beta)
    #     # This is the form that Shao-Ling used
    #     return self.F*self.area_ratio*self.k_neg*math.sqrt(concentrations[0])

    # Replace generic calculation with slightly more efficient calculation using assumption that beta = 1/2
    # def current_density(self,surface_potential,concentrations): # A/m^2

    #     return self.exchange_current_density(concentrations)*(math.exp(self.alpha_a*self.F__RT*surface_potential) - math.exp(-self.alpha_c*self.F__RT*surface_potential))
    
class ButlerVolmer_(BaseButlerVolmer):

    def __init__(self, n, beta, k0_, active_material, particle_radius_, c2_initial_, characteristic_potential_=1.0):

        BaseButlerVolmer.__init__(self,n,beta,k0_,characteristic_potential_)
        
        self.active_material = active_material
        
        self.c2_initial_ = c2_initial_

        self.particle_radius_ = particle_radius_
        self.inverse_particle_radius_ = 1./self.particle_radius_

    def U_(self,c1_):
        r'''
This must be overridden in derived classes. 

Parameters
----------
    c1_ : Dimensional lithium concentration in active material

Returns
-------
    float
        Dimensional equilibrium potential
        '''

        raise NotImplementedError

    def i_n_(self,eta_s_,c1_,c2_):
        i0_ = self.k0_*self.F_*((self.active_material.c_max_ - c1_)*c2_)**self.alpha_a * c1_**self.alpha_c

        # Here, a positive current density corresponds to an anodic reaction, negative to cathodic
        return i0_*(math.exp(eta_s_*self.alpha_a*self.F__RT_) - math.exp(-eta_s_*self.alpha_c*self.F__RT_))

    def residual(self,eta_s_,c1_,c2_,i_n_):

        return self.i_n_(eta_s_,c1_,c2_) - i_n_
    
    def eta_s_(self,c1_,c2_,i_n_,lower_bound=-5.0,upper_bound=5.0): 

        return scipy.optimize.brentq(self.residual,lower_bound,upper_bound,args=(c1_,c2_,i_n_),disp=True)
    
    def V_(self,i_n_,c1_,c2_,lower_bound=-5.0,upper_bound=5.0): # V
        r'''
Required for ``electrode`` postprocessing calculations

Parameters
----------
i_n_ : float
    dimensional current density :math:``[\textrm{A}/\textrm{m}^2]``

c1_ : float
    dimensional lithium concentration in active material :math:``[\textrm{mol}/\textrm{m}^3]``

c2_ : float
    dimensional salt concentration in electrolyte solution :math:``[\textrm{mol}/\textrm{m}^3]``

Returns
-------
float
    Surface overpotential [V]
        '''

        return self.eta_s_(c1_,c2_,i_n_,lower_bound=lower_bound,upper_bound=upper_bound) + self.U_(c1_)

class NMC333Reaction(ButlerVolmer_):

    def __init__(self,n,beta,active_material,particle_radius_,c2_initial_,characteristic_potential_=1.0):

        k0_ = 1.15e-10 # 1/(s mol^{1/2})
        
        ButlerVolmer_.__init__(self,n,beta,k0_,active_material,particle_radius_,c2_initial_,characteristic_potential_=characteristic_potential_)

    def U_(self,c1_):
        theta = c1_/self.active_material.c_max_
        return 0.60826E1+(-0.69922E1+(0.71062E1-0.25947E1*theta)*theta)*theta-0.54549E-4*math.exp(0.12423E3*theta-0.1142593002E3) # V
    
# class Graphite(ButlerVolmer_):

#     def calculate_fractional_occupancy(equilibrium_potential):
#         standard_potential = [0.089, 0.120, 0.165, 0.210, 0.400, 1.500] # V
#         fraction_of_sites = [0.500, 0.250, 0.170, 0.030, 0.049, 0.001]
#         xi = [10., 10., 1., 10., 0.2, 0.2]
#         F = 96487. # C/mol Faraday's constant
#         R = 8.314 # J/mol*K
#         T = 298.15 # K
#         F__RT = F / (R*T) # C/J = 1/V
#         fractional_occupancy = 0.0
#         for j in range(6):
#             fractional_occupancy += fraction_of_sites[j] / (1. + math.exp(xi[j]*F__RT*(equilibrium_potential - standard_potential[j])))
#         # Approaches zero as equilibrium_potential approaches infinity
#         return fractional_occupancy
    
#     def linear_interpolation(x_data,y_data,x):
#         # assume that x_data decreases as the index increases
#         if x < x_data[-1]:
#             return y_data[-1]
#         elif x > x_data[0]:
#             return y_data[0]
#         else:
#             length = len(x_data)
#             assert length == len(y_data)
#             for i in range(length-1):
#                 if x > x_data[i+1]:
#                     break
#             return y_data[i+1] + (x - x_data[i+1])/(x_data[i] - x_data[i+1])*(y_data[i] - y_data[i+1])

#     def equilibrium_potential(self,fractional_occupancy):

#         return self.linear_interpolation(fractional_occupancies, equilibrium_potentials, fractional_occupancy)
        
#     def __init__(self):

#         # Call inheritied __init__ method for basic setup

#         n = 1
#         beta = 0.5
#         GenericButlerVolmer.__init__(self, n, beta)

#         point_count = 200
#         self.fractional_occupancies = numpy.empty(point_count)
#         self.equilibrium_potentials = numpy.empty(point_count)
#         for i in range(point_count):
#             self.equilibrium_potentials[i] = i/float(point_count-1)*1.5 # V
#             self.fractional_occupancies[i] = calculate_fractional_occupancy(self.equilibrium_potentials[i])
                
#         #self.k_a = k_a # rate constant for anodic reaction

#         #self.k_c = k_c # rate constant for cathodic reaction

#         #self.area_ratio = 1.0

#         #self.k_neg = 1e-20
        
#     # Replace generic exchange current density method
#     def exchange_current_density(self,concentrations):
#         # 'concentrations' is a sequence holding the concentration of lithium ions just outside of the electrode
#         # This is in the generic form
#         #return self.n*self.F*self.k_a**self.beta*(self.k_c*concentrations[0])**(1.-self.beta)
#         # This a hybrid with the form that Shao-Ling used
#         #return self.n*self.F*self.area_ratio*self.k_neg*concentrations[0]**(1.-self.beta)
#         # This is the form that Shao-Ling used
#         return self.F*self.area_ratio*self.k_neg*math.sqrt(concentrations[0])

#     # Replace generic calculation with slightly more efficient calculation using assumption that beta = 1/2
#     # def current_density(self,surface_potential,concentrations): # A/m^2

#     #     return self.exchange_current_density(concentrations)*(math.exp(self.alpha_a*self.F__RT*surface_potential) - math.exp(-self.alpha_c*self.F__RT*surface_potential))

    

