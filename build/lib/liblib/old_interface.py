import pygdh
import numpy
import scipy

class InterfaceDomain(pygdh.Grid):
    r'''
Describes objects used to transfer information between ``Problem`` objects

Parameters
----------
name : str
    This identifier will be used to name output files and can also be used to reference the object in a user-friendly way
    '''

    def __init__(self, name):

        self.initialize_grid(name,
                             field_names=['phi1_','phi2_','c1_','c2_'],
                             unit_classes=[ pygdh.Volume ],
                             stored_solution_count=2)
        
        self.active_field = [ False for i in range(self.field_count) ]
        
    def define_field_variables(self):

        self.V_ = numpy.empty(1)        
        self.eta_s_ = numpy.empty(1)
        
    def set_output_fields(self):

        self.output_fields = {
            'V_ [V]' : self.V_,            
            'eta_s_ [V]' : self.eta_s_}

class InterfaceNP(pygdh.Problem):

    def __init__(self,left_subproblem,right_subproblem):

        self.left_subproblem = left_subproblem
        self.right_subproblem = right_subproblem
        
    def set_initial_conditions(self):
        pass
        
    def process_solution(self):

        self.left_subproblem.set_c_(self.right_subproblem.left_get_c_())
        self.left_subproblem.set_phi2_(self.right_subproblem.left_get_phi_())    

class InterfacePN(pygdh.Problem):

    def __init__(self,left_subproblem,right_subproblem):

        self.left_subproblem = left_subproblem
        self.right_subproblem = right_subproblem
        
    def set_initial_conditions(self):
        pass
        
    def process_solution(self):

        self.right_subproblem.set_c_(self.left_subproblem.left_get_c_())
        self.right_subproblem.set_phi2_(self.left_subproblem.left_get_phi_())    
        
# class PorousInterface(pygdh.Problem):

#     def __init__(self,interface_domain,left_subproblem,right_subproblem):

#         self.left_subproblem = left_subproblem
#         self.right_subproblem = right_subproblem

#         self.initialize_problem([interface_domain],stored_solution_count=2)
        
#     def declare_unknowns(self):

#         ## for now, we're just solving for concentration
#         ## what if we're solving for phi as well? this is different between a separator and an electrode. might have to look into both objects
#         self.unknown[0][:,0] = True
            
#     def set_equation_scalar_counts(self):
        
#         self.equation_scalar_counts = {
#             self.governing_equations:1 } ### might be 2, might have to probe objects
        
#     def assign_equations(self):

#         self.equations[0] = [ self.governing_equations ]

#     def solution_governing_equations(self, vol, residual):

#         ### move elsewhere for efficiency?
#         left_residual = numpy.empty()
#         right_residual = numpy.empty()
#         ### might have to probe objects
#         if left_subproblem != None:
#             left_subproblem.grid[0].field[0][0,-1] = self.grid[0].field[0][0,0]
            
#             ## improve efficiency, need to correct grid reference
#             vol = left_subproblem.grid[0].unit_with_number[-1]
#             left_subproblem.right_boundary_governing_equations(vol,left_residual)
#         if right_subproblem != None:
#             right_subproblem.grid[0].field[0][0,0] = self.grid[0].field[0][0,0]
            
#             vol = right_subproblem.grid[0].unit_with_number[0]
#             right_subproblem.left_boundary_governing_equations(vol,right_residual)

#         ### make sure that this is an element-by-element assignment rather than a pointer reassignment
#         residual = left_residual + right_residual

#     def process_solution(self):
#         ### check field numbers
#         if left_subproblem != None:
#             self.grid[0].field[0][0,0] = left_subproblem.grid[0].field[0][0,-1]

#         if right_subproblem != None:
#             self.grid[0].field[0][0,0] = right_subproblem.grid[0].field[0][0,0]
            
class CellDomain(pygdh.Grid):
    r'''
Describes objects used to store calculations for ``Cell``

Parameters
----------
name : str
    This identifier will be used to name output files and can also be used to reference the object in a user-friendly way
    '''

    def __init__(self, name):

        self.initialize_grid(name,
                             field_names=['i_', 'V_'],
                             unit_classes=[ pygdh.Volume ],
                             stored_solution_count=2)

class Cell(pygdh.Problem):
    r'''
Minimal descriptions of objects needed to monitor and control electrochemical cell operation

Parameters
----------
cell_domain : CellDomain
    Storage object for cell-level observables and settings

negative_terminal_domain : InterfaceDomain
    Storage object for terminal-level observables

positive_terminal_domain : InterfaceDomain
    Storage object for terminal-level observables
    '''

    def __init__(self,cell_domain,negative_electrode_problem,positive_electrode_problem):

        self.negative_electrode_problem = negative_electrode_problem
        self.positive_electrode_problem = positive_electrode_problem        
        
        self.initialize_problem([cell_domain],stored_solution_count=2)
                
    def set_initial_conditions(self):
        pass
            
    def set_equation_scalar_counts(self):
        pass
    
    def declare_unknowns(self):
        pass
    
class CellSetCurrentDensity_(Cell):
    r'''
Descriptions of objects needed to monitor electrochemical cell operation and directly control current density

Parameters
----------
cell_domain : CellDomain
    Storage object for cell-level observables and settings; current density is controlled by writing directly to object member

negative_terminal_domain : InterfaceDomain
    Storage object for terminal-level observables

positive_terminal_domain : InterfaceDomain
    Storage object for terminal-level observables
    '''

    def __init__(self,cell_domain,negative_electrode_problem,positive_electrode_problem):

        Cell.__init__(self,cell_domain,negative_electrode_problem,positive_electrode_problem)

    def process_solution(self):

        V__negative = self.negative_electrode_problem.get_phi_()
        V__positive = self.positive_electrode_problem.get_phi_()

        V__field_number = self.grid[0].field_number_with_name['V_']
        self.grid[0].field[0][V__field_number,0] = V__positive - V__negative
    
class CellSetPotential_(Cell):
    r'''
Descriptions of objects needed to monitor electrochemical cell operation and directly control cell potential

Parameters
----------
cell_domain : CellDomain
    Storage object for cell-level observables and settings; cell potential is controlled by writing directly to object member

``positive_terminal_potential_lower_bound_`` : float
    Lower bound on current density [V]

``positive_terminal_potential_upper_bound_`` : float
    Upper bound on current density [V]
    '''

    def __init__(self,cell_domain,negative_electrode_problem,positive_electrode_problem,positive_terminal_potential_lower_bound=0.,positive_terminal_potential_upper_bound=5.):

        self.positive_terminal_potential_lower_bound = positive_terminal_potential_lower_bound # V
        self.positive_terminal_potential_upper_bound = positive_terminal_potential_upper_bound # V

        Cell.__init__(self,cell_domain,negative_electrode_problem,positive_electrode_problem)

    def declare_unknowns(self):
        pass

    def set_initial_conditions(self):
        pass
            
    def set_boundary_values(self):
        pass
       
    def set_equation_scalar_counts(self):
        # While one could use the general solver to obtain i_, this is a
        # one-dimensional problem and it is more efficient to use a specialized
        # solver in postprocessing
        pass

    def residual(self,phi__positive_,V_):

        return self.positive_electrode_problem.i_(phi__positive_) - self.negative_electrode_problem.i_(phi__positive_-V_)

    def process_solution(self):

        cell_domain = self.grid_with_name['cell']
        
        V__field_number = cell_domain.field_number_with_name['V_']
        V_ = cell_domain.field[0][V__field_number,0]
        
        phi1_ = scipy.optimize.brentq(self.residual,self.positive_terminal_potential_lower_bound,self.positive_terminal_potential_upper_bound,args=(V_,),disp=False)
        
        self.positive_electrode_problem.set_phi1_(phi1_)

        self.negative_electrode_problem.set_phi1_(phi1_ - V_)

        i__field_number = cell_domain.field_number_with_name['i_']
        cell_domain.field[0][i__field_number,0] = self.positive_electrode_problem.i_(phi1_)
        
