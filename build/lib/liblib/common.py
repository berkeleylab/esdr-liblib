import pygdh

F_ = 96487. # C/mol, Faraday's constant
inverse_F_ = 1./F_ # mole/C, Faraday's constant        
R_ = 8.314 # J/mol*K

# Objects of this class are the units from which the spatial domain will be
# constructed.
class OneDimensionalVolume(pygdh.Volume):

    def define_variables(self):

        self.left = self.relative_position_with_local_position(-1.0)
        self.right = self.relative_position_with_local_position(1.0)
    
    # Defining this method gives users an opportunity to define their own
    # mathematical operators, for later use in describing governing equations.
    def define_interpolants(self):

        # Operator for estimating first derivative at left boundary of volume
        self.dx_left = self.default_interpolant(-1.0, 1, 1) 
        
        # Operator for estimating function value at the left boundary of volume
        self.interpolate_left = self.default_interpolant(-1.0, 0, 1)

        # Operator for estimating function value at the volume center
        self.interpolate = self.default_interpolant(0.0, 0, 2)

        # Operator for estimating first derivative at the volume center
        self.dx = self.default_interpolant(0.0, 1, 2) 
        
        # Operator for estimating function value at the right boundary of volume
        self.interpolate_right = self.default_interpolant(1.0, 0, 1)
        
        # Operator for estimating first derivative at right boundary of volume
        self.dx_right = self.default_interpolant(1.0, 1, 1)

class CellRegion_:

    def __init__(self,length_scale,concentration_scale,flux_scale,potential_scale=1.0):

        self.length_scale = length_scale
        self.inverse_length_scale = 1./self.length_scale
        self.flux_scale = flux_scale
        self.inverse_flux_scale = 1./self.flux_scale
        self.concentration_scale = concentration_scale
        self.inverse_concentration_scale = 1./self.concentration_scale        
        self.potential_scale = potential_scale
        self.inverse_potential_scale = 1./self.potential_scale        

    def set_neighbors(self,left_neighbor,right_neighbor):
        
        self.left_neighbor = left_neighbor
        self.right_neighbor = right_neighbor

    def pore_volume_(self,unit_number):

        raise NotImplementedError

    def boundary_term_(self,unit_number):

        raise NotImplementedError

    def c2_(self,relative_time_index,unit_number):

        raise NotImplementedError

    def phi2_(self,unit_number):

        raise NotImplementedError

    def left_neighboring_c2(self,relative_time_index):

        left_neighbor_c2_ = self.left_neighbor.c2_(relative_time_index,-1)
        if left_neighbor_c2_ == None:
            return None
        else:
            return left_neighbor_c2_ * self.inverse_concentration_scale

    def right_neighboring_c2(self,relative_time_index):

        right_neighbor_c2_ = self.right_neighbor.c2_(relative_time_index,0)
        if right_neighbor_c2_ == None:
            return None
        else:
            return right_neighbor_c2_ * self.inverse_concentration_scale
    
    def left_neighboring_phi2(self):

        return self.left_neighbor.phi2_(-1) * self.inverse_potential_scale

    def right_neighboring_phi2(self):

        return self.right_neighbor.phi2_(0) * self.inverse_potential_scale
    
    def left_neighboring_pore_volume(self):

        return self.left_neighbor.pore_volume_(-1) * self.inverse_length_scale

    def left_neighboring_boundary_term(self):

        return self.left_neighbor.boundary_term_(-1) * self.inverse_flux_scale

    def right_neighboring_pore_volume(self):

        return self.right_neighbor.pore_volume_(0) * self.inverse_length_scale

    def right_neighboring_boundary_term(self):

        return self.right_neighbor.boundary_term_(0) * self.inverse_flux_scale
    
### specify Grids, Models, then customized Simulation can be used to notify all Models of neighbors
### for Models to not depend on a known Grid layout, they all have to be subsimulations. Or we can pass a grid number, but that seems messy. Could autodetect type of grid, but what if we're using more than one grid of a given type?
### specify grid and model, fixing grid numbers, and also pass left and right Models. Or simulations? We currently link Models after the fact, this is just another way to do it that also fixes the grid identification issue. We might be able to put the Grids in the Simulations
class Layer(pygdh.Simulation):
    def __init__(self,left,main,right):
        
#separator_subsimulation = liblib.separator.ConcentratedBinarySaltSolutionFilledSeparatorSimulation_

