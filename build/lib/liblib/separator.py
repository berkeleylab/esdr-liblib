import math
import numpy
import pygdh
import liblib.common

class SeparatorDomain(pygdh.Grid):
    r'''
Describes objects used to store calculations for ``Separator``

Parameters
----------
name : str
    This identifier will be used to name output files and can also be used to reference the object in a user-friendly way

volume_count : int
    Number of units into which domain is discretized
    '''

    def __init__(self, name, volume_count):

        self.initialize_grid(name,
                             field_names=['c','phi'],
                             coordinate_values={
                                 'x': numpy.linspace(0.0, 1.0, volume_count)},
                             unit_classes=[
                                 liblib.common.OneDimensionalVolume for i in
                                 range(volume_count) ],stored_solution_count=2)
        
    def define_field_variables(self):
        
        self.x_ = numpy.empty(self.unit_count)
        self.c_ = numpy.empty(self.unit_count)
        self.phi_ = numpy.empty(self.unit_count)        

    def set_output_fields(self):

        self.output_fields = {
            'x_ [m]' : self.x_,
            'c_ [mol/m^3]' : self.c_,
            'phi_ [V]' : self.phi_}
        
class ConcentratedBinarySaltSolutionFilledSeparator(pygdh.Model):
    r'''
Asterisks here mark dimensional quantities, those without asterisks are dimensionless. Within the source code, variables representing quantities and having names without a trailing underscore are dimensionlesss. The following definitions transform the dimensional model described by ``ConcentratedBinarySaltSolutionFilledSeparator_``:

.. math::

   c = c^* / \overline{c}^*

.. math::

   D_{\mathrm{eff}} = D_{\mathrm{eff}}^*(c^*) / D_{\mathrm{eff}}^*(\overline{c}^*)

.. math::

   t = \frac{t^* D_{\mathrm{eff}^*}(\overline{c^*})}{{L^*}^2}

.. math::

   x = \frac{x^* - x_0^*}{L^*}

.. math::

   N_+ = \frac{L^* N_+^*}{\overline{c}^* D_{\mathrm{eff}^*}(\overline{c}^*)}

.. math:

   T = \frac{R^* T^*}{F^* \phi_c^*}

where

.. math::

   L^* = x_n^* - x_0^*

and :math:`\phi_c^*` is a user-defined characteristic potential, typically specified in derived classes as 1 V.

This class describes objects that approximately solve

.. math:: 

   \frac{\partial (\epsilon c)}{\partial t} = -\frac{\partial}{\partial x} \left[\frac{N_+}{\nu_+}\right]

using the FVM representation

.. math::
    
   0 = \epsilon \frac{\overline{c}(t,x_i) - \overline{c}(t-\Delta t,x_i)}{\Delta t} (x_{i+1} - x_i) + \left[\frac{N_+}{\nu_+} \right]_{x_i}^{x_{i+1}} + \mathcal{O}(\Delta t)

where

.. math::

   \overline{c}(t,x_i) = \frac{1}{x_{i+1} - x_i} \int_{x_i}^{x_{i+1}} c(t,x) \ dx

.. math::

   \frac{N_+}{\nu_+} = -D_{\mathrm{eff}} \left( 1 - \frac{\partial \ln c_0}{\partial \ln c} \right) \frac{\partial c}{\partial x} + \frac{t_+^0}{z_+ \nu_+} i + c v_0.

It is assumed that the last term is negligible

.. math::

    c v_0 = 0.

At boundaries at which all current is carried by the cations,

.. math::

   \frac{N_+}{\nu_+} = \frac{i}{z_+ \nu_+}.

Parameters
----------
separator_domain : SeparatorDomain
    Storage object for calculations within the particle

porosity : float
    Separator pore volume divided by total volume

T : float
    Dimensionless temperature :math:`T`
    '''

    def __init__(self, porosity, T):

        self.porosity = porosity

        self.T = T

        self.inverse_z_plus_nu_plus = 1.0

    def set_initial_conditions(self):

        c = self.separator_grid.field[0][self.concentration_field_number]
        phi = self.separator_grid.field[0][self.potential_field_number]

        for i in range(self.separator_grid.unit_count):
            # Concentrations are scaled by initial concentration
            c[i] = 1.0
            phi[i] = 0.0

    def set_equation_scalar_counts(self):
        
        self.equation_scalar_counts = {
            self.solution_governing_equations_at_left_interface:1,
            self.solution_governing_equations_in_bulk: 1,
            self.solution_governing_equations_at_right_interface:1}
        
    def assign_equations(self):

        separator_grid_number = 2
        solution_equations = self.equations[separator_grid_number]

        solution_equations[0] = [self.solution_governing_equations_at_left_interface]

        for vol_number in range(1,self.separator_grid.unit_count-1):
            solution_equations[vol_number] = [self.solution_governing_equations_in_bulk]

        # "Right" domain boundary
        solution_equations[-1] = [self.solution_governing_equations_at_right_interface]

    def D_eff(self,c):
        r'''
Return

.. math::
        
   D_{\mathrm{eff}} \left( 1 - \frac{\partial \ln c_0}{\partial \ln c} \right)

Note that this is not merely the dimensionless effective salt diffusivity, although the derivative in the second factor is often assumed to be negligibly small.

This must be overridden in derived classes.

Parameters
----------
c : float
    Dimensionless salt concentration

Returns
-------
float
        '''
        raise NotImplementedError
        
    def i(self):
        r'''
Return instantaneous dimensionless conventional current density in the positive x direction

This must be overriden in derived classes. 

Returns
-------
    float
        '''
        raise NotImplementedError

    def left_flux(self,vol):

        separator_grid = self.separator_grid

        c = separator_grid.field[0][self.concentration_field_number]
        
        c_left = vol.interpolate_left(c)

        D_eff_left = self.D_eff(c_left)

        return -D_eff_left*vol.dx_left(c) + self.cation_transference_number(c_left)*self.inverse_z_plus_nu_plus*self.i()
    
    def right_flux(self,vol):

        separator_grid = self.separator_grid

        c = separator_grid.field[0][self.concentration_field_number]
        
        c_right = vol.interpolate_right(c)

        D_eff_right = self.D_eff(c_right)

        return -D_eff_right*vol.dx_right(c) + self.cation_transference_number(c_right)*self.inverse_z_plus_nu_plus*self.i()
    
    def solution_governing_equations_at_left_interface(self, vol, residual):

        #c_1 = self.left_neighboring_c2(-1)
        #if c_1 == None:
        # doesn't seem like the interpolation makes a difference
        #c_1 = vol.interpolate(self.separator_grid.field[-1][self.concentration_field_number])
        c_1 = self.separator_grid.field[-1][self.concentration_field_number,0]
        
        c = self.separator_grid.field[0][self.concentration_field_number]
        left_neighboring_c2 = self.left_neighboring_c2(0)
        if left_neighboring_c2 == None:
            residual[0] = ((self.left_neighboring_pore_volume() + self.porosity*vol.volume)*(vol.interpolate(c) - c_1)*self.inverse_timestep_size + self.right_flux(vol) - self.left_neighboring_boundary_term())
        else:
            residual[0] = vol.interpolate(c) - self.left_neighboring_c2(0)

    def solution_governing_equations_in_bulk(self, vol, residual):

        c_1 = self.separator_grid.field[-1][self.concentration_field_number]
        c = self.separator_grid.field[0][self.concentration_field_number]

        residual[0] = (self.porosity*vol.volume*(vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size + self.right_flux(vol) - self.left_flux(vol))

    def solution_governing_equations_at_right_interface(self, vol, residual):
        
        c_1 = self.right_neighboring_c2(-1)
        if c_1 == None:
        #c_1 = vol.interpolate(self.separator_grid.field[-1][self.concentration_field_number])
            c_1 = self.separator_grid.field[-1][self.concentration_field_number,-1]
            
        c = self.separator_grid.field[0][self.concentration_field_number]
        
        residual[0] = ((self.porosity*vol.volume + self.right_neighboring_pore_volume())*(vol.interpolate(c) - c_1)*self.inverse_timestep_size + self.right_neighboring_boundary_term() - self.left_flux(vol))
        
    def kappa_eff(self,c):
        r'''
Return dimensionless effective conductivity.

This must be overriden in derived classes. 

Parameters
----------
c : float
    Dimensionless salt concentration

Returns
-------
float
        '''
        raise NotImplementedError        

    def thermodynamic_factor(self,c):
        r'''
Return thermodynamic factor 

.. math::
   \frac{\partial(\ln f)}{\partial(\ln c)}

This must be overriden in derived classes. 

Parameters
----------
c : float
    Dimensionless salt concentration

Returns
-------
float
        '''
        raise NotImplementedError        
    
    def declare_unknowns(self):

        #### to do this generally, we should probably have a consistent format for each region so that the only surrounding regions are included and so the user doesn't have to reach into here and set things, that's the point of separating everything after all. So we'll probably be coordinating coordinators, which gets messy because we only want to write output for the grid that changed, so coordinators shouldn't always just write out all grids. Maybe they could intelligently decide on which ones to write.
        
        # These aliases help with generality, but make the code more verbose. These assignments can't be made in __init__ because this routine is called in initialize_model, but the grids are recognized through that routine as well. This is messy, I should probably define an additional hook.

        self.unknown[0][:,:] = False
        self.unknown[1][:,:] = False        
        separator_grid_number = 2
        self.separator_grid = self.grid[separator_grid_number]

        self.concentration_field_number = self.separator_grid.field_number_with_name['c']
        self.potential_field_number = self.separator_grid.field_number_with_name['phi']

        for i in range(self.separator_grid.unit_count):
            self.unknown[separator_grid_number][self.potential_field_number,i] = False
       
        for i in range(self.separator_grid.unit_count):        
            self.unknown[separator_grid_number][self.concentration_field_number,0] = True

    def process_solution(self):

        ## The integration performed here is based on volume "centers," not the boundaries of the control volumes
        
        c_field = self.separator_grid.field[0][self.concentration_field_number]
        phi_field_number = self.separator_grid.field_number_with_name['phi']        

        self.separator_grid.field[0][phi_field_number,0] = 0.0
        
        vol = self.separator_grid.unit_with_number[0]
            
        c = vol.interpolate(c_field)

        integrand_left = 2.*self.T*(1.-self.cation_transference_number(c))*(1.+self.thermodynamic_factor(c))/c*vol.dx(c_field) - self.i() / self.kappa_eff(c)
        
        for vol_number in range(1,self.separator_grid.unit_count):

            last_vol = vol
            vol = self.separator_grid.unit_with_number[vol_number]
            
            c = vol.interpolate(c_field)

            integrand_right = 2.*self.T*(1.-self.cation_transference_number(c))*(1.+self.thermodynamic_factor(c))/c*vol.dx(c_field) - self.i() / self.kappa_eff(c)

            # Integration by trapezoidal rule
            self.separator_grid.field[0][phi_field_number,vol_number] = self.separator_grid.field[0][phi_field_number,vol_number-1] + 0.5*(vol.coordinate - last_vol.coordinate)*(integrand_left + integrand_right)

            integrand_left = integrand_right

        ## Information will flow from left to right by default
            
        left_neighboring_phi2 = self.left_neighboring_phi2()
        if left_neighboring_phi2 != None:
            self.separator_grid.field[0][phi_field_number,:] += left_neighboring_phi2
        else:
            right_neighboring_phi2 = self.right_neighboring_phi2()
          
            if right_neighboring_phi2 != None:
                self.separator_grid.field[0][phi_field_number,:] += right_neighboring_phi2 - self.separator_grid.field[0][phi_field_number,-1]
            else:
                raise NotImplementedError

class ConcentratedBinarySaltSolutionFilledSeparator_(ConcentratedBinarySaltSolutionFilledSeparator,liblib.common.CellRegion_):
    r'''
All quantities are dimensional. The choice of units for dimensional quantities must be consistent, so that all units cancel in the dimensionless definitions given in the ``ConcentratedBinarySaltSolutionFilledSeparator`` docstring. Within the source code, variables representing quantities and having names with a trailing underscore are dimensional.

This class describes objects that use ``ConcentratedBinarySaltSolutionFilledSeparator`` to approximately solve

.. math:: 

   \frac{\partial (\epsilon c^*)}{\partial t^*} = -\frac{\partial}{\partial x^*} \left[ \frac{N_+^*}{\nu_+} \right]

.. math::

   \frac{N_+^*}{\nu_+} = -D_{\mathrm{eff}}^* \left( 1 - \frac{\partial \ln c_0^*}{\partial \ln c^*} \right) \frac{\partial c^*}{\partial x^*} + \frac{t_+^0}{z_+ \nu_+ F^*} i^* + c^* v_0^*. 

The last term is assumed to be negligible:

.. math::
   c^* v_0^* = 0.

At boundaries at which all current is carried by the cations, 

.. math::

   N_+^* = \frac{i^*}{F^* z_+}.

Parameters
----------
cell_domain : interface.CellDomain
    Storage object for observables and controls at cell level

separator_domain : SeparatorDomain
    Storage object for calculations within the particle

porosity : float
    Separator pore volume divided by total volume

tortuosity : float
    Tortuosity :math:`\tau` of pores in separator with porosity :math:`\phi`, defined so that effective conductivity is given by :math:`\kappa_{\mathrm{eff}}^* = (\phi / \tau) \kappa^*`, where :math:`\kappa^*` is the dimensional intrinsic electrolyte solution conductivity
    
``thickness_`` : float
    Dimensional thickness of separator

electrolyte_solution : electrolyte_solution.Base
    Object describing electrolyte solution properties

``characteristic_potential_`` : float
    Keyword argument; dimensional characteristic potential used to nondimensionalize potentials in calculations, defaults to 1.0 in chosen potential units, which should be a reasonable choice for any cell
    '''

    def __init__(self,cell_domain,porosity,tortuosity,thickness_,electrolyte_solution,characteristic_potential_=1.0):

        self.characteristic_potential_ = characteristic_potential_
        self.electrolyte_solution = electrolyte_solution
        
        T = self.electrolyte_solution.RT__F_ / self.characteristic_potential_

        ConcentratedBinarySaltSolutionFilledSeparator.__init__(self, porosity, T)
        
        # The CellDomain fields are only inputs and so do not need to be
        # "registered" with the nonlinear solver system
        
        self.cell_domain = cell_domain

        i__field_number = self.cell_domain.field_number_with_name['i_']
        self.cell_domain.field[0][i__field_number,0] = 0.0
                   
        self.thickness_ = thickness_       
        
        ## Mark fields as active as a signal to other cell component objects
        
        #c_field_number = self.grid[0].field_number_with_name['c']
        #phi_field_number = self.grid[0].field_number_with_name['phi']        
            
        self.porosity__tortuosity = porosity / tortuosity

        self.D_eff__at_c_initial_ = self.D_eff_(self.electrolyte_solution.c_initial_)        
        self.inverse_D_eff__at_c_initial_ = 1./self.D_eff__at_c_initial_
        
        self.characteristic_time_ = self.thickness_**2 * self.inverse_D_eff__at_c_initial_
        
        self.inverse_characteristic_time_ = 1./self.characteristic_time_

        # Supply length and source scales
        liblib.common.CellRegion_.__init__(self,self.thickness_,self.electrolyte_solution.c_initial_,self.electrolyte_solution.c_initial_*self.D_eff__at_c_initial_/self.thickness_)

    def set_grid(self,grid,stored_solution_count=1):

        pygdh.Model.set_grid(self,grid,stored_solution_count=stored_solution_count)

        #### this is inflexible, the number of domains in the cell might change, but we're not even getting to this point yet
        # self.cell_grid_number = self.grid_number_with_name['cell']
        # self.cell_grid = self.grid_with_name['cell']
        # self.positive_electrode_grid_number = self.grid_number_with_name['positive_electrode']      
        # self.positive_electrode_grid = self.grid_with_name['positive_electrode']
        self.separator_grid_number = self.grid_number_with_name['solution']
        self.separator_grid = self.grid_with_name['solution']        

        ## The dimensional coordinates will not change

        for unit_number in range(self.separator_grid.unit_count):
            self.separator_grid.x_[unit_number] = self.thickness_*self.separator_grid.coordinates_list[0][unit_number]
        
    ## Override methods inherited from CellRegion to allow neighboring regions
    ## to query this region
        
    def pore_volume_(self,unit_number):

        return self.porosity*self.separator_grid.unit_with_number[unit_number].volume*self.thickness_

    def boundary_term_(self,unit_number):

        # There are no lithium ion sources or sinks in a separator

        if unit_number == 0:
            return self.flux_scale*(self.right_flux(self.separator_grid.unit_with_number[0]))# + self.porosity*self.separator_grid.unit_with_number[0].volume*(self.separator_grid.field[0][0,0]-self.separator_grid.field[-1][0,0]))#*self.inverse_timestep_size
        elif unit_number == -1:
            return self.flux_scale*(self.left_flux(self.separator_grid.unit_with_number[-1]))# - self.porosity*self.separator_grid.unit_with_number[-1].volume*(self.separator_grid.field[0][0,-1]-self.separator_grid.field[-1][0,-1]))#*self.inverse_timestep_size
        else:
            raise NotImplementedError

    def c2_(self,relative_time_index,unit_number):

        return self.separator_grid.field[relative_time_index][0,unit_number]*self.electrolyte_solution.c_initial_
    
    def phi2_(self,unit_number):

        return self.separator_grid.field[0][1,unit_number]*self.characteristic_potential_
        
    def time_dependent_system_step(self,timestep_size,output,handle_exception_in_solver=True,adaptive_timestepping=False,minimum_timestep_size=1e-6,use_predictor=True,quiet=False,interrupted=False):

        # Call base class method with dimensionless timestep size
        return ConcentratedBinarySaltSolutionFilledSeparator.time_dependent_system_step(self,timestep_size*self.inverse_characteristic_time_,output,handle_exception_in_solver=handle_exception_in_solver,adaptive_timestepping=adaptive_timestepping,minimum_timestep_size=minimum_timestep_size,use_predictor=use_predictor,quiet=quiet,interrupted=interrupted)
        
    def D_eff_(self,c_):
        r'''
Return

.. math::
        
   D_{\mathrm{eff}}^* \left( 1 - \frac{\partial \ln c_0^*}{\partial \ln c^*} \right)

as computed using parameters provided to __init__().

Note that this is not merely the dimensional effective diffusivity, although the derivative in the second factor is often assumed to be negligibly small.

Parameters
----------
c_ : float
    Dimensional salt concentration

Returns
-------
float
        '''      
        return self.porosity__tortuosity*self.electrolyte_solution.D_(c_)

    def D_eff(self,c):
        r'''
Return

.. math::
        
   D_{\mathrm{eff}} \left( 1 - \frac{\partial \ln c_0}{\partial \ln c} \right)

as computed using the dimensional effective diffusivity defined in ``D_eff_()``.

Note that this is not merely the dimensionless effective diffusivity, although the derivative in the second factor is often assumed to be negligibly small.

Parameters
----------
c : float
    Dimensionless salt concentration

Returns
-------
float
        '''
        return self.D_eff_(c*self.electrolyte_solution.c_initial_)*self.inverse_D_eff__at_c_initial_

    def kappa_eff(self,c):
        r'''
Return dimensionless effective conductivity as computed using parameters provided to __init__().

This is used in the dimensionless calculations of the parent class.

Parameters
----------
c : float
    Dimensionless salt concentration

Returns
-------
float
        '''
        return self.characteristic_potential_*self.inverse_D_eff__at_c_initial_*self.electrolyte_solution.inverse_c_initial_*liblib.common.inverse_F_*self.porosity__tortuosity*self.electrolyte_solution.kappa_(c*self.electrolyte_solution.c_initial_)

    def cation_transference_number(self,c):
        r'''
Return cation transference number as computed using parameters provided to __init__().

This is used in the dimensionless calculations of the parent class.

Parameters
----------
c : float
    Dimensionless salt concentration

Returns
-------
float
        '''
        return self.electrolyte_solution.cation_transference_number(c*self.electrolyte_solution.c_initial_)

    def thermodynamic_factor(self,c):
        r'''
Return thermodynamic factor as computed using parameters provided to __init__().

.. math::
   \frac{\partial(\ln f)}{\partial(\ln c)}

This is used in the dimensionless calculations of the parent class.

Parameters
----------
c : float
    Dimensionless salt concentration

Returns
-------
float
        '''
        return self.electrolyte_solution.thermodynamic_factor_(c*self.electrolyte_solution.c_initial_)

    def i(self):
        r'''
Return instantaneous dimensionless conventional current density in the positive x direction as computed using parameters provided to __init__().

This is used in the dimensionless calculations of the parent class.

Returns
-------
float
        '''
        return self.thickness_*self.inverse_D_eff__at_c_initial_*self.electrolyte_solution.inverse_c_initial_*liblib.common.inverse_F_*self.cell_domain.field[0][self.cell_domain.field_number_with_name['i_'],0]

    def process_solution(self):

        # Perform phi calculation
        ConcentratedBinarySaltSolutionFilledSeparator.process_solution(self)

        c_field_number = self.separator_grid.field_number_with_name['c']
        phi_field_number = self.separator_grid.field_number_with_name['phi']

        for unit_number in range(self.separator_grid.unit_count):
            self.separator_grid.c_[unit_number] = self.electrolyte_solution.c_initial_*self.separator_grid.field[0][c_field_number,unit_number]
            self.separator_grid.phi_[unit_number] = self.characteristic_potential_*self.separator_grid.field[0][phi_field_number,unit_number]

class ConcentratedBinarySaltSolutionFilledSeparatorSimulation_(pygdh.Simulation):
    def __init__(self,grid_list):
        separator_thickness_ = 50e-6 # m
        porosity = 0.41
        tortuosity = 3.809116
        # detect the nature of the neighboring regions by examining the grids, might even need access to the Problems. Might need to define these outside.
        separator_model = liblib.separator.ConcentratedBinarySaltSolutionFilledSeparator_(cell_domain,porosity,tortuosity,separator_thickness_,electrolyte_solution) 
       
        self.initialize_simulation(grid_list,separator_model,stored_solution_count=2)
