import pygdh
import math
import numpy
import liblib.common

class BaseElectrode(liblib.common.CellRegion_):

    def i_(self,phi1_):    
        r'''
This is required to use cell potential for control, must be overloaded in derived classes.

Parameters
----------
``phi1_`` : float
    Dimensional terminal potential [V]

Returns
-------
float
    Dimensional current density :math:`[\textrm{A}/\textrm{m}^2]`
        '''
        raise NotImplementedError
        
class SingleParticleDomain(pygdh.Grid):
    r'''
Describes objects used to store calculations for ``SingleParticle``

Parameters
----------
name : str
    Used to generate output file names and also provides a convenient way of referencing these objects

unit_count : int
    Number of units into which domain is discretized
    '''

    def __init__(self, name, unit_count):

        self.initialize_grid(name,
                             coordinate_values = {
                                 'r': numpy.linspace(0., 1., unit_count)},
                             field_names=['c'],
                             unit_classes=[ liblib.common.OneDimensionalVolume
                                            for i in range(unit_count)],
                             stored_solution_count=2)

    def define_field_variables(self):

        self.r_ = numpy.zeros(self.unit_count)
        self.c_ = numpy.zeros(self.unit_count)

    def set_output_fields(self):

        self.output_fields = {
            'r_ [m]': self.r_,
            'c_ [mol/m^3]': self.c_}

class SingleParticleSurfaceDomain(pygdh.Grid):

    def __init__(self,name):

        self.initialize_grid(name, unit_classes=[ pygdh.Volume ])

    def define_field_variables(self):

        self.phi_ = numpy.zeros(1)        

    ### this output might not appear if there are not regular fields defined
    def set_output_fields(self):

        self.output_fields = {
            'phi_ [V]': self.phi_}
        
class SingleParticle(pygdh.Model):
    r'''
Asterisks mark dimensionless quantities. Within the source code, variables representing quantities and having names without a trailing underscore are dimensionlesss. The following definitions transform the dimensional model described by ``SingleParticle_``:

.. math::

   c_1 = c_1^*/c_{1,\mathrm{max}}^*

.. math::

   r = r^*/r_n^*

.. math::

   t = \frac{t^* D_1^*(c_{1,\mathrm{max}}^*)}{{r_n^*}^2}

.. math::

   N_1 = \frac{r_n^* N_1^*}{D_1^*(c_{1,\mathrm{max}}^*) c_{1,\mathrm{max}}^*}

.. math::

   D_1(c_1) = D_1^*(c_1^*) / D_1^*(c_{1,\mathrm{max}}^*).

This class describes objects that approximately solve

.. math:: 
    
   \frac{\partial c_1}{\partial t} = -\frac{1}{{r}^2} \frac{\partial}{\partial r} \left[{r}^2 N_1\right]

using the FVM representation

.. math::
    
   0 = \frac{\overline{c_1}(t,r_i) - \overline{c_1}(t-\Delta t,r_i)}{\Delta t} \frac{{r_{i+1}}^3 - {r_i}^3}{3} + \left[{r}^2 N_1(t)\right]_{r_i}^{r_{i+1}} + \mathcal{O}(\Delta t),

where

.. math::

   \overline{c_1}(t,r_i) = \frac{3}{{r_{i+1}}^3 - {r_i}^3} \int_{r_i}^{r_{i+1}} {r}^2 c_1(t,r) \ dr

.. math::

   N_1 = -D_1(c_1) \frac{\partial c_1}{\partial r}

.. math::

   N_1 = 0 \textrm{ at center } r = 0

.. math::

   N_1 = i_n \textrm{ at surface } r = 1.

Parameters
----------
c_initial : float
    Initial dimensionless uniform lithium concentration within the particle

interior_domain : electrode.SingleParticleDomain
    Storage object for calculations within the particle

surface_domain : electrode.SingleParticleSurfaceDomain
    Storage object for calculations involving the particle surface
    '''
    
    def __init__(self,c_initial):
        
        self.c_initial = c_initial

        self.one_third = 1./3.

    def declare_unknowns(self):

        self.unknown[0][:,:] = False
        self.unknown[2][:,:] = False
        #### messy
        electrode_grid_number = 1
        self.positive_electrode_grid = self.grid[electrode_grid_number]
        for vol_number in range(self.grid[electrode_grid_number].unit_count):
            self.unknown[electrode_grid_number][0,vol_number] = True
            
    def set_equation_scalar_counts(self):
        self.equation_scalar_counts = {
            self.particle_inner_equation:1,
            self.particle_interior_equation:1,
            self.particle_outer_equation:1 } 

    def assign_equations(self):

        #### messy
        electrode_grid_number = 1
        electrode_equations = self.equations[electrode_grid_number]
        electrode_equations[0] = [self.particle_inner_equation]
        for i in range(1,self.grid[electrode_grid_number].unit_count-1):
            electrode_equations[i] = [self.particle_interior_equation]
        electrode_equations[-1] = [self.particle_outer_equation]        

    def set_initial_conditions(self):
        #### messy
        electrode_grid_number = 1
        for vol in self.grid[electrode_grid_number].unit_with_number:
            self.grid[electrode_grid_number].field[0][0,vol.number] = self.c_initial

    def particle_inner_equation(self, vol, residual):
        # This method isn't actually necessary; particle_interior_equation should give the same result at this location, just slightly less efficiently.

        # Defined for clarity
        c_1 = self.positive_electrode_grid.field[-1][0]        
        c = self.positive_electrode_grid.field[0][0]

        #c_left = vol.interpolate_left(c)
        c_right = vol.interpolate_right(c)        

        r_left = vol.coordinate + vol.left
        r_right = vol.coordinate + vol.right
        
        residual[0] = (vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size*r_right**3*self.one_third - self.D1(c_right)*r_right**2*vol.dx_right(c)

    def particle_interior_equation(self, vol, residual):

        # Defined for clarity
        c_1 = self.positive_electrode_grid.field[-1][0]        
        c = self.positive_electrode_grid.field[0][0]

        c_left = vol.interpolate_left(c)
        c_right = vol.interpolate_right(c)        

        r_left = vol.coordinate + vol.left
        r_right = vol.coordinate + vol.right

        residual[0] = (vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size*(r_right**3 - r_left**3)*self.one_third - (self.D1(c_right)*r_right**2*vol.dx_right(c) - self.D1(c_left)*r_left**2*vol.dx_left(c))

    def particle_outer_equation(self, vol, residual):

        # Defined for clarity
        c_1 = self.positive_electrode_grid.field[-1][0]        
        c = self.positive_electrode_grid.field[0][0]

        c_left = vol.interpolate_left(c)
        #c_right = vol.interpolate_right(c)        

        r_left = vol.coordinate + vol.left
        r_right = vol.coordinate + vol.right

        residual[0] = (vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size*(r_right**3 - r_left**3)*self.one_third + r_right**2*self.i_n() + self.D1(c_left)*r_left**2*vol.dx_left(c)

    def D1(self,c):
        r'''
This must be overridden in derived classes. 

Parameters
----------
    c : Dimensionless lithium concentration

Returns
-------
    float
        Dimensionless lithium diffusivity
        '''
        raise NotImplementedError        

    def i_n(self):
        r'''
This must be overridden in derived classes. 

Returns
-------
    float
        Instantaneous dimensionless conventional current density, equal to the dimensionless lithium flux, in the positive r direction
        '''
        raise NotImplementedError
            
    def process_solution(self):
        pass
    
class SingleParticle_(BaseElectrode,SingleParticle):
    r'''
    All dimensional quantities are marked with asterisks. The choice of units for dimensional quantities must be consistent, so that all units cancel in the dimensionless definitions given in the ``SingleParticle`` docstring. Within the source code, variables representing quantities and having names with a trailing underscore are dimensional.

Uses ``SingleParticle`` to approximately solve

.. math:: 
    
   \frac{\partial c_1^*}{\partial t^*} = -\frac{1}{{r^*}^2} \frac{\partial}{\partial r^*} \left[{r^*}^2 N_1^*\right]

.. math::

   N_1^* = -D_1^* \frac{\partial c_1^*}{\partial r^*},

where 

.. math::

   N_1^* = 0 \textrm{ at center } r^* = 0

.. math::

   N_1^* = i_n^* / F^* \textrm{ at surface } r^* = r_n^*

Parameters
----------
cell_domain : interface.CellDomain
    Storage object for observables and controls at cell level

interior_domain : SingleParticleDomain
    Storage object for calculations within the particle

surface_domain : SingleParticleSurfaceDomain
    Storage object for information at the particle surface

active_material : active_material.BaseMaterial
    Object describing electrode active material propoerties

``particle_radius_`` : float
    Dimensional particle radius

``active_material_loading_`` : float
    Active material mass per unit electrode area :math:`[\textrm{kg}/\textrm{m}^2]`

electrochemical_reaction : electrochemical_reaction.BaseElectrochemicalReaction
    Object describing main electrochemical reaction for electrode
    '''
    
    def __init__(self,cell_domain,active_material,particle_radius_,active_material_loading_,electrochemical_reaction):        
        
        # These are only used for input and output and therefore do not need to
        # be "registered" with the nonlinear solver system

        # All electrolyte solution fields will keep their initial inactive
        # status, information will be copied from the neighboring electrolyte
        # solution regions.

        self.cell_domain = cell_domain
        
        self.active_material = active_material

        SingleParticle.__init__(self,self.active_material.c_initial_*self.active_material.inverse_c_max_)

        self.particle_radius_ = particle_radius_

        self.active_material_volume_per_area_ = active_material_loading_*self.active_material.inverse_density_ # m

        self.capacity_per_area_ = liblib.common.F_*(self.active_material.c_max_ - self.active_material.c_min_)*self.active_material_volume_per_area_ # C / m^2, assumes one-electron reaction   
                
        self.active_material_area_density_ = self.active_material_volume_per_area_ / (4/3.*math.pi*particle_radius_**3) # m^{-2}, particle number area density
        
        self.inverse_active_material_area_density_ = 1./self.active_material_area_density_

        self.electrochemical_reaction = electrochemical_reaction

        self.surface_area_per_particle_ = 4. * math.pi * self.particle_radius_**2

        self.inverse_surface_area_per_particle_ = 1./self.surface_area_per_particle_

        self.characteristic_time_ = self.particle_radius_**2 * self.active_material.inverse_D_at_c_initial_
        
        self.inverse_characteristic_time_ = 1./self.characteristic_time_

    def set_grid(self,grid,stored_solution_count=1):

        SingleParticle.set_grid(self,grid,stored_solution_count=stored_solution_count)

        self.cell_grid_number = self.grid_number_with_name['cell']
        self.cell_grid = self.grid_with_name['cell']
        self.positive_electrode_grid_number = self.grid_number_with_name['positive_electrode']      
        self.positive_electrode_grid = self.grid_with_name['positive_electrode']
        self.separator_grid_number = self.grid_number_with_name['solution']
        self.separator_grid = self.grid_with_name['solution']            

        # These dimensional coordinates never change
        electrode_grid_number = 1
        for index in range(self.positive_electrode_grid.unit_count):
            self.positive_electrode_grid.r_[index] = self.particle_radius_*self.positive_electrode_grid.coordinates_list[0][index]

    def set_initial_conditions(self):
        ### not sure this is the right place for this
        #self.grid[1].phi_[0] = 4.3
        # don't override the initial condition on c_
        SingleParticle.set_initial_conditions(self)
        
    def pore_volume_(self,number):
        # This is not a porous region
        return 0.0

    def boundary_term_(self,number):
        # All current at the interface is carried by cations
        return liblib.common.inverse_F_*self.cell_domain.field[0][self.cell_domain.field_number_with_name['i_'],0]

    def c2_(self,relative_time_index,unit_number):

        return None
    
    def phi2_(self,unit_number):

        return 0.0
    
    def set_phi1_(self,phi_):
        separator_grid_number = 2
        self.grid[separator_grid_number].phi_[0] = phi_
        
    def get_phi_(self):
        separator_grid_number = 2
        return self.grid[separator_grid_number].phi_[0]

    def time_dependent_system_step(self,timestep_size,output,handle_exception_in_solver=True,adaptive_timestepping=False,minimum_timestep_size=1e-6,use_predictor=True,quiet=False,interrupted=False):

        # Call base class method with dimensionless timestep size
        return SingleParticle.time_dependent_system_step(self,timestep_size*self.inverse_characteristic_time_,output,handle_exception_in_solver=handle_exception_in_solver,adaptive_timestepping=adaptive_timestepping,minimum_timestep_size=minimum_timestep_size,use_predictor=use_predictor,quiet=quiet,interrupted=interrupted)
        
    def D1(self,c):
        r'''
This is used in the dimensionless calculations of the parent class.

Parameters
----------
c : float
    Dimensionless lithium concentration

Returns
-------
float
    Dimensionless lithium diffusivity
        '''
        return self.active_material.D_(c*self.active_material.c_max_)*self.active_material.inverse_D_at_c_initial_

    def i_n(self):
        r'''
This is used in the dimensionless calculations of the parent class.

Returns
-------
    float
        Instantaneous dimensionless conventional microscopic current density (at the particle surface), equal to the dimensionless lithium flux, in the positive r direction
        '''       
        i_ = self.cell_domain.field[0][self.cell_domain.field_number_with_name['i_'],0]

        return self.particle_radius_*self.active_material.inverse_D_at_c_initial_*self.active_material.inverse_c_max_*self.inverse_surface_area_per_particle_*self.inverse_active_material_area_density_*i_*liblib.common.inverse_F_

    def i_n_(self):
        r'''
Returns
-------
    float
        Instantaneous dimensional conventional microscopic current density (at the particle surface), equal to the dimensionless lithium flux, in the positive r direction
        '''       
        i_ = self.cell_domain.field[0][self.cell_domain.field_number_with_name['i_'],0]
        
        return self.inverse_surface_area_per_particle_*self.inverse_active_material_area_density_*i_

    def i_(self,phi1_):
        r'''
Calculates dimensional current density by the Butler-Volmer equation, as required to use cell potential for control

Parameters
----------
``phi1_`` : float
    Dimensional terminal potential [V]

Returns
-------
float
    Dimensional current density :math:`[\textrm{A}/\textrm{m}^2]`
    '''

        electrode_grid_number = 1
        c_field_number = self.positive_electrode_grid.field_number_with_name['c']
        c_ = self.positive_electrode_grid.field[0][c_field_number,-1]*self.active_material.c_max_

        if self.left_neighbor != None:
            c2_ = self.left_neighbor.c2_(0,-1)
            phi2_ = self.left_neighbor.phi2_(-1)
        elif self.right_neighbor != None:
            c2_ = self.right_neighbor.c2_(0,0)
            phi2_ = self.right_neighbor.phi2_(0)
        else:
            raise NotImplementedError

        eta_s_ = phi1_ - phi2_ - self.electrochemical_reaction.U_(c_)

        return self.surface_area_per_particle_*self.active_material_area_density_*self.electrochemical_reaction.i_n_(eta_s_,c_,c2_)
    
    def process_solution(self):

        electrode_grid_number = 1
        c_field_number = self.positive_electrode_grid.field_number_with_name['c']
        for index in range(self.positive_electrode_grid.unit_count):
            self.positive_electrode_grid.c_[index] = self.active_material.c_max_*self.positive_electrode_grid.field[0][c_field_number,index]

        if self.left_neighbor != None:
            c2_ = self.left_neighbor.c2_(0,-1)
            phi2_ = self.left_neighbor.phi2_(-1)
        elif self.right_neighbor != None:
            c2_ = self.right_neighbor.c2_(0,0)
            phi2_ = self.right_neighbor.phi2_(0)            
        else:
            raise NotImplementedError

        separator_grid_number = 2
        self.grid[separator_grid_number].phi_[0] = self.electrochemical_reaction.V_(self.i_n_(),self.positive_electrode_grid.c_[-1],c2_) + phi2_

class EmbeddedParticle_(BaseElectrode,SingleParticle):
    r'''
    All dimensional quantities are marked with asterisks. The choice of units for dimensional quantities must be consistent, so that all units cancel in the dimensionless definitions given in the ``SingleParticle`` docstring. Within the source code, variables representing quantities and having names with a trailing underscore are dimensional.

Uses ``SingleParticle`` to approximately solve

.. math:: 
    
   \frac{\partial c_1^*}{\partial t^*} = -\frac{1}{{r^*}^2} \frac{\partial}{\partial r^*} \left[{r^*}^2 N_1^*\right]

.. math::

   N_1^* = -D_1^* \frac{\partial c_1^*}{\partial r^*},

where 

.. math::

   N_1^* = 0 \textrm{ at center } r^* = 0

.. math::

   N_1^* = i_n^* / F^* \textrm{ at surface } r^* = r_n^*

Parameters
----------
cell_domain : interface.CellDomain
    Storage object for observables and controls at cell level

interior_domain : SingleParticleDomain
    Storage object for calculations within the particle

surface_domain : SingleParticleSurfaceDomain
    Storage object for information at the particle surface

active_material : active_material.BaseMaterial
    Object describing electrode active material propoerties

``particle_radius_`` : float
    Dimensional particle radius

``active_material_loading_`` : float
    Active material mass per unit electrode area :math:`[\textrm{kg}/\textrm{m}^2]`

electrochemical_reaction : electrochemical_reaction.BaseElectrochemicalReaction
    Object describing main electrochemical reaction for electrode
    '''
    
    def __init__(self,medium,position,interior_domain,surface_domain,active_material,particle_radius_,active_material_loading_,electrochemical_reaction):        
        
        # These are only used for input and output and therefore do not need to
        # be "registered" with the nonlinear solver system

        # All electrolyte solution fields will keep their initial inactive
        # status, information will be copied from the neighboring electrolyte
        # solution regions.

        self.medium = medium
        self.position = position
        
        self.active_material = active_material

        SingleParticle.__init__(self,self.active_material.c_initial_*self.active_material.inverse_c_max_,interior_domain,surface_domain)

        self.particle_radius_ = particle_radius_

        # These dimensional coordinates never change
        for index in range(self.grid[0].unit_count):
            self.grid[0].r_[index] = self.particle_radius_*self.grid[0].coordinates_list[0][index]

        self.active_material_volume_per_area_ = active_material_loading_*self.active_material.inverse_density_ # m

        self.capacity_per_area_ = liblib.common.F_*(self.active_material.c_max_ - self.active_material.c_min_)*self.active_material_volume_per_area_ # C / m^2, assumes one-electron reaction   
                
        self.active_material_area_density_ = self.active_material_volume_per_area_ / (4/3.*math.pi*particle_radius_**3) # m^{-2}, particle number area density
        
        self.inverse_active_material_area_density_ = 1./self.active_material_area_density_

        self.electrochemical_reaction = electrochemical_reaction

        self.surface_area_per_particle_ = 4. * math.pi * self.particle_radius_**2

        self.inverse_surface_area_per_particle_ = 1./self.surface_area_per_particle_

        self.characteristic_time_ = self.particle_radius_**2 * self.active_material.inverse_D_at_c_initial_
        
        self.inverse_characteristic_time_ = 1./self.characteristic_time_

    def set_initial_conditions(self):
        #### not the right place for this
        self.grid[1].phi_[0] = 4.0
        # don't override the initial condition on c_
        SingleParticle.set_initial_conditions(self)
        
    def pore_volume_(self,number):
        # This is not a porous region
        return 0.0
    #### not used internally
    def boundary_term_(self,number):
        # All current at the interface is carried by cations
        return liblib.common.inverse_F_*1e-8#self.cell_domain.field[0][self.cell_domain.field_number_with_name['i_'],0]

    def c2_(self,relative_time_index,unit_number):

        return None
    
    def phi2_(self,unit_number):

        return 0.0
    
    def set_phi1_(self,phi_):
        self.grid[1].phi_[0] = phi_
        
    def get_phi_(self):
        return self.grid[1].phi_[0]

    def time_dependent_system_step(self,timestep_size,output,handle_exception_in_solver=True,adaptive_timestepping=False,minimum_timestep_size=1e-6,use_predictor=True,quiet=False,interrupted=False):

        # Call base class method with dimensionless timestep size
        return SingleParticle.time_dependent_system_step(self,timestep_size*self.inverse_characteristic_time_,output,handle_exception_in_solver=handle_exception_in_solver,adaptive_timestepping=adaptive_timestepping,minimum_timestep_size=minimum_timestep_size,use_predictor=use_predictor,quiet=quiet,interrupted=interrupted)
        
    def D1(self,c):
        r'''
This is used in the dimensionless calculations of the parent class.

Parameters
----------
c : float
    Dimensionless lithium concentration

Returns
-------
float
    Dimensionless lithium diffusivity
        '''
        return self.active_material.D_(c*self.active_material.c_max_)*self.active_material.inverse_D_at_c_initial_

    def i_n(self):
        r'''
This is used in the dimensionless calculations of the parent class.

Returns
-------
    float
        Instantaneous dimensionless conventional microscopic current density (at the particle surface), equal to the dimensionless lithium flux, in the positive r direction
        '''
        # Unlike with SingleParticle_, the macroscopic current density is not uniformly distributed to all particles and must be computed        
        return self.particle_radius_*self.active_material.inverse_D_at_c_initial_*self.active_material.inverse_c_max_*self.inverse_surface_area_per_particle_*self.inverse_active_material_area_density_*liblib.common.inverse_F_*self.i_n_()
    
    def i_n_(self):
        r'''
Returns
-------
    float
        Instantaneous dimensional conventional microscopic current density (at the particle surface), equal to the dimensionless lithium flux, in the positive r direction
        '''
        ## Unlike with SingleParticle_, the macroscopic current density is not uniformly distributed to all particles and must be computed
        
        c_field_number = self.grid[0].field_number_with_name['c']
        c_ = self.grid[0].field[0][c_field_number,-1]*self.active_material.c_max_

        phi1_ = self.grid[1].phi_[0]

        #### We could alternatively use direct access to data structures, which would be faster but harder to maintain
        c2_ = self.medium.c2_(self.position)
        phi2_ = self.medium.phi2_(self.position)

        eta_s_ = phi1_ - phi2_ - self.electrochemical_reaction.U_(c_)

        return self.electrochemical_reaction.i_n_(eta_s_,c_,c2_)

    def i_(self,phi1_):
        r'''
Calculates dimensional current density by the Butler-Volmer equation, as required to use cell potential for control

Parameters
----------
``phi1_`` : float
    Dimensional terminal potential [V]

Returns
-------
float
    Dimensional current density :math:`[\textrm{A}/\textrm{m}^2]`
    '''

        c_field_number = self.grid[0].field_number_with_name['c']
        c_ = self.grid[0].field[0][c_field_number,-1]*self.active_material.c_max_
        #### this now has to come out of the information about the medium. This should now be used for all control, not just cell potential control, since the particles might have different current densities due to different internal concentrations, different potentials, different medium information. This will still be used for cell potential control, but when current control is used, we might require an root-finding step to get the correct potential and then use this. Or in the original code, the boundary condition for the matrix potential was tied to the applied current density, can that be done here? Not if the entire matrix is at the same potential

        #### Could alternatively use direct access to data structures
        c2_ = self.medium.c2_(self.position)
        phi2_ = self.medium.phi2_(self.position)

        eta_s_ = phi1_ - phi2_ - self.electrochemical_reaction.U_(c_)
        #### this is macroscopic. why don't we use the i_n_ here in place of the one defined above?
        return self.surface_area_per_particle_*self.active_material_area_density_*self.electrochemical_reaction.i_n_(eta_s_,c_,c2_)
    
    def process_solution(self):

        c_field_number = self.grid[0].field_number_with_name['c']
        for index in range(self.grid[0].unit_count):
            self.grid[0].c_[index] = self.active_material.c_max_*self.grid[0].field[0][c_field_number,index]

        #### Could alternatively use direct access to data structures
        c2_ = self.medium.c2_(self.position)
        phi2_ = self.medium.phi2_(self.position)

        self.grid[1].phi_[0] = self.electrochemical_reaction.V_(self.i_n_(),self.grid[0].c_[-1],c2_) + phi2_

        
# Objects of this class describe the spatial domains on which models will be
# solved.
class ElectrodeDomain(pygdh.Grid):

    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_grid' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self, name, volume_count):

        # # NMC333
        # self.phi1_int = 4.3 # V initial potential of positive electrode, from COMSOL model
        # #self.phi1_int = 4.290516691195901 # V initial potential of positive electrode, modified
        # self.phi2_int = 0.0 # V initial potential of solution, from COMSOL model
        # Graphite
        self.phi1_int = 1.3 # V initial potential of positive electrode, from COMSOL model
        self.phi2_int = 0.0 # V initial potential of solution, from COMSOL model        

        self.c2_int = 500. # mol/m^3 initial salt concentration, from COMSOL model

        self.initialize_grid(name,
                             field_names=['phi1','phi2','c2'],
                             # The dependent variable will be named 'x',
                             # and 'phi1' will be computed at the specified number
                             # of equally-spaced values of 'x'
                             coordinate_values={
                                 'x': numpy.linspace(0.0, 1.0, volume_count)},
                             # Each unit in 'Domain' will be described by a
                             # 'Volume' object
                             unit_classes=[
                                 CellVolume for i in range(volume_count) ])

    # This method is required. It informs GDB of the unknown quantities for
    # for which it must solve.
    def declare_unknowns(self):

        # self.unknown[0][:,:] = False
        # self.unknown[1][:,:] = False        
        # electrode_grid_number = 1
        # for vol_number in range(self.positive_electrode_grid.unit_count):
        #     self.unknown[electrode_grid_number][0,vol_number] = True
        
        for vol_number in range(0, self.unit_count):
            vol = self.unit_with_number[vol_number]
            vol.unknown_field[0] = True
            vol.unknown_field[1] = True            
            vol.unknown_field[2] = True

    def set_initial_conditions(self):

        phi1 = self.field[0][0]        
        phi2 = self.field[0][1]        
        c2 = self.field[0][2]
        
        for i in range(self.unit_count):
            phi1[i] = self.phi1_int
            phi2[i] = self.phi2_int
            c2[i] = self.c2_int

    def define_field_variables(self):

        self.surface_overpotential = numpy.empty(self.unit_count)        
        self.i_n_value = numpy.empty(self.unit_count)
        
        # self.left_Li_flux = numpy.empty(self.unit_count)
        # self.right_Li_flux = numpy.empty(self.unit_count)        
        # self.moles_Li_change = numpy.empty(self.unit_count)
        # self.flux_difference = numpy.empty(self.unit_count)        

    def set_output_fields(self):

        self.output_fields = {
            'surface_overpotential' : self.surface_overpotential,
            'i_n' : self.i_n_value}#,
            # 'left_Li_flux' : self.left_Li_flux,
            # 'right_Li_flux' : self.right_Li_flux,            
            # 'moles_Li_change' : self.moles_Li_change,
            # 'flux_difference' : self.flux_difference}
        
class PackedBedMedium(pygdh.Model):

    def __init__(self,domain):
        self.initialize_model([domain],stored_solution_count=2)
        for position in range(domain.unit_count):
            self.grid[0].field[0][0,position] = (domain.unit_count - position)/float(domain.unit_count)
    
    def phi2_(self,position):
        return 0.1

    def c2_(self,position):
        return self.grid[0].field[0][0,position]

#### maybe simulations should be responsible for printing structure contents (allowing for overrides), and Models are responsible for the math (and might share structures), and simulations can coordinate Models and other cooridinators. It would be nice to be able to have simulations do staggered or simultaneous solves, although the equations would always contain the staggered forms, although that's okay as that should be compatible with the simultaneous forms. But this would mean separating the solving structure from the Models and moving it to the simulation.
    
class ParticleMatrix_(pygdh.Simulation):

    # taking one timestep of this Model should evolve all particle solutions by one global timestep
    
    def __init__(self,medium,positions,interior_unit_count,cell_domain,active_material,particle_radius_,active_material_loading_,electrochemical_reaction):
        
        self.positions = positions

        self.count = len(self.positions)

        self.interior_domains = [ SingleParticleDomain('interior_' + str(i),interior_unit_count) for i in range(self.count) ]
        
        self.surface_domains = [ SingleParticleSurfaceDomain('surface_' + str(i)) for i in range(self.count) ]        

        #### in the porous bed, we don't know where the current collector sits, maybe just assume that everything has the same path to a current collector and so is at the same potential, maybe do the same for potential in solution, relative distance to the other electrode might be large. could always return a constant
        #### let's assume that the surrounding medium includes a conductive matrix and that we can query that matrix to get the potential of the conductive material
        #### also assume that potential in solution can be queried
        #### same for salt concentration
        #### p

        #### in a porous electrode, the current collector is at the far end, potential in solution varies end-to-end
        
        #### embeddedparticle is like singleparticle but has a more complicated relationship to the surrounding electrolyte region
        
        self.particles = [ EmbeddedParticle_(medium,i,self.interior_domains[i],self.surface_domains[i],active_material,particle_radius_,active_material_loading_,electrochemical_reaction) for i in range(self.count) ]

        grid_sequence = []
        grid_sequence.extend(self.interior_domains)
        grid_sequence.extend(self.surface_domains)
        
        self.initialize_simulation(grid_sequence,self.particles,stored_solution_count=2)

        #### want to be able to take one timestep of the simulation and for all coordinated models to then take one timestep
        
# class PorousElectrode_(ConcentratedBinarySaltSolutionFilledSeparator_,BaseElectrode):

#     def solution_governing_equations_at_left_interface(self, vol, residual):

#         #c_1 = self.left_neighboring_c2(-1)
#         #if c_1 == None:
#         # doesn't seem like the interpolation makes a difference
#         #c_1 = vol.interpolate(self.separator_grid.field[-1][self.concentration_field_number])
#         c_1 = self.separator_grid.field[-1][self.concentration_field_number,0]
        
#         c = self.separator_grid.field[0][self.concentration_field_number]
#         left_neighboring_c2 = self.left_neighboring_c2(0)
#         if left_neighboring_c2 == None:
#             residual[0] = ((self.left_neighboring_pore_volume() + self.porosity*vol.volume)*(vol.interpolate(c) - c_1)*self.inverse_timestep_size + self.right_boundary_term(vol) - self.left_neighboring_boundary_term())
#         else:
#             residual[0] = vol.interpolate(c) - self.left_neighboring_c2(0)

        
# # Objects of this class bring the spatial domain definitions together with
# # equations, boundary conditions, and for time-dependent models, initial
# # conditions. These objects can then be directed to compute numerical
# # solutions.
# class Newman(pygdh.Model):
        
#     def i_n(self,c1s,c2,surface_overpotential):
#         i0 = self.k0*self.F*((self.c1max-c1s)*c2)**self.alpha_a*c1s**self.alpha_c # discrepancy with COMSOL model parameter, but consistent with COMSOL variable definition
#         return i0*(math.exp(surface_overpotential*self.alpha_a*self.F__RT) - math.exp(-surface_overpotential*self.alpha_c*self.F__RT)) # consistent with COMSOL model cathode variable definition, if surface_overpotential = phi1 - phi2 - Ueq
    
#     def calculate_global_values(self):

#         electrode_grid = self.grid[0]
#         particle_grid = self.grid[2]
        
#         phi1 = electrode_grid.field[0][0]
#         phi2 = electrode_grid.field[0][1]
#         c2 = electrode_grid.field[0][2]
        
#         for electrode_vol in electrode_grid.unit_with_number:
#             particle_vol = particle_grid.unit_with_index[electrode_vol.number,-1]
#             c1s = particle_grid.field[0][0,particle_vol.number]        
#             electrode_grid.surface_overpotential[electrode_vol.number] = (electrode_vol.interpolate(phi1) - electrode_vol.interpolate(phi2) - self.Ueq(c1s))
#             electrode_grid.i_n_value[electrode_vol.number] = self.i_n(c1s,electrode_vol.interpolate(c2),electrode_grid.surface_overpotential[electrode_vol.number])
            
#     def electrode_governing_equations_at_positive_current_collector(self, vol, residual):

#         electrode_grid = self.grid[0]
        
#         phi1 = electrode_grid.field[0][0]
#         phi2 = electrode_grid.field[0][1]
#         c2_1 = electrode_grid.field[-1][2]
#         c2 = electrode_grid.field[0][2]

#         # approximate integral of rate source
#         integral_of_rate_source = self.Sa*electrode_grid.i_n_value[vol.number]*vol.volume # on discharge, the concentration drops in the solution phase of the electrode, so this has to be negative

#         # Zero flux boundary condition at positive current collector
#         self.solution_governing_equations_at_left_interface(vol, residual)
#         residual[0] -= (1.0-self.tplus)*integral_of_rate_source*self.Lpos**2/self.F
        
#         ## Solid phase
#         # During cell discharge, self.i_app < 0 and so electrons flow into positive electrode from
#         # positive current collector.
#         # Electrons flow toward increasing potential, so the potential derivative at the current
#         # collector should be positive, so the self.i_app term has the correct sign.
#         residual[1] = (self.sigma_eff*vol.dx_right(phi1) + self.i_app*self.Lpos
#                        - self.Lpos**2*integral_of_rate_source)

#         c2_right = vol.interpolate_right(c2)

#         kappa_eff_right = self.kappa(c2_right)*self.eff_factor_pos
#         kappa_effd_right = kappa_eff_right*self.effd_factor(c2_right)

#         ## Solution phase
#         # Zero flux boundary condition at positive current collector
#         residual[2] = (-kappa_eff_right*vol.dx_right(phi2) + kappa_effd_right*vol.dx_right(c2)/c2_right - self.Lpos**2*integral_of_rate_source)

#     def electrode_governing_equations(self, vol, residual):

#         electrode_grid = self.grid[0]

#         phi1 = electrode_grid.field[0][0]
#         phi2 = electrode_grid.field[0][1]
#         c2_1 = electrode_grid.field[-1][2]
#         c2 = electrode_grid.field[0][2]        

#         # approximate integral of rate source
#         integral_of_rate_source = self.Sa*electrode_grid.i_n_value[vol.number]*vol.volume
        
#         # Salt concentration
#         # dc2/dt will have the same sign as integral_of_rate_source. On discharge, Li+ ions are being
#         # absorbed from the solution, so integral_of_rate_source < 0.
        
#         # This fills residual[0] with the residual for a separator
#         self.solution_governing_equations_in_bulk(vol,residual)

#         # To the residual for a separator, one must add a source term describing the outflow of cations from the co-located active material
#         residual[0] -= (1.0-self.tplus)*integral_of_rate_source*self.Lpos**2)/self.F
        
#         ## Solid phase
#         residual[1] = (self.sigma_eff*(vol.dx_right(phi1) - vol.dx_left(phi1))
#                        - self.Lpos**2*integral_of_rate_source) # checks

#         ## Solution phase
#         c2_right = vol.interpolate_right(c2)
#         c2_left = vol.interpolate_left(c2)

#         kappa_eff_left = self.kappa(c2_left)*self.eff_factor_pos
#         kappa_eff_right = self.kappa(c2_right)*self.eff_factor_pos
#         kappa_effd_left = kappa_eff_left*self.effd_factor(c2_left)
#         kappa_effd_right = kappa_eff_right*self.effd_factor(c2_right)
        
#         residual[2] = (-kappa_eff_right*vol.dx_right(phi2) + kappa_effd_right*vol.dx_right(c2)/c2_right + kappa_eff_left*vol.dx_left(phi2) - kappa_effd_left*vol.dx_left(c2)/c2_left - self.Lpos**2*integral_of_rate_source) # checks
                
#     def electrode_governing_equations_at_separator(self, vol, residual):

#         electrode_grid = self.grid[0]

#         phi1 = electrode_grid.field[0][0]
#         phi2 = electrode_grid.field[0][1]
#         c2_1 = electrode_grid.field[-1][2]
#         c2 = electrode_grid.field[0][2]        

#         # approximate integral of rate source
#         integral_of_rate_source = self.Sa*electrode_grid.i_n_value[vol.number]*vol.volume

#         # Salt concentration
#         # dc2/dt will have the same sign as integral_of_rate_source. On discharge, Li+ ions are being
#         # absorbed from the solution, so integral_of_rate_source < 0.

#         self.solution_governing_equations_at_right_interface(vol, residual)
#         residual[0] -= (1.0-self.tplus)*integral_of_rate_source*self.Lpos**2/self.F        
        
#         ## Solid phase
#         residual[1] = (self.sigma_eff*vol.dx_left(phi1)
#                        + self.Lpos**2*integral_of_rate_source)

#         # Boundary conditions for phi2, c2 are not explicitly given in COMSOL
#         # The two "half-volumes" here are treated as a "whole-volume" controlled by a single value for each field. The governing equation averages that on the two sides.
        
#         c2_left = vol.interpolate_left(c2)
#         c2_right = vol.interpolate_right(c2)
                        
#         kappa_eff_left = self.kappa(c2_left)*self.eff_factor_pos
#         kappa_eff_right = self.kappa(c2_right)*self.eff_factor_pos
#         kappa_effd_left = kappa_eff_left*self.effd_factor(c2_left)
#         kappa_effd_right = kappa_eff_right*self.effd_factor(c2_right)

#         corresponding_solution_vol = self.grid[1].unit_with_number[0]
#         corresponding_phi2 = self.grid[1].field[0][0]
#         corresponding_c2 = self.grid[1].field[0][1]
#         corresponding_c2_left = corresponding_solution_vol.interpolate_left(corresponding_c2)                
#         corresponding_kappa_eff_left = self.kappa(corresponding_c2_left)*self.eff_factor_sep
#         corresponding_kappa_effd_left = corresponding_kappa_eff_left*self.effd_factor(corresponding_c2_left)
#         corresponding_D2_eff_left = self.D2(corresponding_c2_left)*self.eff_factor_sep
#         corresponding_c2_right = corresponding_solution_vol.interpolate_right(corresponding_c2)                        
#         corresponding_D2_eff_right = self.D2(corresponding_c2_right)*self.eff_factor_sep                  
#         corresponding_kappa_eff_right = self.kappa(corresponding_c2_right)*self.eff_factor_sep
#         corresponding_kappa_effd_right = corresponding_kappa_eff_right*self.effd_factor(corresponding_c2_right)
#         # This is the equation governing the interior of the electrode
#         residual[2] = (-(self.Lpos/self.Lsep*(corresponding_kappa_eff_right*corresponding_solution_vol.dx_right(corresponding_phi2) - corresponding_kappa_effd_right*corresponding_solution_vol.dx_right(corresponding_c2)/corresponding_c2_right)) + kappa_eff_left*vol.dx_left(phi2) - kappa_effd_left*vol.dx_left(c2)/c2_left - self.Lpos**2*integral_of_rate_source)

#     def particle_governing_equations(self, vol, residual):

#         # Defined for clarity
#         c1_1 = self.grid[2].field[-1][0]        
#         c1 = self.grid[2].field[0][0]

#         c1_left = vol.interpolate_left(c1)
#         c1_right = vol.interpolate_right(c1)        
#         c1_up = vol.interpolate_up(c1)        
#         c1_down = vol.interpolate_down(c1)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]
        
#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c1) - vol.interpolate(c1_1))*self.inverse_timestep_size + (-x2**2*self.D_Li(c1_right)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_right(c1) + x1**2*self.D_Li(c1_left)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_left(c1))*vol.length[1] + (-y2**2*self.D_Li(c1_up)/self.rp*vol.dy_up(c1) + y1**2*self.D_Li(c1_down)/self.rp*vol.dy_down(c1))*vol.length[0]

#     def particle_governing_equations_zero_flux_down_boundary(self, vol, residual):

#         # Defined for clarity
#         c1_1 = self.grid[2].field[-1][0]        
#         c1 = self.grid[2].field[0][0]

#         c1_left = vol.interpolate_left(c1)
#         c1_right = vol.interpolate_right(c1)        
#         c1_up = vol.interpolate_up(c1)        
#         c1_down = vol.interpolate_down(c1)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]
        
#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c1) - vol.interpolate(c1_1))*self.inverse_timestep_size + (-x2**2*self.D_Li(c1_right)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_right(c1) + x1**2*self.D_Li(c1_left)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_left(c1))*vol.length[1] + (-y2**2*self.D_Li(c1_up)/self.rp*vol.dy_up(c1))*vol.length[0]

#     def particle_governing_equations_down_left_boundary(self, vol, residual):

#         # Defined for clarity
#         c1_1 = self.grid[2].field[-1][0]        
#         c1 = self.grid[2].field[0][0]

#         c1_left = vol.interpolate_left(c1)
#         c1_right = vol.interpolate_right(c1)        
#         c1_up = vol.interpolate_up(c1)        
#         c1_down = vol.interpolate_down(c1)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]
        
#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c1) - vol.interpolate(c1_1))*self.inverse_timestep_size + (-x2**2*self.D_Li(c1_right)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_right(c1))*vol.length[1] + (-y2**2*self.D_Li(c1_up)/self.rp*vol.dy_up(c1))*vol.length[0]

#     def particle_governing_equations_down_right_boundary(self, vol, residual):

#         # Defined for clarity
#         c1_1 = self.grid[2].field[-1][0]        
#         c1 = self.grid[2].field[0][0]

#         c1_left = vol.interpolate_left(c1)
#         c1_right = vol.interpolate_right(c1)        
#         c1_up = vol.interpolate_up(c1)        
#         c1_down = vol.interpolate_down(c1)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]

#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c1) - vol.interpolate(c1_1))*self.inverse_timestep_size + (x1**2*self.D_Li(c1_left)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_left(c1))*vol.length[1] + (-y2**2*self.D_Li(c1_up)/self.rp*vol.dy_up(c1))*vol.length[0]
        
#     def particle_governing_equations_zero_flux_left_boundary(self, vol, residual):

#         # Defined for clarity
#         c1_1 = self.grid[2].field[-1][0]        
#         c1 = self.grid[2].field[0][0]

#         c1_left = vol.interpolate_left(c1)
#         c1_right = vol.interpolate_right(c1)        
#         c1_up = vol.interpolate_up(c1)        
#         c1_down = vol.interpolate_down(c1)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]
        
#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c1) - vol.interpolate(c1_1))*self.inverse_timestep_size + (-x2**2*self.D_Li(c1_right)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_right(c1))*vol.length[1] + (-y2**2*self.D_Li(c1_up)/self.rp*vol.dy_up(c1) + y1**2*self.D_Li(c1_down)/self.rp*vol.dy_down(c1))*vol.length[0]

#     def particle_governing_equations_zero_flux_right_boundary(self, vol, residual):

#         # Defined for clarity
#         c1_1 = self.grid[2].field[-1][0]        
#         c1 = self.grid[2].field[0][0]

#         c1_left = vol.interpolate_left(c1)
#         c1_right = vol.interpolate_right(c1)        
#         c1_up = vol.interpolate_up(c1)        
#         c1_down = vol.interpolate_down(c1)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]
        
#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c1) - vol.interpolate(c1_1))*self.inverse_timestep_size + (x1**2*self.D_Li(c1_left)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_left(c1))*vol.length[1] + (-y2**2*self.D_Li(c1_up)/self.rp*vol.dy_up(c1) + y1**2*self.D_Li(c1_down)/self.rp*vol.dy_down(c1))*vol.length[0]
        
#     def particle_governing_equations_influx_boundary(self, vol, residual):

#         electrode_grid = self.grid[0]

#         # Defined for clarity
#         c1_1 = self.grid[2].field[-1][0]        
#         c1 = self.grid[2].field[0][0]

#         c1_left = vol.interpolate_left(c1)
#         c1_right = vol.interpolate_right(c1)        
#         c1_up = vol.interpolate_up(c1)        
#         c1_down = vol.interpolate_down(c1)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]

#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c1) - vol.interpolate(c1_1))*self.inverse_timestep_size + (-x2**2*self.D_Li(c1_right)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_right(c1) + x1**2*self.D_Li(c1_left)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_left(c1))*vol.length[1] + (electrode_grid.i_n_value[vol.index[0]]/self.F + y1**2*self.D_Li(c1_down)/self.rp*vol.dy_down(c1))*vol.length[0]

#     def particle_governing_equations_up_left_boundary(self, vol, residual):

#         electrode_grid = self.grid[0]

#         # Defined for clarity
#         c1_1 = self.grid[2].field[-1][0]        
#         c1 = self.grid[2].field[0][0]

#         c1_left = vol.interpolate_left(c1)
#         c1_right = vol.interpolate_right(c1)        
#         c1_up = vol.interpolate_up(c1)        
#         c1_down = vol.interpolate_down(c1)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]

#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c1) - vol.interpolate(c1_1))*self.inverse_timestep_size + (-x2**2*self.D_Li(c1_right)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_right(c1))*vol.length[1] + (electrode_grid.i_n_value[vol.index[0]]/self.F + y1**2*self.D_Li(c1_down)/self.rp*vol.dy_down(c1))*vol.length[0]

#     def particle_governing_equations_up_right_boundary(self, vol, residual):

#         electrode_grid = self.grid[0]

#         # Defined for clarity
#         c1_1 = self.grid[2].field[-1][0]        
#         c1 = self.grid[2].field[0][0]

#         c1_left = vol.interpolate_left(c1)
#         c1_right = vol.interpolate_right(c1)        
#         c1_up = vol.interpolate_up(c1)        
#         c1_down = vol.interpolate_down(c1)        

#         x1 = vol.coordinate[0] + vol.relative_position_with_local_position((-1.0,0.0))[0]
#         x2 = vol.coordinate[0] + vol.relative_position_with_local_position((1.0,0.0))[0]
#         y1 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,-1.0))[1]
#         y2 = vol.coordinate[1] + vol.relative_position_with_local_position((0.0,1.0))[1]
        
#         residual[0] = vol.length[0]*(y2**3 - y1**3)/3.0*self.rp*(vol.interpolate(c1) - vol.interpolate(c1_1))*self.inverse_timestep_size + (x1**2*self.D_Li(c1_left)*self.interparticle_diffusion_multiplier/self.rp*vol.dx_left(c1))*vol.length[1] + (electrode_grid.i_n_value[vol.index[0]]/self.F + y1**2*self.D_Li(c1_down)/self.rp*vol.dy_down(c1))*vol.length[0]
        
#     # This method is used to define boundary conditions in which the solution
#     # value is specified. This must be consistent with the contents of the
#     # 'declare_unknowns' methods defined in the corresponding 'Grid' objects.
#     def set_boundary_values(self):
#         # message of consistency check is too cryptic.
#         phi2_electrode = self.grid[0].field[0][1]
#         c2_electrode = self.grid[0].field[0][2]

#         phi2_separator = self.grid[1].field[0][0]
#         c2_separator = self.grid[1].field[0][1]

#         # Solution electrochemical potential is continuous at the interface
#         # between the electrode and separator
#         phi2_separator[0] = phi2_electrode[-1]

#         # The condition for c2 at the interface between electrode and
#         # separator is not explicitly given to COMSOL
#         c2_separator[0] = c2_electrode[-1]

#         # Electrochemical potential throughout the cell will be measured
#         # relative to that at the negative electrode surface.
#         phi2_separator[-1] = 0.0
        
#     # This is a required method that notifies GDB about the number of scalar
#     # values returned by each governing equation method.
#     def set_equation_scalar_counts(self):
        
#         # This is a dictionary associating governing equation methods with
#         # the number of scalar values that they return
#         self.equation_scalar_counts = {
#             self.electrode_governing_equations_at_positive_current_collector: 3,
#             self.electrode_governing_equations: 3,
#             self.electrode_governing_equations_at_separator: 3,
#             self.solution_governing_equations: 2,
#             self.solution_governing_equations_at_negative_electrode:1,
#             self.particle_governing_equations:1,
#             self.particle_governing_equations_zero_flux_left_boundary:1,
#             self.particle_governing_equations_zero_flux_down_boundary:1,
#             self.particle_governing_equations_zero_flux_right_boundary:1,
#             self.particle_governing_equations_up_left_boundary:1,
#             self.particle_governing_equations_up_right_boundary:1,
#             self.particle_governing_equations_down_left_boundary:1,
#             self.particle_governing_equations_down_right_boundary:1,
#             self.particle_governing_equations_influx_boundary:1}        
        
#     # This is a required method that notifies GDB about the methods that
#     # describe governing equations, and indicates where in the domain the
#     # equations apply. This must be consistent with the 'declare_unknowns'
#     # methods of the corresponding 'Grid' objects.
#     def assign_equations(self):

#         # Defined for clarity, equations to be defined on the only Grid,
#         # with index 0
#         electrode_equations = self.equations[0]

#         # "Left" domain boundary
#         electrode_equations[0] = [self.electrode_governing_equations_at_positive_current_collector]

#         # Interior volumes
#         for vol_number in range(1, self.grid[0].unit_count-1):
#             electrode_equations[vol_number] = [self.electrode_governing_equations]

#         # "Right" domain boundary
#         electrode_equations[-1] = [self.electrode_governing_equations_at_separator]

#         solution_equations = self.equations[1]

#         solution_equations[0] = []
        
#         for vol_number in range(1,self.grid[1].unit_count-1):
#             solution_equations[vol_number] = [self.solution_governing_equations]

#         # "Right" domain boundary
#         solution_equations[-1] = [self.solution_governing_equations_at_negative_electrode]

#         particle_equations = self.equations[2]

#         # No boundaries for interior volumes
#         for i in range(1,self.grid[2].index_bounds[0]-1):
#             for j in range(1,self.grid[2].index_bounds[1]-1):
#                 particle_equations[self.grid[2].unit_with_index[i,j].number] = [self.particle_governing_equations]

#         for vol in self.grid[2].unit_set_with_name['bottom']:
#             particle_equations[vol.number] = [self.particle_governing_equations_zero_flux_down_boundary]

#         for vol in self.grid[2].unit_set_with_name['top']:
#             particle_equations[vol.number] = [self.particle_governing_equations_influx_boundary]
            
#         for vol in self.grid[2].unit_set_with_name['left']:
#             particle_equations[vol.number] = [self.particle_governing_equations_zero_flux_left_boundary]

#         for vol in self.grid[2].unit_set_with_name['right']:
#             particle_equations[vol.number] = [self.particle_governing_equations_zero_flux_right_boundary]
            
#         # Corner volumes
#         particle_equations[self.grid[2].unit_with_index[0,0].number] = [self.particle_governing_equations_down_left_boundary]
#         particle_equations[self.grid[2].unit_with_index[-1,0].number] = [self.particle_governing_equations_down_right_boundary]
#         particle_equations[self.grid[2].unit_with_index[0,-1].number] = [self.particle_governing_equations_up_left_boundary]
#         particle_equations[self.grid[2].unit_with_index[-1,-1].number] = [self.particle_governing_equations_up_right_boundary]        

#     def source(self,coordinate):

#         ### it is flexible, but inefficient, to reference this by coordinate position and interpolate, if we have as many particles as electrode positions. However, this is necessary if we allow these number to be different, although it seems inefficient when there are an excess of particles
#         ### Assuming that macroscopic current density is given, it has to be distributed among particles at all positions. The distribution of particles is assumed to be uniform, so one could write microscopic current density as a function of position
#         ### the way tthat I think this works is that the overall current density is set at the cell boundaries, and everywhere in the porous electrode, there's a term that describes the current flowing into the particles. The potential in solution is simultaneously solved and ensures zero ionic flux at the current collector. The potential in the matrix is also simultaneously solved and all current is carried electronically at the current collector. So this forces the potential
        
#     def solution_governing_equations_at_right_interface(self, vol, residual):
        
#         c_1 = self.right_neighboring_c2(-1)
#         if c_1 == None:
#         #c_1 = vol.interpolate(self.separator_grid.field[-1][self.concentration_field_number])
#             c_1 = self.separator_grid.field[-1][self.concentration_field_number,-1]
            
#         c = self.separator_grid.field[0][self.concentration_field_number]
        
#         residual[0] = ((self.porosity*vol.volume + self.right_neighboring_pore_volume())*(vol.interpolate(c) - c_1)*self.inverse_timestep_size + self.right_neighboring_boundary_term() - self.left_boundary_term(vol))
    
# ### maybe these should be coordinated as separate models instead
# ### maybe we should get rid of simulations and generalize models to contain other models that can be solved simultaneously or staggered
# ### a model is kind of like a simulation with one submodel
# ### maybe we should be separating mathematical model descriptions from the solvers, but sometimes the discretization is closely tied to the submodel ordering, like when information is expected to flow from region to region in a particular order
# ### maybe simulations should be able to coordinate other simulations
# ### seems like output should be handled at the lowest level, but maybe we want multiple models to write to the same grid?
# ### what if multiple models use the same grid?
# class Pseudo2DSimulation(pygdh.Simulation):
#     #BaseElectrode,pygdh.Model):

#     def __init__(self,particle_count):
    
#         self.particle_count = particle_count

#         self.particle_interior_domain = [ SingleParticleDomain('particle_interior_'+str(i),unit_count) for i in range(self.particle_count) ] 
#         self.particle_surface_domain = [ SingleParticleSurfaceDomain('particle_surface_domain_'+str(i)) for i in range(self.particle_count) ]       
#         self.particle = [ SingleParticle(c_initial,self.particle_interior_domain[i],self.particle_surface_domain[i]) for i in range(self.particle_count) ]

#         self.initialize_simulation(self.particle_surface_domain,self.particle,stored_solution_count=2)

        
class LithiumSurfaceDomain(pygdh.Grid):

    def __init__(self, name):

        self.initialize_grid(name, unit_classes=[ pygdh.Volume ])
    
    def define_field_variables(self):

        self.phi_ = numpy.zeros(1)        

    def set_output_fields(self):

        self.output_fields = {
            'phi_ [V]': self.phi_}
        
class Lithium(BaseElectrode,pygdh.Model):
    r'''
Parameters
----------
cell_domain : interface.CellDomain
    Storage object for observables and controls at cell level

surface_domain : LithiumSurfaceDomain
    Storage object for information at the particle surface

electrochemical_reaction : electrochemical_reaction.BaseElectrochemicalReaction
    Object describing main electrochemical reaction for electrode
    '''

    def __init__(self,cell_domain,surface_domain,electrochemical_reaction):

        self.cell_domain = cell_domain
        self.surface_domain = surface_domain
        #self.solution_domain = solution_domain        
        self.electrochemical_reaction = electrochemical_reaction

    def pore_volume_(self,number):
        # This is not a porous region
        return 0.0

    def boundary_term_(self,number):
        # All current at the interface is carried by cations
        return liblib.common.inverse_F_*self.cell_domain.field[0][self.cell_domain.field_number_with_name['i_'],0]

    def c2_(self,relative_time_index,unit_number):

        return None
    
    def set_initial_conditions(self):
        pass
 
    def set_phi1_(self,phi_):
        self.surface_domain.phi_[0] = phi_
        
    def get_phi_(self):
        return self.surface_domain.phi_[0]

    def i_(self,phi_):
        r'''
This is required to use cell potential for control

Parameters
----------
``phi1_`` : float
    Dimensional terminal potential [V]

Returns
-------
float
    Dimensional current density :math:`[\textrm{A}/\textrm{m}^2]`
        '''

        if self.left_neighbor != None:
            c2_ = self.left_neighbor.c2_(0,-1)
            phi2_ = self.left_neighbor.phi2_(-1)
        elif self.right_neighbor != None:
            c2_ = self.right_neighbor.c2_(0,0)
            phi2_ = self.right_neighbor.phi2_(0)            
        else:
            raise NotImplementedError
        
        eta_s_ = phi_ - phi2_

        return self.electrochemical_reaction.i_n_(eta_s_,None,c2_)

    def process_solution(self):

        if self.left_neighbor != None:
            c2_ = self.left_neighbor.c2_(0,-1)
            phi2_ = self.left_neighbor.phi2_(-1)
        elif self.right_neighbor != None:
            c2_ = self.right_neighbor.c2_(0,0)
            phi2_ = self.right_neighbor.phi2_(0)            
        else:
            raise NotImplementedError

        self.surface_domain.phi_[0] = self.electrochemical_reaction.V_(self.cell_domain.field[0][self.cell_domain.field_number_with_name['i_'],0],None,c2_) + phi2_

