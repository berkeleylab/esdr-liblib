import pygdh
import math
#import liblib.common
import numpy

class BaseElectrode:

    def i_(self,phi1_):    
        r'''
This is required to use cell potential for control, must be overloaded in derived classes.

Parameters
----------
``phi1_`` : float
    Dimensional terminal potential [V]

Returns
-------
float
    Dimensional current density :math:`[\textrm{A}/\textrm{m}^2]`
        '''
        raise NotImplementedError
        
class SingleParticleDomain(pygdh.Grid):
    r'''
Describes objects used to store calculations for ``SingleParticle``

Parameters
----------
name : str
    Used to generate output file names and also provides a convenient way of referencing these objects

unit_count : int
    Number of units into which domain is discretized
    '''

    def __init__(self, name, unit_count):

        self.initialize_grid(name,
                             coordinate_values = {
                                 'r': numpy.linspace(0., 1., unit_count)},
                             field_names=['c'],
                             unit_classes=[ liblib.common.OneDimensionalVolume
                                            for i in range(unit_count)],
                             stored_solution_count=2)

    def define_field_variables(self):

        self.r_ = numpy.zeros(self.unit_count)
        self.c_ = numpy.zeros(self.unit_count)

    def set_output_fields(self):

        self.output_fields = {
            'r_ [m]': self.r_,
            'c_ [mol/m^3]': self.c_}

class SingleParticleSurfaceDomain(pygdh.Grid):

    def __init__(self,name):

        self.initialize_grid(name, unit_classes=[ pygdh.Volume ])

    def define_field_variables(self):

        self.phi_ = numpy.zeros(1)        

    ### this output might not appear if there are not regular fields defined
    def set_output_fields(self):

        self.output_fields = {
            'phi_ [V]': self.phi_}
        
class SingleParticleAdjacentSolutionDomain(pygdh.Grid):
    r'''
Describes objects used to store calculations for ``SingleParticle``

Parameters
----------
name : str
    Used to generate output file names and also provides a convenient way of referencing these objects

unit_count : int
    Number of units into which domain is discretized
    '''

    def __init__(self, name):

        self.initialize_grid(name,
                             field_names=[],
                             unit_classes=[ pygdh.Volume ])

    def define_field_variables(self):

        self.c_ = numpy.zeros(1)
        self.phi_ = numpy.zeros(1)        

class SingleParticle(pygdh.Problem):
    r'''
Asterisks mark dimensionless quantities. Within the source code, variables representing quantities and having names without a trailing underscore are dimensionlesss. The following definitions transform the dimensional problem described by ``SingleParticle_``:

.. math::

   c_1 = c_1^*/c_{1,\mathrm{max}}^*

.. math::

   r = r^*/r_n^*

.. math::

   t = \frac{t^* D_1^*(c_{1,\mathrm{max}}^*)}{{r_n^*}^2}

.. math::

   N_1 = \frac{r_n^* N_1^*}{D_1^*(c_{1,\mathrm{max}}^*) c_{1,\mathrm{max}}^*}

.. math::

   D_1(c_1) = D_1^*(c_1^*) / D_1^*(c_{1,\mathrm{max}}^*).

This class describes objects that approximately solve

.. math:: 
    
   \frac{\partial c_1}{\partial t} = -\frac{1}{{r}^2} \frac{\partial}{\partial r} \left[{r}^2 N_1\right]

using the FVM representation

.. math::
    
   0 = \frac{\overline{c_1}(t,r_i) - \overline{c_1}(t-\Delta t,r_i)}{\Delta t} \frac{{r_{i+1}}^3 - {r_i}^3}{3} + \left[{r}^2 N_1(t)\right]_{r_i}^{r_{i+1}} + \mathcal{O}(\Delta t),

where

.. math::

   \overline{c_1}(t,r_i) = \frac{3}{{r_{i+1}}^3 - {r_i}^3} \int_{r_i}^{r_{i+1}} {r}^2 c_1(t,r) \ dr

.. math::

   N_1 = -D_1(c_1) \frac{\partial c_1}{\partial r}

.. math::

   N_1 = 0 \textrm{ at center } r = 0

.. math::

   N_1 = i_n \textrm{ at surface } r = 1.

Parameters
----------
c_initial : float
    Initial dimensionless uniform lithium concentration within the particle

interior_domain : electrode.SingleParticleDomain
    Storage object for calculations within the particle

    '''
    
    def __init__(self,c_initial,interior_domain,surface_domain,solution_domain):
        
        self.c_initial = c_initial

        self.initialize_problem([interior_domain,surface_domain,solution_domain],stored_solution_count=2)

        self.F_ = 96487. # eq / mol

        self.inverse_F_ = 1./self.F_
        
        self.one_third = 1./3.

    def declare_unknowns(self):
        
        for vol_number in range(self.grid[0].unit_count):
            self.unknown[0][0,vol_number] = True
            
    def set_equation_scalar_counts(self):
        self.equation_scalar_counts = {
            self.particle_inner_equation:1,
            self.particle_interior_equation:1,
            self.particle_outer_equation:1 } 

    def assign_equations(self):

        electrode_equations = self.equations[0]

        electrode_equations[0] = [self.particle_inner_equation]
        for i in range(1,self.grid[0].unit_count-1):
            electrode_equations[i] = [self.particle_interior_equation]
        electrode_equations[-1] = [self.particle_outer_equation]        

    def set_initial_conditions(self):

        for vol in self.grid[0].unit_with_number:
            self.grid[0].field[0][0,vol.number] = self.c_initial

    def particle_inner_equation(self, vol, residual):
        # This method isn't actually necessary; particle_interior_equation should give the same result at this location, just slightly less efficiently.

        # Defined for clarity
        c_1 = self.grid[0].field[-1][0]        
        c = self.grid[0].field[0][0]

        #c_left = vol.interpolate_left(c)
        c_right = vol.interpolate_right(c)        

        r_left = vol.coordinate + vol.left
        r_right = vol.coordinate + vol.right
        
        residual[0] = (vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size*r_right**3*self.one_third - self.D1(c_right)*r_right**2*vol.dx_right(c)

    def particle_interior_equation(self, vol, residual):

        # Defined for clarity
        c_1 = self.grid[0].field[-1][0]        
        c = self.grid[0].field[0][0]

        c_left = vol.interpolate_left(c)
        c_right = vol.interpolate_right(c)        

        r_left = vol.coordinate + vol.left
        r_right = vol.coordinate + vol.right

        residual[0] = (vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size*(r_right**3 - r_left**3)*self.one_third - (self.D1(c_right)*r_right**2*vol.dx_right(c) - self.D1(c_left)*r_left**2*vol.dx_left(c))

    def particle_outer_equation(self, vol, residual):

        # Defined for clarity
        c_1 = self.grid[0].field[-1][0]        
        c = self.grid[0].field[0][0]

        c_left = vol.interpolate_left(c)
        #c_right = vol.interpolate_right(c)        

        r_left = vol.coordinate + vol.left
        r_right = vol.coordinate + vol.right

        residual[0] = (vol.interpolate(c) - vol.interpolate(c_1))*self.inverse_timestep_size*(r_right**3 - r_left**3)*self.one_third + r_right**2*self.i_n() + self.D1(c_left)*r_left**2*vol.dx_left(c)

    def D1(self,c):
        r'''
This must be overridden in derived classes. 

Parameters
----------
    c : Dimensionless lithium concentration

Returns
-------
    float
        Dimensionless lithium diffusivity
        '''
        raise NotImplementedError        

    def i_n(self):
        r'''
This must be overridden in derived classes. 

Returns
-------
    float
        Instantaneous dimensionless conventional current density, equal to the dimensionless lithium flux, in the positive r direction
        '''
        raise NotImplementedError
            
    def process_solution(self):
        pass
    
class SingleParticle_(BaseElectrode,SingleParticle):
    r'''
    All dimensinal quantities are marked with asterisks. The choice of units for dimensional quantities must be consistent, so that all units cancel in the dimensionless definitions given in the ``SingleParticle`` docstring. Within the source code, variables representing quantities and having names with a trailing underscore are dimensional.

Uses ``SingleParticle`` to approximately solve

.. math:: 
    
   \frac{\partial c_1^*}{\partial t^*} = -\frac{1}{{r^*}^2} \frac{\partial}{\partial r^*} \left[{r^*}^2 N_1^*\right]

.. math::

   N_1^* = -D_1^* \frac{\partial c_1^*}{\partial r^*},

where 

.. math::

   N_1^* = 0 \textrm{ at center } r^* = 0

.. math::

   N_1^* = i_n^* / F^* \textrm{ at surface } r^* = r_n^*

Parameters
----------
cell_domain : interface.CellDomain
    Storage object for observables and controls at cell level

interior_domain : SingleParticleDomain
    Storage object for calculations within the particle

surface_domain : SingleParticleSurfaceDomain
    Storage object for information at the particle surface

solution_domain : SingleParticleAdjacentSolutionDomain
    Storage object for information about solution adjacent to particle

active_material : active_material.BaseMaterial
    Object describing electrode active material propoerties

``particle_radius_`` : float
    Dimensional particle radius

``active_material_loading_`` : float
    Active material mass per unit electrode area :math:`[\textrm{kg}/\textrm{m}^2]`

electrochemical_reaction : electrochemical_reaction.BaseElectrochemicalReaction
    Object describing main electrochemical reaction for electrode
    '''
    
    def __init__(self,cell_domain,interior_domain,surface_domain,solution_domain,active_material,particle_radius_,active_material_loading_,electrochemical_reaction):
        
        # These are only used for input and output and therefore do not need to
        # be "registered" with the nonlinear solver system

        # All electrolyte solution fields will keep their initial inactive
        # status, information will be copied from the neighboring electrolyte
        # solution regions.

        self.cell_domain = cell_domain
        self.solution_domain = solution_domain
        
        self.active_material = active_material

        SingleParticle.__init__(self,self.active_material.c_initial_*self.active_material.inverse_c_max_,interior_domain,surface_domain,solution_domain)

        self.particle_radius_ = particle_radius_

        # These dimensional coordinates never change
        for index in range(self.grid[0].unit_count):
            self.grid[0].r_[index] = self.particle_radius_*self.grid[0].coordinates_list[0][index]

        self.active_material_volume_per_area_ = active_material_loading_*self.active_material.inverse_density_ # m

        self.capacity_per_area_ = self.F_*(self.active_material.c_max_ - self.active_material.c_min_)*self.active_material_volume_per_area_ # C / m^2, assumes one-electron reaction   
                
        self.active_material_area_density_ = self.active_material_volume_per_area_ / (4/3.*math.pi*particle_radius_**3) # m^{-2}, particle number area density
        
        self.inverse_active_material_area_density_ = 1./self.active_material_area_density_

        self.electrochemical_reaction = electrochemical_reaction

        self.surface_area_per_particle_ = 4. * math.pi * self.particle_radius_**2

        self.inverse_surface_area_per_particle_ = 1./self.surface_area_per_particle_

        self.characteristic_time_ = self.particle_radius_**2 * self.active_material.inverse_D1_at_c_initial_
        
        self.inverse_characteristic_time_ = 1./self.characteristic_time_

    def set_c_(self,c_):
        self.grid[2].c_[0] = c_

    def set_phi2_(self,phi_):
        self.grid[2].phi_[0] = phi_

    def set_phi1_(self,phi_):
        self.grid[1].phi_[0] = phi_
        
    def get_phi_(self):
        return self.grid[1].phi_[0]

    def time_dependent_system_step(self,timestep_size,output,handle_exception_in_solver=True,adaptive_timestepping=False,minimum_timestep_size=1e-6,use_predictor=True,quiet=False,interrupted=False):

        # Call base class method with dimensionless timestep size
        return SingleParticle.time_dependent_system_step(self,timestep_size*self.inverse_characteristic_time_,output,handle_exception_in_solver=handle_exception_in_solver,adaptive_timestepping=adaptive_timestepping,minimum_timestep_size=minimum_timestep_size,use_predictor=use_predictor,quiet=quiet,interrupted=interrupted)
        
    def D1(self,c):
        r'''
This is used in the dimensionless calculations of the parent class.

Parameters
----------
c : float
    Dimensionless lithium concentration

Returns
-------
float
    Dimensionless lithium diffusivity
        '''
        return self.active_material.D1_(c*self.active_material.c_max_)*self.active_material.inverse_D1_at_c_initial_

    def i_n(self):
        r'''
This is used in the dimensionless calculations of the parent class.

Returns
-------
    float
        Instantaneous dimensionless conventional microscopic current density (at the particle surface), equal to the dimensionless lithium flux, in the positive r direction
        '''       
        i_ = self.cell_domain.field[0][self.cell_domain.field_number_with_name['i_'],0]

        return self.particle_radius_*self.active_material.inverse_D1_at_c_initial_*self.active_material.inverse_c_max_*self.inverse_surface_area_per_particle_*self.inverse_active_material_area_density_*i_*self.inverse_F_

    def i_n_(self):
        r'''
Returns
-------
    float
        Instantaneous dimensional conventional microscopic current density (at the particle surface), equal to the dimensionless lithium flux, in the positive r direction
        '''       
        i_ = self.cell_domain.field[0][self.cell_domain.field_number_with_name['i_'],0]
        
        return self.inverse_surface_area_per_particle_*self.inverse_active_material_area_density_*i_

    def i_(self,phi1_):
        r'''
Calculates dimensional current density by the Butler-Volmer equation, as required to use cell potential for control

Parameters
----------
``phi1_`` : float
    Dimensional terminal potential [V]

Returns
-------
float
    Dimensional current density :math:`[\textrm{A}/\textrm{m}^2]`
    '''

        c_field_number = self.grid[0].field_number_with_name['c']
        c_ = self.grid[0].field[0][c_field_number,-1]*self.active_material.c_max_

        eta_s_ = phi1_ - self.grid[2].phi_[0] - self.electrochemical_reaction.U_(c_)

        return self.surface_area_per_particle_*self.active_material_area_density_*self.electrochemical_reaction.i_n_(eta_s_,c_,self.grid[2].c_[0])
    
    def process_solution(self):

        c_field_number = self.grid[0].field_number_with_name['c']
        for index in range(self.grid[0].unit_count):
            self.grid[0].c_[index] = self.active_material.c_max_*self.grid[0].field[0][c_field_number,index]

        self.grid[1].phi_[0] = self.electrochemical_reaction.V_(self.i_n_(),self.grid[0].c_[-1],self.grid[2].c_) + self.grid[2].phi_[0]
        
class LithiumSurfaceDomain(pygdh.Grid):

    def __init__(self, name):

        self.initialize_grid(name, unit_classes=[ pygdh.Volume ])
    
    def define_field_variables(self):

        self.phi_ = numpy.zeros(1)        

    def set_output_fields(self):

        self.output_fields = {
            'phi_ [V]': self.phi_}

class LithiumAdjacentSolutionDomain(pygdh.Grid):

    def __init__(self, name):

        self.initialize_grid(name, unit_classes=[ pygdh.Volume ])
    
    def define_field_variables(self):

        self.phi_ = numpy.zeros(1)        
        self.c_ = numpy.zeros(1)
        
class Lithium(BaseElectrode,pygdh.Problem):

    def __init__(self,cell_domain,surface_domain,solution_domain,electrochemical_reaction):

        self.cell_domain = cell_domain
        self.surface_domain = surface_domain
        self.solution_domain = solution_domain        
        self.electrochemical_reaction = electrochemical_reaction

    def set_initial_conditions(self):
        pass
 
    def set_c_(self,c_):
        self.solution_domain.c_[0] = c_
   
    def set_phi2_(self,phi_):
        self.solution_domain.phi_[0] = phi_

    def set_phi1_(self,phi_):
        self.surface_domain.phi_[0] = phi_
        
    def get_phi_(self):
        return self.surface_domain.phi_[0]

    def i_(self,phi_):
        r'''
This is required to use cell potential for control

Parameters
----------
``phi1_`` : float
    Dimensional terminal potential [V]

Returns
-------
float
    Dimensional current density :math:`[\textrm{A}/\textrm{m}^2]`
        '''
        
        eta_s_ = phi_ - self.solution_domain.phi_[0]

        return self.electrochemical_reaction.i_n_(eta_s_,None,self.solution_domain.c_[0])

    def process_solution(self):

        self.surface_domain.phi_[0] = self.electrochemical_reaction.V_(self.cell_domain.field[0][self.cell_domain.field_number_with_name['i_'],0],None,self.solution_domain.c_[0]) + self.solution_domain.phi_[0]
