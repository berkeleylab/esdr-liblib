from __future__ import absolute_import
from . import separator
from . import electrolyte_solution
from . import electrode
import pygdh

# lithium_electrode = electrode.Lithium()
# for j in range(0,1000,100):
#     V = -3 + j/999.0*6
#     i = -1e3 + j/999.0*2e3
#     print V, lithium_electrode.current_density(V,[1000.0])
#     print i, lithium_electrode.surface_potential(-3.0,3.0,i,[1000.0])
#     c = j
#     print j, lithium_electrode.exchange_current_density([j])
    
solution = electrolyte_solution.Generic('D2.pkl','kappa.pkl')

def kappa_d(c):
    return solution.effd_factor(c)*solution.kappa(c)

def D2(c):
   return 1e-7

def kappa(c):
   return 1e-8

def kappa_d(c):
    return 1e-8

coordinator = pygdh.Coordinator()

separator_unit_count = 40
applied_current_density = -1e-10 # A / m^2
separator_length = 25e-6 # m
separator_porosity = 0.5
separator_tortuosity = 4.0
c2_initial = 1000.0 # mol / m^3

# having shared grids goes against encapsulation, but makes data sharing easy.
positive_electrode_domain = electrode.SurfaceElectrodeDomain('working')

separator_domain = separator.SeparatorDomain('separator',separator_unit_count)

#electrode_grid = SurfaceElectrodeDomain('lithium_electrode')
negative_electrode_domain = electrode.SurfaceElectrodeDomain('counter')

coordinator.initialize_grids([positive_electrode_domain,separator_domain,negative_electrode_domain],past_timestep_count=1)

# Li is depositing on the positive electrode, so there is a cathodic reaction here, which from the perspective of the electrochemical reaction model will correspond to a negative current density. The sign of applied_current_density is negative for discharge, based on the convention chosen for the separator region.
positive_electrode_problem = electrode.LithiumElectrode(applied_current_density,(positive_electrode_domain, separator_domain, negative_electrode_domain), 0, 0)

# Use concentration-dependent parameter values
#separator_problem = separator.ConcentratedSolution(separator_unit_count, separator_length, separator_porosity, separator_tortuosity, solution.cation_transference_number, solution.D2, solution.kappa, kappa_d, c2_initial, applied_current_density, (positive_electrode_domain, separator_domain, negative_electrode_domain))

# Use constant parameter values
separator_problem = separator.ConcentratedSolution(separator_unit_count, separator_length, separator_porosity, separator_tortuosity, solution.cation_transference_number, D2, kappa, kappa_d, c2_initial, applied_current_density, (positive_electrode_domain, separator_domain, negative_electrode_domain))

# Li is being stripped from the negative electrode, so there is an anodic reaction here, which from the perspective of the electrochemical reaction model will correspond to a positive current density
negative_electrode_problem = electrode.LithiumElectrode(-applied_current_density,(positive_electrode_domain, separator_domain, negative_electrode_domain), 2, separator_unit_count-1)

filename_root = 'example'
output_types = ['GNUPLOT']

#separator_problem.solve_time_dependent_system(filename_root, output_types, timestep_count=10, timestep_size=1e-5, handle_exception_in_solver=False)

coordinator.initialize_problems([separator_problem,negative_electrode_problem,positive_electrode_problem])

coordinator.solve_time_dependent_system(filename_root, output_types, timestep_count=10, timestep_size=1e-5, handle_exception_in_solver=False)

# what if I don't want to solve discretized equations, but rather do some postprocessing? I think that what I'm trying to handle here is reorganization to facilitate reuse
# I don't have to use all of the machinery, i could just use the postprocessor.
# I can't really store function calls to make in the future in Python, but I could create wrapper functions, all with the same form, that are easy to call without knowing much.
# it would be nice to be able to automatically transition between simultaneous and staggered solves. would have to combine dof vectors. knowing all of the grids would be helpful for this, which I do have.
# the current coordination approach of taking a step of the timestepper might be a reasonable fit for the intended approach, but seems a bit limiting
# might want to reference grids by some kind of non-numerical label that doesn't change if we change the number of grids, which could happen if we're swapping out components. could use enumerations, but then where are the enumerations defined and how are they accessed? Might have to go with an enumeration or dictionary associating name and number
# might look like this
#self.separator_grid_number=problem.grid_number_with_name['']
# might want to change the structure of problem initializations so that there a clear step in which the grids are introduced
# might want problem merging code
# this approach might be superior to the original, since it permits easier testing
# currently, we're passing the grids to each problem separately, which might actually make sense here, since they don't all need to work with the same grids. good for encapsulation too.
# might make it harder to merge things though

### the current problem is that the grids know their own numbers, and so even if a problem just uses one grid, it should be given access to all grids to keep the numbering straight.
### the clean appraoch might be to allow different grid numbering per Problem
### but the simple change is to just have all problems receive all grids.
# let's make the simple change first and consider the cleaner one later.
