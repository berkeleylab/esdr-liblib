#!/bin/sh
PYGDH_VERSION=`cat ../esdr-pygdh/version.txt`
VERSION=`cat version.txt`
YEARS=`cat years.txt`
sed "s/PYGDH_VERSION/${PYGDH_VERSION}/" README_template.md > README.md
sed "s/VERSION/${VERSION}/" Makefile_template > Makefile
sed "s/VERSION/${VERSION}/" setup_template.py > setup.py 
sed "s/YEARS/${YEARS}/" license_template.txt > LICENSE
sed "s/YEARS/${YEARS}/" copyright_notice_template.txt > copyright_notice.txt 
